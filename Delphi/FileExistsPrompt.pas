unit FileExistsPrompt;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, CommonStuff;

type
  TfmFileExists = class(TForm)
    Label1: TLabel;
    cbSheetName: TComboBox;
    lblSheetName: TLabel;
    btnOK: TButton;
    rbReplace: TRadioButton;
    rbAppend: TRadioButton;
    btnCancel: TButton;
    procedure btnOKClick(Sender: TObject);
    procedure rbAppendClick(Sender: TObject);
    procedure rbReplaceClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmFileExists: TfmFileExists;

implementation

{$R *.dfm}

procedure TfmFileExists.btnOKClick(Sender: TObject);
begin
  if  rbReplace.Checked then ModalResult := mrOK
  else if rbAppend.Checked then ModalResult := mrAppend
  else ModalResult := mrCancel;
end;

procedure TfmFileExists.rbAppendClick(Sender: TObject);
begin
  lblSheetName.Enabled := true;
  cbSheetName.Enabled := true;
  btnOK.Enabled := true;
end;

procedure TfmFileExists.rbReplaceClick(Sender: TObject);
begin
  lblSheetName.Enabled := false;
  cbSheetName.Enabled := false;
  btnOK.Enabled := true;
end;

end.
