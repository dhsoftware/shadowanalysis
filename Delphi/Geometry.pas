unit Geometry;


interface
  uses System.Math, System.Generics.Collections;

  const
    NearZero = 1.0E-6;
    x = 0;
    y = 1;
    z = 2;
    //maxpoly = 256; { number of points in a polygon }

    pi = 3.1415926535897932384626433832795;
    twopi =2.0*pi;
    halfpi = pi/2.0;


  type
    asInt = integer;
    aFloat = double;

    point = packed record
          case aSInt of
             0:
                (x, y, z: aFloat);
             1:
                (v: array [0 .. 2] of aFloat); { vertex }
          end;

    pntarr = array of point;
    pntlist = TList<point>;

    tDirection = (clockwise, anticlockwise);

    PROCEDURE ply_extent (const ply : pntarr; var pmin, pmax : point);   overload;
    PROCEDURE ply_extent (const ply : TList<point>; var pmin, pmax : point);   overload;

    PROCEDURE FixPolygon (var Ply : pntarr;
                          const direction : tDirection = clockwise);   overload;
    PROCEDURE FixPolygon (var Ply : pntlist;
                          const direction : tDirection = clockwise);   overload;

    FUNCTION AngleBetween (const p1, p2, p3 : Point) : double;

    FUNCTION CrossProductPt (const p1, p2, p3 : point) : point;

    FUNCTION PolygonArea(ply : pntarr) : double;

    FUNCTION Intr_SegSeg (p1, p2, p3, p4 : point; var intr : point;
                          var ParallelFlag : boolean) : boolean;

    FUNCTION DisFromSegment2 (segPt1, segPt2, pt : point;
                              var onSegment : boolean): double;

    FUNCTION DisFromLine2 (linPt1, linPt2, pt : point;
                           var onSegment : boolean): double;

    FUNCTION  PntInPly (const pnt : point;
										    ply : pntarr) : integer;        overload;

    FUNCTION PntInPly (const pnt : point;
                       ply : PntList) : integer;   overload;

    FUNCTION MidPnt (const p1, p2 : point) : point;





{$I ../inc/geometry.inc}

  procedure ply_extent (const ply : pntarr; var pmin, pmax : point);
  var
    i : integer;
  begin
    pmin := ply[0];
    pmax := ply[0];
    i := 1;
    while i <= high(ply) do begin
      pmin.x := min(pmin.x, ply[i].x);
      pmin.y := min(pmin.y, ply[i].y);
      pmin.z := min(pmin.z, ply[i].z);
      pmax.x := max(pmax.x, ply[i].x);
      pmax.y := max(pmax.y, ply[i].y);
      pmax.z := max(pmax.y, ply[i].z);
      i := i+1;
    end;
  end;

PROCEDURE ply_extent (const ply : PntList; var pmin, pmax : point);
  var
    i : integer;
  begin
    pmin := ply[0];
    pmax := ply[0];
    i := 1;
    while i < ply.Count do begin
      pmin.x := min(pmin.x, ply[i].x);
      pmin.y := min(pmin.y, ply[i].y);
      pmax.x := max(pmax.x, ply[i].x);
      pmax.y := max(pmax.y, ply[i].y);
      i := i+1;
    end;
  end;

FUNCTION DotProductPt (const p1, p2, p3 : point;
                       var Dis1, Dis2 : double) : double;
VAR
  v12, v23 : point;
BEGIN
  v12.x := p2.x - p1.x;
  v12.y := p2.y - p1.y;
  v12.z := p2.z - p1.z;
  Dis1 := Sqrt(sqr(v12.x) + sqr(v12.y) + sqr(v12.z));
  v23.x := p3.x - p2.x;
  v23.y := p3.y - p2.y;
  v23.z := p3.z - p2.z;
  Dis2 := Sqrt(sqr(v23.x) + sqr(v23.y) + sqr(v23.z));
  result := v12.x*v23.x + v12.y*v23.y + v12.z*v23.z;
END;

FUNCTION CrossProductPt (const p1, p2, p3 : point) : point;

BEGIN
  Result.x := (p1.y-p2.y)*(p3.z-p2.z) - (p1.z-p2.z)*(p3.y-p2.y);
  Result.y := (p1.z-p2.z)*(p3.x-p2.x) - (p1.x-p2.x)*(p3.z-p2.z);
  Result.z := (p1.x-p2.x)*(p3.y-p2.y) - (p1.y-p2.y)*(p3.x-p2.x);
END;

FUNCTION AngleBetween (const p1, p2, p3 : Point) : double;
VAR
  dotProd : double;
  d1, d2 : double;
BEGIN
  dotProd := DotProductPt (p1, p2, p3, d1, d2);
  result := arccos (dotProd / (d1*d2));
END;


/// default is to define polygon is in a clockwise direction (note that this is
/// the opposite order to the DataCAD poly_fix procedure).
/// A specific direction can be specified by passing the direction parameter.
PROCEDURE FixPolygon (var Ply : pntarr;
                      const direction : tDirection = clockwise);
VAR
  lr, lrprev, lrnext, size : integer;
  i, j, step : integer;
  tempply : pntarr;

  FUNCTION FindLowRight : integer;
  VAR
    i : integer;
  BEGIN
    result := low(Ply);
    setlength (tempply, size);
    tempply[low(Ply)] := Ply[low(Ply)];

    for i := low(Ply)+1 to high(Ply) do begin
      tempply[i] := ply[i];
      if (Ply[i].y < Ply[result].y) or
         ((Ply[i].y = Ply[result].y) and (Ply[i].x > Ply[result].x)) then
        result := i;
    end;
  END;

BEGIN
  size := high(Ply) - low(Ply) + 1;
  if size < 3 then exit;  // should never have 1 or 2 point polygons, but just in case ...

  { Find the lowest vertex (or, if there is more than one vertex with the same lowest
    coordinate, the rightmost of those vertices) and then take the cross product of
    the edges before and after it. If the cross product is negative then the polygon
    is anti-clockwise.
    The reason that the lowest, rightmost (or any other such extreme) point works is
    that the internal angle at this vertex is necessarily less than pi. }
  lr := FindLowRight;
  if lr = 0 then
    lrprev := high(Ply)
  else
    lrprev := lr-1;
  lrnext := (lr+1) mod size;
  if CrossProductPt (Ply[lrprev], Ply[lr], Ply[lrnext]).z < 0 then begin
    // polygon is anticlockwise
    if direction = anticlockwise then begin
      if lr = 0 then
        exit; // no need to change anything
      step := 1;
    end
    else
      step := -1   // reverse order of points
  end
  else begin
    if direction = clockwise then begin
      if lr=0 then
        exit;  // no need to change anything
      step := 1;
    end
    else
      step := -1;  // reverse order of points
  end;

  j := lr;
  for i := 0 to high(ply) do begin
    ply[i] := tempply[j];
    j := j + step;
    if j < 0 then
      j := high(ply)
    else if j > high(ply) then
      j := 0;
  end;
END;



/// default is to define polygon is in a clockwise direction (note that this is
/// the opposite order to the DataCAD poly_fix procedure).
/// A specific direction can be specified by passing the direction parameter.
PROCEDURE FixPolygon (var Ply : pntlist;
                      const direction : tDirection = clockwise);
VAR
  lr, lrprev, lrnext, size : integer;
  i, j, step : integer;
  tempply : pntarr;

  FUNCTION FindLowRight : integer;
  VAR
    i : integer;
  BEGIN
    result := 0;
    setlength (tempply, Ply.Count);
    tempply[0] := Ply[0];

    for i := 1 to Ply.Count-1 do begin
      tempply[i] := ply[i];
      if (Ply[i].y < Ply[result].y) or
         ((Ply[i].y = Ply[result].y) and (Ply[i].x > Ply[result].x)) then
        result := i;
    end;
  END;

BEGIN
  size := Ply.Count;
  if size < 3 then exit;  // should never have 1 or 2 point polygons, but just in case ...

  { Find the lowest vertex (or, if there is more than one vertex with the same lowest
    coordinate, the rightmost of those vertices) and then take the cross product of
    the edges before and after it. If the cross product is negative then the polygon
    is anti-clockwise.
    The reason that the lowest, rightmost (or any other such extreme) point works is
    that the internal angle at this vertex is necessarily less than pi. }
  lr := FindLowRight;
  if lr = 0 then
    lrprev := Ply.Count - 1
  else
    lrprev := lr-1;
  lrnext := (lr+1) mod size;
  if CrossProductPt (Ply[lrprev], Ply[lr], Ply[lrnext]).z < 0 then begin
    // polygon is anticlockwise
    if direction = anticlockwise then begin
      if lr = 0 then
        exit; // no need to change anything
      step := 1;
    end
    else
      step := -1   // reverse order of points
  end
  else begin
    if direction = clockwise then begin
      if lr=0 then
        exit;  // no need to change anything
      step := 1;
    end
    else
      step := -1;  // reverse order of points
  end;

  j := lr;
  for i := 0 to Ply.Count-1 do begin
    ply[i] := tempply[j];
    j := j + step;
    if j < 0 then
      j := Ply.Count-1
    else if j >= Ply.Count then
      j := 0;
  end;
END;


FUNCTION PolygonArea(ply : pntarr) : double;  // calculate area of a horizontal polygon (ignores z co-ordinates)
VAR
  i, j : integer;
BEGIN
  result := 0;   // Accumulates area in each iteration
  j := length (ply)-1; // set j to last vertex (i.e. previous to i in the closed polygon)

  for i := 0 to length(ply)-1 do begin
    result := result + (ply[j].x + ply[i].x) * (ply[j].y - ply[i].y);
    j := i;  //j is previous vertex to i
  end;
  result := result/2;  // easier to just divide total by 2 than to divide the calculated x dimension by 2 in the calculation for each side)
END;


FUNCTION Intr_SegSeg (p1, p2, p3, p4 : point; var intr : point; var ParallelFlag : boolean) : boolean;
var
  delta, rmu, l1, l2, l3: double;
BEGIN
  intr.x := 0;
  intr.y := 0;
  intr.z := p1.z;   // note: this function only calculates 2D, but assign a z value so that it is a valid number
  result := false;
  if (Min(p1.x, p2.x) > Max(p3.x, p4.x)) or
     (Min(p1.y, p2.y) > Max(p3.y, p4.y)) or
     (Max(p1.x, p2.x) < Min(p3.x, p4.x)) or
     (Max(p1.y, p2.y) < Min(p3.y, p4.y)) then
    exit;

  ParallelFlag := False;
  p2.x := p2.x - p1.x;
  p2.y := p2.y - p1.y;
  p4.x := p4.x - p3.x;
  p4.y := p4.y - p3.y;
  delta := p2.x * p4.y - p2.y * p4.x;
  {First case segments are paralell !}
  if abs(delta) < 0.00001 then
  begin
    ParallelFlag := False;
    p2.x := p2.x + p1.x;
    p4.x := p4.x + p3.x;
    p2.y := p2.y + p1.y;
    p4.y := p4.y + p3.y;
    l1 := sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
    l2 := sqrt((p3.x - p1.x) * (p3.x - p1.x) + (p3.y - p1.y) * (p3.y - p1.y));
    l3 := sqrt((p3.x - p2.x) * (p3.x - p2.x) + (p3.y - p2.y) * (p3.y - p2.y));
    if RealEqual(l1, l2 - l3, 0.0000001) or RealEqual(l1, l2 + l3, 0.0000001) then
    begin
      Result := true;
      Parallelflag := true;
      intr.x := (p1.x + p2.x + p3.x + p4.x) / 4;
      intr.y := (p1.y + p2.y + p3.y + p4.y) / 4;
    end
    else
      Result := False;
    Exit;
  end;
  {End of parallel case}
  rmu := ((p3.x - p1.x) * p4.y - (p3.y - p1.y) * p4.x) / delta;
  if (rmu > 1.0000001) or (rmu < -0.0000001) then
  begin
    result := False;
    Exit;
  end;
  intr.x := p1.x + RMU * p2.x;
  intr.y := p1.y + RMU * p2.y;
  p2.x := p2.x + p1.x;
  p4.x := p4.x + p3.x;
  p2.y := p2.y + p1.y;
  p4.y := p4.y + p3.y;

  {Rangecheck of the solution}
  if (intr.x > max(p1.x, p2.x)) or (intr.x < min(p1.x, p2.x)) or (intr.x > max(p3.x, p4.x)) or (intr.x < min(p3.x, p4.x))
    or (intr.y > max(p1.y, p2.y)) or (intr.y < min(p1.y, p2.y)) or (intr.y > max(p3.y, p4.y)) or (intr.y < min(p3.y, p4.y))
  then begin
    Result := False;
    exit;
  end;
  Result := True;
END;


function DisFromSegment2( segPt1, segPt2, pt : point; var onSegment : boolean): double;
// returns the square of the shortest distance from pt to the line segment between segPt1
// and segPt2.
// onSegment returns true if the projection of the point onto the infinite line defined
// by segPt1 segPt2 lies between segPt1 and segPt2
var
  a, b, c, d: double;
  len_sq: double;
  param: double;
  xx, yy: double;
  dx, dy: double;
begin
  a := pt.x - segPt1.x;
  b := pt.y - segPt1.y;
  c := segPt2.x - segPt1.x;
  d := segPt2.y - segPt1.y;

  len_sq := sqr(c) + sqr(d);
  param := -1;

  if (len_sq <> 0) then
    param := ((a * c) + (b * d)) / len_sq;

  onSegment := (param >= 0) and (param <= 1);

  xx := segPt1.x + param * c;
  yy := segPt1.y + param * d;

  dx := pt.x - xx;
  dy := pt.y - yy;
  result := sqr(dx) + sqr(dy);
end;


function DisFromLine2( linPt1, linPt2, pt : point; var onSegment : boolean): double;
// returns the square of the shortest distance from pt to the infinite line defined by
// linPt1 and linPt2.
// onSegment returns true if the projection of the point onto the line lies on the
// esegment defined by linPt1 and linPt2
var
  a, b, c, d: double;
  len_sq: double;
  param: double;
  xx, yy: double;
  dx, dy: double;
begin
  a := pt.x - linPt1.x;
  b := pt.y - linPt1.y;
  c := linPt2.x - linPt1.x;
  d := linPt2.y - linPt1.y;

  onSegment := false;

  len_sq := sqr(c) + sqr(d);
  param := -1;

  if (len_sq <> 0) then
  begin
    param := ((a * c) + (b * d)) / len_sq;
  end;

  if param < 0 then
  begin
    xx := linPt1.x;
    yy := linPt1.y;
  end
  else if param > 1 then
  begin
    xx := linPt2.x;
    yy := linPt2.y;
  end
  else begin
    onSegment := true;
    xx := linPt1.x + param * c;
    yy := linPt1.y + param * d;
  end;

  dx := pt.x - xx;
  dy := pt.y - yy;
  result := sqr(dx) + sqr(dy);
end;


FUNCTION  PntInPly (const pnt : point;
										ply : pntarr) : integer;
/// this function will return the following values:
///    1  if pnt is inside ply
///    0  if pnt is on the boundary of ply
///    -1 if pnt is outside ply
VAR
	i, j					: integer;
	ospnt					: point;	// a point set up to be outside ply
	minpnt, maxpnt: point;
	satisfactory	: boolean;
	intr					: point;
	xcount				: integer;
	trycount			: integer;
  ParallelFlag,
  onBoundary    : boolean;

BEGIN

  if length (ply) < 3 then begin   // not a valid polygon, just return -1
    result := -1;
    exit;
  end;

  // check if point is outside bounding rectangle
  ply_extent (ply, minpnt, maxpnt);
  if (pnt.x > maxpnt.x) or (pnt.x < minpnt.x) or (pnt.y > maxpnt.y) or (pnt.y < minpnt.y) then begin
    result := -1;
    exit;
  end;

	// set up ospnt to a point outside the bounding rectangle
	ospnt.x := minpnt.x-1.0;
	ospnt.y := minpnt.y + (maxpnt.y - minpnt.y)/2.0;

	// check how many times a line from ospnt to pnt crosses ply boundary
	// An odd number of times indicates that pnt is inside ply
	// An even number of times indicates that pnt is outside ply
	repeat
		satisfactory := true;
		trycount := 0;
		xcount := 0;
    j := length(ply)-1;
		for i := 0 to length(ply)-1 do begin
      // check for point on side for first try
      if (trycount=0) and (DisFromSegment2(ply[j], ply[i], pnt, onBoundary) < NearZero) and onBoundary then begin
        result := 0;
        exit;
      end;

			if Intr_SegSeg (ospnt, pnt, ply[i], ply[j], intr, ParallelFlag) then begin
				if PointsEqual (intr, ply[i]) or PointsEqual (intr, ply[j]) or ParallelFlag then begin
					// looks like our line goes though one of the points  ... result may not be reliable
					satisfactory := false;
					trycount := trycount+1;
					if PointsEqual (intr, ply[i]) then
						xcount := xcount+1;

          ospnt.y := ospnt.y + (maxpnt.y - minpnt.y)/37;
        end
				else
					xcount := xcount+1;
      end;
      j := i;
		end;
	until satisfactory or (trycount > 20);

	if xcount mod 2 = 0 then
		result := -1
	else
		result := 1;
END;  // PntInPly for pntarr poly


FUNCTION PntInPly (const pnt : point;
                   ply : PntList) : integer;
/// this function will return the following values:
///    1  if pnt is inside ply
///    0  if pnt is on the boundary of ply
///    -1 if pnt is outside ply
VAR
	i, j					: integer;
	ospnt					: point;	// a point set up to be outside ply
	minpnt, maxpnt: point;
	satisfactory	: boolean;
	intr					: point;
	xcount				: integer;
	trycount			: integer;
  ParallelFlag,
  onBoundary    : boolean;

BEGIN

  if ply.Count < 3 then begin   // not a valid polygon, just return -1
    result := -1;
    exit;
  end;

  // check if point is outside bounding rectangle
  ply_extent (ply, minpnt, maxpnt);
  if (pnt.x > maxpnt.x) or (pnt.x < minpnt.x) or (pnt.y > maxpnt.y) or (pnt.y < minpnt.y) then begin
    result := -1;
    exit;
  end;

	// set up ospnt to a point outside the bounding rectangle
	ospnt.x := minpnt.x-1.0;
	ospnt.y := minpnt.y + (maxpnt.y - minpnt.y)/2.0;

	// check how many times a line from ospnt to pnt crosses ply boundary
	// An odd number of times indicates that pnt is inside ply
	// An even number of times indicates that pnt is outside ply
	repeat
		satisfactory := true;
		trycount := 0;
		xcount := 0;
    j := ply.Count-1;
		for i := 0 to ply.Count-1 do begin
      // check for point on side for first try
      if (trycount=0) and (DisFromSegment2(ply[j], ply[i], pnt, onBoundary) < NearZero) and onBoundary then begin
        result := 0;
        exit;
      end;

			if Intr_SegSeg (ospnt, pnt, ply[i], ply[j], intr, ParallelFlag) then begin
				if PointsEqual (intr, ply[i]) or PointsEqual (intr, ply[j]) or ParallelFlag then begin
					// looks like our line goes though one of the points  ... result may not be reliable
					satisfactory := false;
					trycount := trycount+1;
					if PointsEqual (intr, ply[i]) then
						xcount := xcount+1;

          ospnt.y := ospnt.y + (maxpnt.y - minpnt.y)/37;
        end
				else
					xcount := xcount+1;
      end;
      j := i;
		end;
	until satisfactory or (trycount > 20);

	if xcount mod 2 = 0 then
		result := -1
	else
		result := 1;
END;  // PntInPly for TList poly


FUNCTION MidPnt (const p1, p2 : point) : point;
BEGIN
  Result.x := (p1.x + p2.x)/2;
  Result.y := (p1.y + p2.y)/2;
  Result.z := (p1.z + p2.z)/2;
END;  // MidPnt

end.
