program ShadAnalysis;
uses
  Vcl.Forms,
  ShadowStat in 'ShadowStat.pas' {fmShadowStats},
  Geometry in 'Geometry.pas',
  CommonStuff in 'CommonStuff.pas',
  SunCalcs in 'SunCalcs.pas',
  Process in 'Process.pas',
  FileExistsPrompt in 'FileExistsPrompt.pas' {fmFileExists},
  About in 'About.pas' {fmAbout};

{EditLocation}

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfmShadowStats, fmShadowStats);
  Application.CreateForm(TfmFileExists, fmFileExists);
  Application.CreateForm(TfmAbout, fmAbout);
  Application.Run;
end.
