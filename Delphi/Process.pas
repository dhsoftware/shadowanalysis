unit Process;

interface

uses CommonStuff, SunCalcs, Vcl.StdCtrls, System.Classes, ActiveX, ComObj,
     SysUtils, System.DateUtils, vcl.dialogs, System.Variants, WinAPI.Windows,
     Math, Geometry, System.Generics.Collections, System.Generics.Defaults,
     GetXLSheet, System.UITypes;

type
  ShadowTp = record
    ply : pntarr;
    area : double;
  end;

  TProcessResult = (ProcessOK, WrongFace, ProcessError);
  ShadeFlag = (OutsideSurface, NotShaded, ShadedG1, ShadedG2, ShadedG12);

  TProcessThrd = class(TThread)
  private
    startdate, stopdate : TDateTime;
    startndx, stopndx, increment, frequencyndx : integer;
    SurfMin, SurfMax : point;
    ShadeFlags : array of array of ShadeFlag;
    Xflagpos, Yflagpos : array of double;
    PntsInSurface, PntsShadedG1, PntsShadedG2, PntsShadedG12 : integer;
    function ProcessSunPosition (altitude, azimuth : double;
                                 var pctGroup1shade,
                                     pctGroup2shade,
                                     pctTotalShade : double) : TProcessResult;
  protected
    procedure Execute; override;
  public
    procedure setValues (pStartDate, pStopDate : TDateTime;
                         pStartNdx, pStopNdx, pIncrement, pFreqNdx : integer);
  end;

  function SubtractShadows (var Surfaces : TList<pntarr>;
                            var Shadows : TList<ShadowTp>) : boolean;     // included in interface so it can be called in tests

 implementation
  var
    debugfile : TextFile;



  procedure LogDebugInfo (info : string);
  begin
    if ShowDebugInfo then begin
      Writeln(debugfile, info);
      Flush (debugfile);
    end;
  end;

  procedure Log2Numbers (info1 : string; num1 : integer;
                         info2 : string; num2 : integer);
  var
    s : string;
  begin
    if ShowDebugInfo then begin
      s := info1 + ': ' +  IntToStr(num1) + ', ' + info2 + ':' + IntToStr(num2);
      LogDebugInfo (s);
    end;
  end;

  procedure LogPoints (p : array of point);
  var
    s : string;
    ndx : integer;
  begin
    s := '';
    for ndx := low(p) to high(p) do begin
      s:= s + '(' + FloatToStr(p[ndx].x) + ',' + FloatToStr(p[ndx].y) + ',' + FloatToStr(p[ndx].z) + ') '
    end;
    LogDebugInfo(s);
  end;

  function FindMatchingVertex (testpnt : point;
                                TestPly : TList<point>;
                                var VertexNum : integer) : boolean;
  var
    i : integer;
  begin
    result := false;
    for i := 0 to TestPly.Count-1 do begin
      if PointsEqual(testpnt, TestPly[i]) then begin
        VertexNum := i;
        result := true;
        exit;
      end;
    end;
  end;  //FindMatchingVertex



  function SubtractShadows (var Surfaces : TList<pntarr>;
                            var Shadows : TList<ShadowTp>) : boolean;
  var
    ndxShad, ndxSurf, i, j, iprev, jprev : integer;
    NumIntersection : integer;
    Shadow, Surface, newSurf : PntList;
    NewSurfArray : pntarr;
    IntersectFlags : TList<boolean>;
    surfacePointsUsed : array of boolean;
    SurfacePly : pntarr;
    intr : point;
    LinesParallel, surfOK, shadOK, Joined : boolean;
    surfJoinNdx, shadJoinNdx, ndxInsertSurf, ndxInsertShad : integer;
    ndxSurfPnt, ndxShadPnt : integer;
    NewSurfaces : TList<pntarr>;
    RemoveSurface, LoopComplete : boolean;
    {debugging stuff}
    debugname : string;
    fl : TFileStream;
    TempPnt : point;
  begin
    result := true;  // initial setting to no error

    {$REGION 'Sort the individual shadows so that we can process larger ones first' }
    Shadows.Sort(
      TComparer<ShadowTp>.Construct(
        function(const A, B: ShadowTp): Integer
        begin
          if abs(A.area) > abs(B.area) then
            result := -1
          else if abs(A.area) < abs(B.area) then
            result := 1
          else
            result := 0;
        end
      )
    );
    {$ENDREGION}
    LogDebugInfo('  Shadows Sorted');

    for ndxShad := 0 to Shadows.Count-1 do begin
      Shadow := PntList.create;      // create Shadow and Surface as generic lists as
      Surface := PntList.create;     // it is easier to insert/delete points that way
      IntersectFlags := TList<boolean>.create;

      NewSurfaces := TList<pntarr>.Create;  // a single surface polygon may produce multiple
                                            // results (e.g. if a shadow cuts it in half)
                                            // so put results in a new list and add them
                                            // to Surface list at the end of processing

      try
        Shadow.Clear;
        for i := 0 to Length(Shadows[ndxShad].ply)-1 do
          Shadow.Add (Shadows[ndxShad].ply[i]);
        FixPolygon (Shadow, anticlockwise);

        ndxSurf := 0;
        while ndxSurf < Surfaces.Count do begin
          {$REGION 'while loop logic for Surfaces'}
          if length (Surfaces[ndxSurf]) < 3 then
            Surfaces.Delete (ndxSurf)
          else begin
            Surface.Clear;
            IntersectFlags.Clear;
            for i := 0 to Length (Surfaces[ndxSurf])-1 do begin
              Surface.Add(Surfaces[ndxSurf][i]);
              IntersectFlags.Add(false);
            end;
            FixPolygon (Surface, clockwise);

            {$REGION 'add a point into Shadow and Surface at every intersection'}
            NumIntersection := 0;
            iprev := Surface.Count-1;
            i := 0;
            while i < Surface.Count do begin
              j := 0;
              jprev := Shadow.Count-1;
              while j < Shadow.Count do begin
                Log2Numbers('    i('+inttostr(Surface.Count)+')', i, 'j('+ inttostr(Shadow.Count)+')', j);
                if intr_segseg (Surface[iprev], Surface[i], Shadow[jprev], Shadow[j], intr, LinesParallel) then begin
                  LogPoints ([Surface[iprev], Surface[i], Shadow[jprev], Shadow[j], intr]);
                  if LinesParallel then begin
                    // parallel ... no need to do anything; ends of adjacent sides should
                    //              produce any required intersections
                  end

                  // if intersection is really close to a vertex then adjust the vertex, otherwise insert a new vertex
                  else if PointsEqual (intr, Surface[iprev]) then begin
                    if not IntersectFlags[iprev] then begin
                      NumIntersection := NumIntersection+1;
                      IntersectFlags[iprev] := true;
                    end;
                    Surface[iprev] := intr;
                  end
                  else if PointsEqual (intr, Surface[i]) then begin
                    if not IntersectFlags[i] then begin
                      NumIntersection := NumIntersection+1;
                      IntersectFlags[i] := true;
                    end;
                    Surface[i] := intr;
                  end
                  else begin
                    Surface.Insert(i, intr);
                    NumIntersection := NumIntersection+1;
                    IntersectFlags.Insert(i, true);
                    if iprev > i then     // i was zero so iprev was count-1
                      iprev := iprev+1;   // count has increased so update iprev
                  end;
                  if PointsEqual (intr, Shadow[jprev]) then begin
                    Shadow[jprev] := intr;
                  end
                  else if PointsEqual (intr, Shadow[j]) then begin
                    Shadow[j] := intr;
                  end
                  else begin
                    Shadow.Insert(j, intr);
                  end;
                end;
                jprev := j;
                j := j+1;
              end;  // j < Shadow.Count


              iprev := i;
              i := i+1;
            end;   // while i < Surface.Count
            {$ENDREGION}

            LogDebugInfo('   ' + inttostr(NumIntersection) + ' Intersections added');


            // both shadow and surface should now have vertices at every intersection.
            if NumIntersection < 2 then begin
              {$REGION 'no intersections, or just one "touch point"' }
              // There are 3 possibilitys: 1.Surface is entirely within shadow
              //                           2.Shadow is entirely within surface
              //                           3.There is no overlap between surface & shadow
              i := PntInPly (Surface[0], Shadows[0].ply);
              Log2Numbers ('Surface.Count', Surface.Count, 'Shadows.Count', Shadows.Count);
              if (i = 1) or ((i = 0) and (PntInPly (Surface[1], Shadows[0].ply) = 1))  // need to test another point if this one is the 'touch point'
              then begin
                // surface is completeley within shadow so can be removed
                LogDebugInfo('    Surface completely within shadow');
                RemoveSurface := true
              end
              else if PntInPly (Shadows[0].ply[0], Surfaces[ndxSurf]) = 1 then begin
                // shadow is completely within surface
                // need to update surface to 'wrap around' the shadow
                LogDebugInfo('    SHadow completely within surface');
                RemoveSurface := false;

                // find suitable vertices to 'join' the surface and shadow polygons
                surfJoinNdx := 0;
                joined := false;
                while (surfJoinNdx < Surface.Count) and (not joined) do begin
                  shadJoinNdx := 0;
                  while (shadJoinNdx < Shadow.Count) and (not joined) do begin
                    i := 0;
                    iprev := Surface.Count-1;
                    surfok := true;
                    while (i < Surface.Count) and surfok do begin
                      if (i <> surfJoinNdx) and (iprev <> surfJoinNdx) then begin
                        if Intr_SegSeg(Surface[surfJoinNdx], Shadow[shadJoinNdx],
                                       Surface[iprev], Surface[i], intr, LinesParallel) then
                          surfok := false;
                      end;
                      iprev := i;
                      i := i+1;
                    end;

                    shadok := true;
                    if surfok then begin
                      i := 0;
                      iprev := Shadow.Count-1;
                      while (i < Shadow.Count) and shadok do begin
                        if (i <> shadJoinNdx) and (iprev <> shadJoinNdx) then begin
                          if Intr_SegSeg(Surface[surfJoinNdx], Shadow[shadJoinNdx],
                                         Shadow[iprev], Shadow[i], intr, LinesParallel) then
                            shadok := false;
                        end;
                        iprev := i;
                        i := i+1;
                      end;
                    end;

                    if surfok and shadok then begin
                      // existing ndx are ok to use, so join the polygons
                      joined := true;
                      ndxInsertSurf := (surfJoinNdx + 1) mod Surface.Count;
                      ndxInsertShad := shadJoinNdx;
                      repeat
                        Surface.Insert (ndxInsertSurf, Shadow[ndxInsertShad]);
                        ndxInsertSurf := ndxInsertSurf + 1;
                        ndxInsertShad := (ndxInsertShad + 1) mod Shadow.Count;
                      until ndxInsertShad = shadJoinNdx;
                      Surface.Insert (ndxInsertSurf, Shadow[shadJoinNdx]);
                      ndxInsertSurf := ndxInsertSurf + 1;
                      surface.Insert(ndxInsertSurf, Surface[surfJoinNdx]);
                      setlength (SurfacePly, surface.Count);
                      for i := 0 to surface.Count-1 do
                        SurfacePly[i] := Surface[i];
                      Surfaces[ndxSurf] := SurfacePly;
                    end
                    else
                      shadJoinNdx := shadJoinNdx+1;
                  end;
                  surfJoinNdx := SurfJoinNdx+1;
                end;

                ndxSurf := ndxSurf+1;
              end
              else begin
                // no overlap, leave surface unchanged
                RemoveSurface := false;
                LogDebugInfo('    No overlap');

              end;
              {$ENDREGION}
            end   // NumIntersection < 2
            else begin
              {$REGION 'Remove shadow from available surface'}
              LogDebugInfo('    Start removing shadow');

              RemoveSurface := true;
              setlength (surfacePointsUsed, Surface.Count);
              for i := low(surfacePointsUsed) to high (surfacePointsUsed) do  // flag any points inside the shadow as used as
                surfacePointsUsed[i] := (PntInPly (Surface[i], Shadow) > 0);  // they don't need to be processed further.
              LogDebugInfo('    SurfacePointsUsed initialised');

              repeat
                ndxSurfPnt := 0;
                while surfacePointsUsed[ndxSurfPnt] and (ndxSurfPnt < Surface.Count) do begin
                  ndxSurfPnt := ndxSurfPnt + 1;
                end;

                Log2Numbers('    Repeat loop, ndxSurfPnt', ndxSurfPnt, 'Surface.Count', Surface.Count);

                if ndxSurfPnt < Surface.Count then begin
                  LoopComplete := false;
                  newsurf := TList<point>.Create;
                  while (ndxSurfPnt < Surface.Count) and not surfacePointsUsed[ndxSurfPnt] do begin
                    NewSurf.Add(Surface[ndxSurfPnt]);
                    surfacePointsUsed[ndxSurfPnt] := true;
                    ndxSurfPnt := ndxSurfPnt + 1;
                    if IntersectFlags[ndxSurfPnt-1] and
                         ( surfacePointsUsed[ndxSurfPnt mod Surface.Count]  // next point is inside shadow
                            or
                           (PntInPly (MidPnt(Surface[ndxSurfPnt mod Surface.Count], Surface[ndxSurfPnt-1]), Shadow) = 1)    // side passes through shadow
                         )
                    then begin
                      // find point on shadow that matches ndxSurfPnt-1
                      if not FindMatchingVertex (Surface[ndxSurfPnt-1], Shadow, ndxShadPnt) then begin
                        {$REGION 'Logging logic'}
                        if ShowDebugInfo then begin
                          DateTimeToString (debugname, 'yymmddhhnnss', Now);
                          debugname := ExtractFileDir(ParamStr(0)) + '\\SubractShadowsFail-' + debugname + '.log';
                          fl := TFileStream.Create (debugname, fmCreate);
                          try
                            i := ndxSurfPnt-1;
                            fl.Write(i, 4);
                            fl.Write(Surface.Count, 4);
                            for i := 0 to Surface.Count-1 do begin
                              TempPnt := Surface[i];
                              fl.write (TempPnt.v, 24);
                            end;
                            fl.Write(Shadow.Count, 4);
                            for i := 0 to Shadow.Count-1 do begin
                              TempPnt := Shadow[i];
                              fl.write (TempPnt.v, 24);
                            end;
                          finally
                            fl.Destroy;
                          end;
                        end;
                        {$ENDREGION}
                        result := false;
                        exit;
                      end;

                      ndxShadPnt := ndxShadPnt+1;
                      if ndxShadPnt >= Shadow.Count then
                        ndxShadPnt := 0;
                      repeat
                        NewSurf.Add(Shadow[ndxShadPnt]);
                        if FindMatchingVertex (Shadow[ndxShadPnt], Surface, ndxSurfPnt) then begin
                          SurfacePointsUsed[ndxSurfPnt] := true;
                          ndxSurfPnt := ndxSurfPnt + 1;
                          ndxShadPnt := Shadow.count; // exit shadow loop and return to processing surface outline
                        end
                        else begin
                          ndxShadPnt := ndxShadPnt+1;
                          if ndxShadPnt >= Shadow.Count then
                            ndxShadPnt := 0;
                        end;

                      until ndxShadPnt >= Shadow.Count;

                    end;
                  end;
                  setlength (newSurfArray, NewSurf.Count);
                  for i := 0 to NewSurf.Count-1 do
                    newSurfArray[i] := NewSurf[i];
                  NewSurfaces.Add (NewSurfArray);
                end
                else
                  LoopComplete := true;

              until LoopComplete;
              LogDebugInfo('   LoopComplete');

              {$ENDREGION}
            end;  // num intersections >= 2

            if RemoveSurface then
              Surfaces.Delete(ndxSurf)
            else
              ndxSurf := ndxSurf+1;
          end;
          {$ENDREGION}
        end;  // while ndxSurf < Surfaces.Count
        while NewSurfaces.Count > 0 do begin
          Surfaces.Add(NewSurfaces[0]);
          NewSurfaces.Delete(0);
        end;
      finally
        Shadow.Free;
        Surface.Free;
        NewSurfaces.Free;
        IntersectFlags.Free;
      end;
    end;
  end;   // SubtractShadows

  function FixPct(pct : double) : double;
  begin
    if pct < 0.0001 then
      result := 0
    else if pct > 99.9999 then
      result := 100
    else
      result := pct;
  end;

  function TProcessThrd.ProcessSunPosition (altitude, azimuth : double;
                                            var pctGroup1shade,
                                                pctGroup2shade,
                                                pctTotalShade : double) : TProcessResult;
  // returns WrongFace if the sun is on the wrong side of the surface,
  // ProcessError if an unexpected error occurs, or ProcessOK if processed without error.
  var
    SunPoint, refpoint : point;
    g, i, j : integer;
    xAdj, yAdj,HeightProportion : double;
    Shadows : TList<ShadowTp>;
    SurfaceRemaining : TList<pntarr>;
    shadow : ShadowTp;
    plymin,plymax : point;
    SurfaceRemainingItem : pntarr;
    SurfaceArea, ShadowArea : double;
    debugname : string;
    fl : TFileStream;
    tmpPnt : point;
  begin
    result := ProcessOK;

    // set sunpoint at correct azimuth along the x axis relative to a reference point
    SunPoint.x := 1;
    SunPoint.y := 0;
    SunPoint.z := tan(altitude);
    // rotate around the z axis to be north of the origin
    SunPoint := RotatePoint (SunPoint, North, z);
    // rotate around the z axis by the azimuth (actually -azimuth since I'm using DataCAD angles which increase anti-clockwise
    SunPoint := RotatePoint (SunPoint, -azimuth, z);

    // make Sunpoint relative to a reference point
    refpoint := ReferencePoint;
    SunPoint.x := SunPoint.x + refpoint.x;
    SunPoint.y := SunPoint.y + refpoint.y;
    SunPoint.z := SunPoint.z + refpoint.z;

    //now apply the same transformations to the sunpoint & refernce point as we applied to the surface and entities
    for i := 0 to TransformHist.Count-1 do
      case TransformHist[i].transform of
        rotate :    begin
                      SunPoint := RotatePoint (SunPoint, TransformHist[i].ang,
                                               TransformHist[i].axis);
                      RefPoint := RotatePoint (RefPoint, TransformHist[i].ang,
                                               TransformHist[i].axis);
                    end;
        rotateRel : begin
                      SunPoint := RotatePointRel (SunPoint, TransformHist[i].relpnt,
                                                  TransformHist[i].ang,
                                                  TransformHist[i].axis);
                      RefPoint := RotatePointRel (RefPoint, TransformHist[i].relpnt,
                                                  TransformHist[i].ang,
                                                  TransformHist[i].axis);
                    end;
        move :      begin
                      SunPoint := MovePoint (SunPoint, TransformHist[i].vect);
                      RefPoint := MovePoint (RefPoint, TransformHist[i].vect);
                    end;
      end;

    // make SunPoint relative to the origin
    SunPoint.x := SunPoint.x - refpoint.x;
    SunPoint.y := SunPoint.y - refpoint.y;
    SunPoint.z := SunPoint.z - refpoint.z;

    if SunPoint.z <= 0 then begin
      // sun is on wrong side of surface polygon
      result := WrongFace;
      exit;
    end;

    PntsShadedG1 := 0;
    PntsShadedG2 := 0;
    PntsShadedG12 := 0;
    for i := 0 to length(ShadeFlags)-1 do begin
      for j := 0 to length(ShadeFlags[i])-1 do begin
        tmppnt.x := Xflagpos[i];
        tmppnt.y := Yflagpos[j];
        if ShadeFlags[i,j] <> OutsideSurface then
          ShadeFlags[i,j] := NotShaded;
      end;
    end;

    for g := 0 to Group1.Count-1 do begin
      SetLength (Shadow.ply, Length(Group1[g]));
      for j := 0 to Length (Group1[g])-1 do begin
        HeightProportion := Group1[g][j].z/SunPoint.z;
        Shadow.ply[j].x := Group1[g][j].x - HeightProportion * SunPoint.x;
        Shadow.ply[j].y := Group1[g][j].y - HeightProportion * SunPoint.y;
        Shadow.ply[j].z := 0;
      end;
      ply_extent(Shadow.ply, plymin, plymax);
      if (plymin.x > SurfMax.x) or (plymax.x < SurfMin.x) or
         (plymin.y > SurfMax.y) or (plymax.y < SurfMin.y) then
        // no overlap of extents, discard this polygon (do nothing with it)
      else begin
        for i := 0 to length(ShadeFlags)-1 do begin
          tmppnt.x := Xflagpos[i];
          if (tmppnt.x >= plymin.x) and (tmppnt.x <= plymax.x) then begin
            for j := 0 to length(ShadeFlags[i])-1 do begin
              tmppnt.y := Yflagpos[j];
              if (tmppnt.y >= plymin.y) and (tmppnt.y <= plymax.y) then begin
                if ShadeFlags[i,j] = NotShaded then begin
                  if PntInPly(tmppnt, Shadow.ply) >= 0 then begin
                    ShadeFlags[i,j] := ShadedG1;
                    PntsShadedG1 := PntsShadedG1+1;
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
    PntsShadedG12 := PntsShadedG1;

    for g := 0 to Group2.Count-1 do begin
      SetLength (Shadow.ply, Length(Group2[g]));
      for j := 0 to Length (Group2[g])-1 do begin
        HeightProportion := Group2[g][j].z/SunPoint.z;
        Shadow.ply[j].x := Group2[g][j].x - HeightProportion * SunPoint.x;
        Shadow.ply[j].y := Group2[g][j].y - HeightProportion * SunPoint.y;
        Shadow.ply[j].z := 0;
      end;
      ply_extent(Shadow.ply, plymin, plymax);
      if (plymin.x > SurfMax.x) or (plymax.x < SurfMin.x) or
         (plymin.y > SurfMax.y) or (plymax.y < SurfMin.y) then
        // no overlap of extents, discard this polygon (do nothing with it)
      else begin
        i := 0;
        while (i < length(ShadeFlags)) and (PntsShadedG2 < PntsInSurface) do begin
          tmppnt.x := Xflagpos[i];
          if (tmppnt.x >= plymin.x) and (tmppnt.x <= plymax.x) then begin
            j := 0;
            while (j < length(ShadeFlags[i])) and (PntsShadedG2 < PntsInSurface) do begin
              tmppnt.y := Yflagpos[j];
              if (tmppnt.y >= plymin.y) and (tmppnt.y <= plymax.y) then begin
                if ShadeFlags[i,j] in [NotShaded, ShadedG1] then begin
                  if PntInPly(tmppnt, Shadow.ply) >= 0 then begin
                    PntsShadedG2 := PntsShadedG2+1;
                    if ShadeFlags[i,j] = NotShaded then begin
                      ShadeFlags[i,j] := ShadedG2;
                      PntsShadedG12 := PntsShadedG12 + 1;
                    end
                    else
                      ShadeFlags[i,j] := ShadedG12;
                  end;
                end;
              end;
              j := j+1;
            end;
          end;
          i := i+1;
        end;
      end;
    end;

    pctGroup1shade := FixPct (100 * PntsShadedG1/PntsInSurface);
    pctGroup2shade := FixPct (100 * PntsShadedG2/PntsInSurface);
    pctTotalShade := FixPct (100 * PntsShadedG12/PntsInSurface);

{$REGION 'Commented out vector code'}
//    Shadows := TList<ShadowTp>.Create();
//    SurfaceArea := PolygonArea (Surface);
//
//    SurfaceRemaining := TList<pntarr>.Create();
//    try
//      {$REGION 'Process Group1'}
//      SurfaceRemaining.Add(Surface);
//      Shadows.Clear;
//      for i := 0 to Group1.Count-1 do begin
//        SetLength (Shadow.ply, Length(Group1[i]));
//        for j := 0 to Length (Group1[i])-1 do begin
//          HeightProportion := Group1[i][j].z/SunPoint.z;
//          Shadow.ply[j].x := Group1[i][j].x - HeightProportion * SunPoint.x;
//          Shadow.ply[j].y := Group1[i][j].y - HeightProportion * SunPoint.y;
//          Shadow.ply[j].z := 0;
//        end;
//        ply_extent(Shadow.ply, plymin, plymax);
//        if (plymin.x > SurfMax.x) or (plymax.x < SurfMin.x) or
//           (plymin.y > SurfMax.y) or (plymax.y < SurfMin.y) then
//          // no overlap of extents, discard this polygon (do nothing with it)
//        else begin
//          Shadow.area := PolygonArea (Shadow.ply);
//          Shadows.Add(Shadow);   // add to list of shadows for subsequent processing
//        end;
//      end;
//
//      {$REGION 'Debug Logic'}
//      if ShowDebugInfo then begin
//        debugname := ExtractFileDir(ParamStr(0)) + '\\G1Shadows.log';
//        fl := TFileStream.Create (debugname, fmCreate);
//        try
//          i := -2;    // not relevant to this debug file
//          fl.Write(i, 4);
//          i := length(Surface);
//          fl.Write(i, 4);
//          for i := 0 to length(Surface)-1 do begin
//            TempPnt := Surface[i];
//            fl.write (TempPnt.v, 24);
//          end;
//          for i := 0 to Shadows.Count-1 do begin
//            j := length(Shadows[i].ply);
//            fl.Write(j, 4);
//            for j := 0 to length(Shadows[i].ply)-1 do begin
//              TempPnt := Shadows[i].ply[j];
//              fl.write (TempPnt.v, 24);
//            end;
//          end;
//        finally
//          fl.Destroy;
//        end;
//      end;
//      {$ENDREGION}
//
//      if Shadows.Count = 0 then
//        pctGroup1shade := 0
//      else begin
//        // remove shadow areas from surface
//        if not SubtractShadows (SurfaceRemaining, Shadows) then begin
//          result := ProcessError;
//        end
//        else begin
//          ShadowArea := SurfaceArea;
//          for SurfaceRemainingItem in SurfaceRemaining do
//            ShadowArea := ShadowArea - PolygonArea(SurfaceRemainingItem);
//          pctGroup1shade := FixPct(100 * ShadowArea/SurfaceArea);
//
//          {$REGION 'Debug Logic'}
//          if ShowDebugInfo then begin
//            debugname := ExtractFileDir(ParamStr(0)) + '\\G1SurfaceLeft.log';
//            fl := TFileStream.Create (debugname, fmCreate);
//            try
//              i := -3;    // not relevant to this debug file
//              fl.Write(i, 4);
//              i := length(Surface);
//              fl.Write(i, 4);
//              for i := 0 to length(Surface)-1 do begin
//                TempPnt := Surface[i];
//                fl.write (TempPnt.v, 24);
//              end;
//              for i := 0 to SurfaceRemaining.Count-1 do begin
//                j := length(SurfaceRemaining[i]);
//                fl.Write(j, 4);
//                for j := 0 to length(SurfaceRemaining[i])-1 do begin
//                  TempPnt := SurfaceRemaining[i][j];
//                  fl.write (TempPnt.v, 24);
//                end;
//              end;
//            finally
//              fl.Destroy;
//            end;
//          end;
//          {$ENDREGION}
//
//
//
//        end;
//      end;
//      {$ENDREGION}
//
//      {$REGION 'Process Group2'}
//      SurfaceRemaining.Clear;
//      SurfaceRemaining.Add(Surface);
//      Shadows.Clear;
//      for i := 0 to Group2.Count-1 do begin
//        SetLength (Shadow.ply, Length(Group2[i]));
//        for j := 0 to Length (Group2[i])-1 do begin
//          Shadow.ply[j] := Group2[i][j];
//        end;
//        ply_extent(Shadow.ply, plymin, plymax);
//        if (plymin.x > SurfMax.x) or (plymax.x < SurfMin.x) or
//           (plymin.y > SurfMax.y) or (plymax.y < SurfMin.y) then
//          // no overlap of extents, discard this polygon (do nothing with it)
//        else begin
//          Shadow.area := PolygonArea (Shadow.ply);
//          Shadows.Add(Shadow);   // add to list of shadows for subsequent processing
//        end;
//      end;
//      if Shadows.Count = 0 then
//        pctGroup1shade := 0
//      else begin
//        // remove shadow areas from surface
//        if not SubtractShadows (SurfaceRemaining, Shadows) then begin
//          result := ProcessError;
//        end
//        else begin
//          ShadowArea := SurfaceArea;
//          for SurfaceRemainingItem in SurfaceRemaining do
//            ShadowArea := ShadowArea - PolygonArea(SurfaceRemainingItem);
//          pctGroup2shade := FixPct(100 * ShadowArea/SurfaceArea);
//          {$REGION 'Debug Logic'}
//          if ShowDebugInfo then begin
//            debugname := ExtractFileDir(ParamStr(0)) + '\\G2SurfaceLeft.log';
//            fl := TFileStream.Create (debugname, fmCreate);
//            try
//              i := -3;    // not relevant to this debug file
//              fl.Write(i, 4);
//              i := length(Surface);
//              fl.Write(i, 4);
//              for i := 0 to length(Surface)-1 do begin
//                TempPnt := Surface[i];
//                fl.write (TempPnt.v, 24);
//              end;
//              for i := 0 to SurfaceRemaining.Count-1 do begin
//                j := length(SurfaceRemaining[i]);
//                fl.Write(j, 4);
//                for j := 0 to length(SurfaceRemaining[i])-1 do begin
//                  TempPnt := SurfaceRemaining[i][j];
//                  fl.write (TempPnt.v, 24);
//                end;
//              end;
//            finally
//              fl.Destroy;
//            end;
//          end;
//          {$ENDREGION}
//
//        end;
//      end;
//      {$ENDREGION}
//
//      {$REGION 'Process Combined Groups'}
//      // start with SurfaceRemaining as it was after processing Group2 and process
//      // group1 against that
//      LogDebugInfo('Starting Combined Groups');
//      Shadows.Clear;
//      for i := 0 to Group1.Count-1 do begin
//        SetLength (Shadow.ply, Length(Group1[i]));
//        for j := 0 to Length (Group1[i])-1 do begin
//          Shadow.ply[j] := Group1[i][j];
//        end;
//        ply_extent(Shadow.ply, plymin, plymax);
//        if (plymin.x > SurfMax.x) or (plymax.x < SurfMin.x) or
//           (plymin.y > SurfMax.y) or (plymax.y < SurfMin.y) then
//          // no overlap of extents, discard this polygon (do nothing with it)
//        else begin
//          Shadow.area := PolygonArea (Shadow.ply);
//          Shadows.Add(Shadow);   // add to list of shadows for subsequent processing
//        end;
//      end;
//      if Shadows.Count = 0 then
//        pctTotalshade := 0
//      else begin
//        LogDebugInfo(' Shadows to Remove: subract Shadows');
//        Log2Numbers(' SurfaceRemaining.count', SurfaceRemaining.Count, 'Shadows.count', Shadows.Count);
//        // remove shadow areas from surface
//        if not SubtractShadows (SurfaceRemaining, Shadows) then begin
//          result := ProcessError;
//        end
//        else begin
//          LogDebugInfo('  Subtract successful');
//          ShadowArea := SurfaceArea;
//          for SurfaceRemainingItem in SurfaceRemaining do
//            ShadowArea := ShadowArea - PolygonArea(SurfaceRemainingItem);
//          pctTotalShade := FixPct(100 * ShadowArea/SurfaceArea);
//        end;
//      end;
//      {$ENDREGION}
//
//    finally
//      Shadows.Free;
//      SurfaceRemaining.Free;
//    end;
{$ENDREGION}

  end;  // ProcessSunPosition


  procedure TProcessThrd.setValues (pStartDate, pStopDate : TDateTime;
                                    pStartNdx, pStopNdx, pIncrement, pFreqNdx : integer);
  begin
    StartDate := pStartDate;
    StopDate := pStopDate;
    StartNdx := pStartNdx;
    StopNdx := pStopNdx;
    Increment := pIncrement;
    FrequencyNdx := pFreqNdx;
  end;


  procedure TProcessThrd.Execute;
  var
    thisdate, time, sunrise, sunset, stoptime, solarnoon : TDateTime;
    specificDates : boolean;
    ndx : integer;
    s, s1 : string;
    altitude, azimuth : double;
    pctA, pctB, pctTot : double;
    ProcessResult : TProcessResult;
    xdiv, ydiv : double;
    i, j : integer;
    tmppnt : point;
    RowNum : integer;
    CellRow : variant;
    template : string;
    ExistingItem : boolean;
    GridDis, GridMinDiv : double;
  const
    MaxGridDivs = 46340;
  begin

    CoInitialize(nil);

    if ShowDebugInfo then begin
      AssignFile (debugfile, 'ProcessDebug.log');
      Rewrite(debugfile);
    end;

    try
      ply_extent (surface, SurfMin, SurfMax);

      // set up ShadFlags
      Case SpeedSetting of
        0 : begin
              GridDis := 192;
              GridMinDiv := 100;
            end;
        1 : begin
              GridDis := 156;
              GridMinDiv := 123;
            end;
        2 : begin
              GridDis := 128;
              GridMinDiv := 150;
            end;
        3 : begin
              GridDis := 92;
              GridMinDiv := 184;
            end;
        4 : begin
              GridDis := 72;
              GridMinDiv := 225;
            end;
        5 : begin
              GridDis := 58;
              GridMinDiv := 275;
            end;
        6 : begin
              GridDis := 48;
              GridMinDiv := 340;
            end;
        7 : begin
              GridDis := 39;
              GridMinDiv := 415;
            end;
        8 : begin
              GridDis := 32;
              GridMinDiv := 500;
            end;
        9 : begin
              GridDis := 28;
              GridMinDiv := 650;
            end;
      End;
      setlength (Xflagpos, Min(round(Max((SurfMax.x - SurfMin.x)/GridDis, GridMinDiv)),MaxGridDivs));
      xdiv := (SurfMax.x - SurfMin.x)/length(Xflagpos);
      Xflagpos[0] := SurfMin.x + xdiv/2;
      for i := 1 to length(XFlagpos)-1 do
        Xflagpos[i] := Xflagpos[i-1] + xdiv;
      setlength (Yflagpos, Min(round(Max((SurfMax.y - SurfMin.y)/GridDis, GridMinDiv)),MaxGridDivs));
      ydiv := (SurfMax.y - SurfMin.y)/length(Yflagpos);
      Yflagpos[0] := SurfMin.y + ydiv/2;
      for i := 1 to length(YFlagpos)-1 do
        Yflagpos[i] := Yflagpos[i-1] + ydiv;
      setlength (ShadeFlags, length(Xflagpos));
      tmppnt.z := 0;
      PntsInSurface := 0;
      for i := 0 to length(ShadeFlags)-1 do begin
        setlength (ShadeFlags[i], length(Yflagpos));
        for j := 0 to length(ShadeFlags[i])-1 do begin
          tmppnt.x := Xflagpos[i];
          tmppnt.y := Yflagpos[j];
          if PntInPly(tmppnt, surface) > 0 then begin
            ShadeFlags[i,j] := NotShaded;
            PntsInSurface := PntsInSurface+1;
          end
          else
            ShadeFlags[i,j] := OutsideSurface;
        end;
      end;

      if usingExcel then begin
        RowNum := 2;

        try
          ExcelApplication := GetActiveOleObject('Excel.Application');
          ExistingExcel := true;
        except
          ExcelApplication := CreateOleObject('Excel.Application');
          ExistingExcel := false;
        end;

        if not FileExists(FileName) then begin
          if Group2.Count > 0 then
            s :=  ExtractFilePath (ParamStr(0)) +  'DefaultShadStat2.xlsx'
          else
            s :=  ExtractFilePath (ParamStr(0)) +  'DefaultShadStat1.xlsx';
          if not CopyFile (PWideChar (s), PWideChar (FileName), true) then begin
            messagedlg ('Failed to copy default file (' + s + ')' + sLineBreak + sLineBreak + 'Processing cancelled',
                         mtError, [mbOK], 0);
            exit;
          end;
        end;
        try
          wb := ExcelApplication.Workbooks.Item(FileName);
          ExistingItem := true;
        except
          wb := ExcelApplication.Workbooks.Open(FileName);
          ExistingItem := false;
        end;

        if FileExistsResult = mrAppend then begin
          if NewSheet then begin
            if Group2.Count > 0 then
              template :=  ExtractFileDir(ParamStr(0)) + '\\DefaultShadStat2.xltx'
            else
              template :=  ExtractFileDir(ParamStr(0)) + '\\DefaultShadStat1.xltx';
            ws := wb.Sheets.Add (After := wb.sheets[wb.Sheets.Count], Type := template);
            if length (SheetName) > 0 then
              ws.Name := SheetName;
          end
          else begin
            ws := wb.Sheets[SheetName];
            RowNum :=ws.UsedRange.Rows.Count + 2;
          end;

        end
        else
          ws := wb.Worksheets[1];

        ExcelApplication.Visible := true;
      end;

      specificDates := (length(dates) > 0);
      if specificDates then begin
        ndx := 0;
        ThisDate := Dates[0];
      end
      else
        ThisDate := StartDate;

      while (((ThisDate < StopDate) and (not specificDates)) or
             (specificDates and (ndx < Length(dates)))) and
            (not StopRequested) do begin
        DateTimeToString(s, 'dd-mmm-yy', ThisDate);
        memStatus.Lines.Add ('');
        memStatus.Lines.Add (' Processing ' + s);

        if ShowDebugInfo or (startndx = 0) or (stopndx = 0) or solarAnchor then begin
          CalcSunriseSunset (Latitude, Longitude, ThisDate, TimeZone, sunrise, sunset);
          DateTimeToString (s, 'hh:mm:ss', sunrise);
          DateTimeToString (s1, 'hh:mm:ss', sunset);
          memStatus.Lines.Add('   Sunrise/set: ' + s + ' / ' + s1);
          solarnoon := (sunrise + sunset)/2;
        end
        else
          solarnoon := ThisDate + 0.5;

        if (startndx = 0) then begin
          time := ThisDate;
          if solarAnchor then begin
            time := solarnoon;
            repeat
              time := IncMinute (time, -increment)
            until time <= sunrise;
            time := IncMinute(time, increment);
          end
          else repeat
            time := IncMinute(time, increment);
          until time > sunrise;
        end
        else begin
          if solarAnchor then begin
            time := solarnoon;
            repeat
              time := IncMinute (time, -increment)
            until time < IncMinute(ThisDate, (startndx+15)*15);
            time := IncMinute(time, increment);
          end
          else
            time := IncMinute(ThisDate, (startndx+15)*15);
        end;
        if (stopndx = 0) then
          stoptime := sunset
        else
          stoptime := incMinute(ThisDate, (stopndx+27)*15);

        while (time <= stoptime) and (not StopRequested) do begin
          CalcSun (time, altitude, azimuth);
          if ShowDebugInfo then begin
            DateTimeToString(s, '   hh:nn', time);
            s := s + '  Altitude=' + FloatToStr(altitude);
            s := s + '  Azimuth=' + FloatToStr(azimuth);
            if (altitude >= MinAltitude) then
              memStatus.Lines.Add(s)
            else begin
              s := s + ' (not processed - ';
              if (altitude < 0) then
                s := s + 'sun below horizon)'
              else
                s := s + 'below minimum altitude)';
              memStatus.Lines.Add(s);
            end;
          end;
          if altitude >= MinAltitude then begin

            ProcessResult := ProcessSunPosition (altitude*Pi/180, azimuth*Pi/180, // note: convert degrees to radians for this call
                                pctA, pctB, pctTot);
            if usingExcel then begin
              ws.Cells[RowNum, 1] := DateOf(time);
              ws.Cells[RowNum, 2] := TimeOf(time);
              if solarAnchor and RealEqual (time, solarnoon) then begin
                ws.Cells[RowNum, 2].AddComment ('Solar Noon');
              end;
              ws.Cells[RowNum, 3] := pctA/100;
              ws.Cells[RowNum, 3].NumberFormat := '0.0%';
              if Group2.Count > 0 then begin
                ws.Cells[RowNum, 4] := pctB/100;
                ws.Cells[RowNum, 4].NumberFormat := '0.0%';
                ws.Cells[RowNum, 5] := pctTot/100;
                ws.Cells[RowNum, 5].NumberFormat := '0.0%';
                ws.Cells[RowNum, 6] := '=E' + inttostr(RowNum) + '-C' + inttostr(RowNum);
                ws.Cells[RowNum, 6].NumberFormat := '0.0%';
              end;


              RowNum := RowNum + 1;
            end
            else begin
              DateTimeToString(s, 'dd-mmm-yy, hh:mm', time);
              if ProcessResult = ProcessOK then begin
                if Group2.Count > 0 then
                  WriteLn (csvFile, s + ',' + FloatToStr(round(pctA*1000)/1000) + '%,'
                                            + FloatToStr(round(pctB*1000)/1000) + '%,'
                                            + FloatToStr(round(pctTot*1000)/1000) +'%,'
                                            + FloatToStr(round((pctTot-pctA)*1000)/1000) +'%')
                else
                  WriteLn (csvFile, s + ',' + FloatToStr(round(pctA*1000)/1000) + '%');
              end
              else if ProcessResult = WrongFace then
                WriteLn (csvFile, s + ',Sun not shining on selected face')
              else
                WriteLn (csvFile, s + ',Processing Error');
            end;
          end;

          time := IncMinute(time, increment);
        end;


        if specificDates then begin
          Ndx := ndx + 1;
          if ndx < Length (Dates) then
            ThisDate := Dates[ndx];
        end
        else case frequencyndx of
          0 : ThisDate := IncDay(ThisDate, 1);
          1 : ThisDate := IncDay(ThisDate, 2);
          2 : ThisDate := IncWeek(ThisDate, 1);
          3 : ThisDate := IncMonth(ThisDate, 1)
          else ThisDate := IncDay(ThisDate, 1);
        end;

        if BlankLines then begin
          if usingExcel then
            RowNum := RowNum + 1
          else
             WriteLn (csvFile);
        end;
      end;

    finally

      if ShowDebugInfo then
        CloseFile (debugfile);
      try
        if usingExcel then begin
          wb.save;
          ExcelApplication := Unassigned;
          wb := Unassigned;
          ws := Unassigned;
        end
        else begin
          CloseFile(csvFile);
        end;
      except

      end;

      Buttons[1].Enabled := true;
      Buttons[2].Enabled := false;
      Buttons[3].Enabled := true;

      if StopRequested then
        memStatus.Lines.Add('Stopped by user')
      else
        MessageDlg('Shadow Processing Complete', mtInformation, [mbOK], 0);


      memStatus.Lines.Add('');
    end;

    Terminate;

  end;


end.
