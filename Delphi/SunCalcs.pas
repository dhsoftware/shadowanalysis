unit SunCalcs;

interface

  uses SysUtils, DateUtils, Math, CommonStuff, Vcl.Dialogs, System.UITypes;

  procedure  CalcSunriseSunset (latitude, longitude : double;
                                date : TDateTime;
                                timezone : double;
                                var sunrise, sunset : TDateTime);

  procedure CalcSun (shadDateTime : tDateTime;
                     var altitude : double; var azimuth : double);

  function calcKeyDate( k : KeyDateTp; year : integer ) : TDateTime;


implementation

  function calcInitial( k : KeyDateTp; year : integer ) : double;
  var
    Y : double;
  begin
    Y :=( year-2000)/1000;
    case k of
      SummerSolstice: result := 2451900.05952 + 365242.74049*Y - 0.06223*IntPower(Y, 2) - 0.00823*IntPower(Y, 3) + 0.00032*IntPower(Y, 4);
      SpringEquinox : result := 2451810.21715 + 365242.01767*Y - 0.11575*IntPower(Y, 2) + 0.00337*IntPower(Y, 3) + 0.00078*IntPower(Y, 4);
      WinterSolstice: result := 2451716.56767 + 365241.62603*Y + 0.00325*IntPower(Y, 2) + 0.00888*IntPower(Y, 3) - 0.00030*IntPower(Y, 4);
      AutumnEquinox : result := 2451623.80984 + 365242.37404*Y + 0.05169*IntPower(Y, 2) - 0.00411*IntPower(Y, 3) - 0.00057*IntPower(Y, 4);
      else result := 0;
    end;
  end;


  function periodic24( T : double ) : double;
  const
	  A : Array [0..23] of integer = (485,203,199,182,156,136,77,74,70,58,52,50,45,44,29,18,17,16,14,12,12,12,9,8);
	  B : Array [0..23] of double = (324.96,337.23,342.08,27.85,73.14,171.52,222.54,296.72,243.58,119.81,297.17,21.02,
			                             247.54,325.15,60.93,155.12,288.79,198.04,199.76,95.39,287.11,320.81,227.73,15.45);
	  C : Array [0..23] of double = (1934.136,32964.467,20.186,445267.112,45036.886,22518.443,
                                   65928.934,3034.906,9037.513,33718.147,150.678,2281.226,
                                   29929.562,31555.956,4443.417,67555.328,4562.452,62894.029,
                                   31436.921,14577.848,31931.756,34777.259,1222.114,16859.074);
	var
    i : integer;
  begin
    result := 0;
    for i := 0 to 23 do
      result := result + A[i]*Cos((B[i] + (C[i]*T))*Pi/180 );
  end;



  function calcKeyDate( k : KeyDateTp; year : integer ) : TDateTime;
	var
    JDE, JDE0, T, W, dL, S : double;
  begin
    if year < 2020 then
      year := 2020
    else if year > 3000 then
      year := 3000;

	  JDE0 := calcInitial( k, year);
	  T := ( JDE0 - 2451545.0) / 36525;
	  W := 35999.373*T - 2.47;
	  dL := 1 + 0.0334*cos(W * Pi/180) + 0.0007*Cos(2*W * Pi/180);
	  S := periodic24( T );
	  JDE := JDE0 + ( (0.00001*S) / dL ); 	// This is the answer in Julian Ephemeris Days
    result  := IncMinute (JulianDateToDateTime (JDE), Round(TimeZone*60));
	end;


  FUNCTION isDaylightSaving (date : TDateTime) : boolean;
  VAR
    y, m, d : word;
  BEGIN
    result := useDS;
    if result then begin
      DecodeDate (date, y, m, d);
      if y <> DS_Year then begin
        {$REGION 'Calc DS start/end dates for correct year'}
        DS_Year := y;
        case DS_Start_Ndx of
          0 : begin  // second Sunday in March
                DS_Start := EncodeDate (y, 3, 14);
                while DayOfWeek(DS_Start) <> 1 do
                  DS_Start := IncDay (DS_Start, -1);
              end;
          1 : begin  // Friday before last Sunday in March
                DS_Start := EncodeDate (y, 3, 31);
                while DayOfWeek(DS_Start) <> 1 do
                  DS_Start := IncDay (DS_Start, -1);
                DS_Start := IncDay (DS_Start, -2);
              end;
          2 : begin  // Last Friday in March
                DS_Start := EncodeDate (y, 3, 31);
                while DayOfWeek(DS_Start) <> 1 do
                  DS_Start := IncDay (DS_Start, -1);
              end;
          3 : begin  // Last Sunday in March
                DS_Start := EncodeDate (y, 3, 31);
                while DayOfWeek(DS_Start) <> 1 do
                  DS_Start := IncDay (DS_Start, -1);
              end;
          4 : begin  // First Sunday in April
                DS_Start := EncodeDate (y, 4, 1);
                while DayOfWeek(DS_Start) <> 1 do
                  DS_Start := IncDay (DS_Start, 1);
              end;
          5 : begin  // First Sunday in September
                DS_Start := EncodeDate (y, 9, 1);
                while DayOfWeek(DS_Start) <> 1 do
                  DS_Start := IncDay (DS_Start, 1);
              end;
          6 : begin  // Last Sunday in September
                DS_Start := EncodeDate (y, 9, 30);
                while DayOfWeek(DS_Start) <> 1 do
                  DS_Start := IncDay (DS_Start, -1);
              end;
          7 : begin  // First Sunday in October
                DS_Start := EncodeDate (y, 10, 1);
                while DayOfWeek(DS_Start) <> 1 do
                  DS_Start := IncDay (DS_Start, 1);
              end;
          8 : begin  // First Sunday in November
                DS_Start := EncodeDate (y, 11, 1);
                while DayOfWeek(DS_Start) <> 1 do
                  DS_Start := IncDay (DS_Start, 1);
              end
          else result := false;
        end;
        if useDS then case DS_End_Ndx of
          0 : begin // Third Sunday in January
                DS_End := EncodeDate (y, 1, 15);
                while DayOfWeek(DS_End) <> 1 do
                  DS_End := IncDay (DS_End, 1);
              end;
          1 : begin // Fourth Sunday in March
                DS_End := EncodeDate (y, 3, 22);
                while DayOfWeek(DS_End) <> 1 do
                  DS_End := IncDay (DS_End, 1);
              end;
          2 : begin // First Sunday in April
                DS_End := EncodeDate (y, 4, 1);
                while DayOfWeek(DS_End) <> 1 do
                  DS_End := IncDay (DS_End, 1);
              end;
          3 : begin // Last Friday  in October
                DS_End := EncodeDate (y, 10, 31);
                while DayOfWeek(DS_End) <> 6 do
                  DS_End := IncDay (DS_End, -1);
              end;
          4 : begin // Last Sunday in October
                DS_End := EncodeDate (y, 10, 31);
                while DayOfWeek(DS_End) <> 1 do
                  DS_End := IncDay (DS_End, -1);
              end;
          5 : begin // First Sunday in November
                DS_End := EncodeDate (y, 11, 1);
                while DayOfWeek(DS_End) <> 1 do
                  DS_End := IncDay (DS_End, 1);
              end
          else result := false;
        end;
        {$ENDREGION}
      end;
    end;
    if result then begin
      if (DS_Start < DS_End) then begin
        result := (date >= DS_Start) and (date < DS_End);
      end
      else if (DS_Start > DS_End) then begin
        result := (date >= DS_Start) or (date < DS_End);
      end;
    end;

  END;

  FUNCTION calcGeomMeanAnomalySun(t: double) : double;
  BEGIN
    result := 357.52911 + t * (35999.05029 - 0.0001537 * t);
  END;

  function calcEccentricityEarthOrbit(t: double): double;
  BEGIN
    result := 0.016708634 - t * (0.000042037 + 0.0000001267 * t);
  END;


  FUNCTION calcGeomMeanLongSun(t:double) : double;
  VAR
    Lo  : double;
  BEGIN
      Lo := 280.46646 + t * (36000.76983 + 0.0003032 * t);
      while(Lo > 360.0) do
        Lo := Lo - 360.0;
      while (Lo < 0.0) do
        Lo := Lo + 360.0;
      result := Lo;		// in degrees
  END;

  FUNCTION calcSunEqOfCenter(t : double) : double;
  VAR
    m, mrad ,sinm,
    sin2m, sin3m  : real;
  BEGIN
     m := calcGeomMeanAnomalySun(t);

     mrad := degToRad(m);
     sinm := sin(mrad);
     sin2m := sin(mrad+mrad);
     sin3m := sin(mrad+mrad+mrad);

     result := sinm * (1.914602 - t * (0.004817 + 0.000014 * t))
                  + sin2m * (0.019993 - 0.000101 * t) + sin3m * 0.000289;
  END;

  FUNCTION calcSunTrueLong(t : double) : double;
  VAR
    Lo, c : double;
  BEGIN
    Lo := calcGeomMeanLongSun(t);
    c := calcSunEqOfCenter(t);

    result := Lo + c;
  END;

  FUNCTION calcSunApparentLong(t : double) : double;
  VAR
    o, omega : double;
  BEGIN
    o := calcSunTrueLong(t);
    omega := 125.04 - 1934.136 * t;
    result := o - 0.00569 - 0.00478 * sin(degToRad(omega));
  END;


  FUNCTION calcMeanObliquityOfEcliptic(t : double): double;
  VAR
    seconds : double;
  BEGIN
      seconds := 21.448 - t*(46.8150 + t*(0.00059 - t*(0.001813)));
      result := 23.0 + (26.0 + (seconds/60.0))/60.0;
  END;

  FUNCTION calcObliquityCorrection(t:double) : double;
  VAR
    e0, omega : double;
  BEGIN
      e0 := calcMeanObliquityOfEcliptic(t);
      omega := 125.04 - 1934.136 * t;
      result := e0 + 0.00256 * cos(degToRad(omega));
  END;

  FUNCTION calcSunDeclination(t : double) : double;
  VAR
    e,
    lambda,
    sint  : double;
  BEGIN
    e := calcObliquityCorrection(t);
    lambda := calcSunApparentLong(t);

    sint := sin(degToRad(e)) * sin(degToRad(lambda));
    result := radToDeg(arcsin(sint));
  END;

	function calcHourAngleSunrise(lat, solarDec : double) : double;
  // calculate the hour angle (in radians) of the sun at sunrise for the latitude
  var
    latRad, sdRad : double;
	begin
		latRad := degToRad(lat);
		sdRad  := degToRad(solarDec);

		result := (arccos(cos(degToRad(90.833))/(cos(latRad)*cos(sdRad))-tan(latRad) * tan(sdRad)));

	end;

	function calcHourAngleSunset(lat, solarDec : double) : double;
  // calculate the hour angle of the sun at sunset for the latitude
  var
    latRad, sdRad : double;
	begin
		latRad := degToRad(lat);
		sdRad  := degToRad(solarDec);

		result := -(arccos(cos(degToRad(90.833))/(cos(latRad)*cos(sdRad))-tan(latRad) * tan(sdRad)));

	end;

	function calcTimeJulianCent(jd : double) : double;
	begin
		result := (jd - 2451545.0)/36525.0;
	end;

  FUNCTION CalcEquationOfTime(t: double) : double;
  VAR
    epsilon,sinm,
    cos2Lo,
    Lo,
    e,
    m,
    y,
    sin2Lo,
    sin4Lo,
    sin2m,
    Etime : double;

  BEGIN
    epsilon := calcObliquityCorrection(t);
    Lo := calcGeomMeanLongSun(t);
    e := calcEccentricityEarthOrbit(t);
    m := calcGeomMeanAnomalySun(t);

    y := tan(degToRad(epsilon)/2.0);
    y := y * y;

    sin2Lo := sin(2.0 * degToRad(Lo));
    sinm := sin(degToRad(m));
    cos2Lo := cos(2.0 * degToRad(Lo));
    sin4Lo := sin(4.0 * degToRad(Lo));
    sin2m  := sin(2.0 * degToRad(m));

    Etime := y * sin2Lo - 2.0 * e * sinm + 4.0 * e * y * sinm * cos2Lo
        - 0.5 * y * y * sin4Lo - 1.25 * e * e * sin2m;

    Result :=  radToDeg(Etime)*4.0;	// in minutes of time
  END;


  function calcJD(date : TDateTime) : double;
  var
    d, m, y : word;
    a, b : double;
  begin
    DecodeDate (date, y, m, d);
    if (m <= 2) then begin
      y := y-1;
      m := m+12;
    end;
    a := Floor(y/100);
    b := 2 - a + Floor(a/4);

    result := Floor(365.25*(y + 4716)) + Floor(30.6001*(m+1)) + d + b - 1524.5;
  end;  //calcJD


	function calcJDFromJulianCent(t : double) : double;
  // convert centuries since J2000.0 to Julian Day
	begin
		result := t * 36525.0 + 2451545.0;
	end;

	function calcSolNoonUTC(t, longitude : double) : double;
  // calculate the Universal Coordinated Time (UTC) of solar noon for the given day at the given location on earth
  var
    tnoon, eqTime, newt : double;
	begin
		// First pass uses approximate solar noon to calculate eqtime
		tnoon := calcTimeJulianCent(calcJDFromJulianCent(t) - longitude/360.0);
		eqTime := calcEquationOfTime(tnoon);
		result := 720 - (longitude * 4) - eqTime; // min

	  newt := calcTimeJulianCent(calcJDFromJulianCent(t) -0.5 + result/1440.0);

		eqTime := calcEquationOfTime(newt);
		result := 720 - (longitude * 4) - eqTime; // min
	end; //calcSolNoonUTC

  function calcSunriseUTC(JD, latitude, longitude : double) : double;
  // return sunrise in minutes since midnight
  var
    t, noonmin, tnoon, eqTime, solarDec, hourAngle,
    delta, timeDiff, timeUTC, newT : double;
  begin
    try
      t := calcTimeJulianCent(JD);

      // Find the time of solar noon at the location, and use
      // that declination. This is better than start of the
      // Julian day

      noonmin := calcSolNoonUTC(t, longitude);
      tnoon := calcTimeJulianCent (JD+noonmin/1440.0);

      // First pass to approximate sunrise (using solar noon)

      eqTime := calcEquationOfTime(tnoon);
      solarDec := calcSunDeclination(tnoon);
      hourAngle := calcHourAngleSunrise(latitude, solarDec);

      delta := -longitude - radToDeg(hourAngle);
      timeDiff := 4 * delta;	// in minutes of time
      timeUTC := 720 + timeDiff - eqTime;	// in minutes

      // Second pass includes fractional jday in gamma calc

      newt := calcTimeJulianCent(calcJDFromJulianCent(t) + timeUTC/1440.0);
      eqTime := calcEquationOfTime(newt);
      solarDec := calcSunDeclination(newt);
      hourAngle := calcHourAngleSunrise(latitude, solarDec);
      delta := -longitude - radToDeg(hourAngle);
      timeDiff := 4 * delta;

      result := 720 + timeDiff - eqTime; // in minutes
    except
      result := 0;
    end;
  end;

	function calcSunsetUTC(JD, latitude, longitude : double) : double;
  // calculate the UTC of sunset	for the given day at the given location on earth
  var
    t, noonmin, tnoon, eqTime, solarDec, hourAngle,
    delta, timeDiff, timeUTC, newT : double;
	begin
    try
      t := calcTimeJulianCent(JD);

      // Find the time of solar noon at the location, and use
      // that declination. This is better than start of the
      // Julian day

      noonmin := calcSolNoonUTC(t, longitude);
      tnoon := calcTimeJulianCent (JD+noonmin/1440.0);

      // First calculates sunrise and approx length of day

      eqTime := calcEquationOfTime(tnoon);
      solarDec := calcSunDeclination(tnoon);
      hourAngle := calcHourAngleSunset(latitude, solarDec);

      delta := -longitude - radToDeg(hourAngle);
      timeDiff := 4 * delta;
      timeUTC := 720 + timeDiff - eqTime;

      // first pass used to include fractional day in gamma calc

      newt := calcTimeJulianCent(calcJDFromJulianCent(t) + timeUTC/1440.0);
      eqTime := calcEquationOfTime(newt);
      solarDec := calcSunDeclination(newt);
      hourAngle := calcHourAngleSunset(latitude, solarDec);

      delta := -longitude - radToDeg(hourAngle);
      timeDiff := 4 * delta;
      result := 720 + timeDiff - eqTime; // in minutes
    except
      result := 1440;
    end;
	end;



  procedure  CalcSunriseSunset (latitude, longitude : double;
                                date : TDateTime;
                                timezone : double;
                                var sunrise, sunset : TDateTime);
  var
    jd, risetimeGMT, settimeGMT : double;
  begin
    // ensure latitude is not within 1 degree of poles
    if abs(latitude) > 89 then begin
      MessageDlg('Cannot calculate for specified latitude' + sLineBreak +
                 'Latitude used in calculations has been adjusted to 89', mtWarning, [mbOK], 0);
      if latitude > 89 then
        latitude := 89
      else if latitude < -89 then
        latitude := -89;
    end;

    jd :=  calcJD (date);

//		T := calcTimeJulianCent(JD);

		//solarDec := calcSunDeclination(T);
//		eqTime := calcEquationOfTime(T);

		riseTimeGMT := calcSunriseUTC(JD, latitude, longitude);
		setTimeGMT := calcSunsetUTC(JD, latitude, longitude);

    sunrise := IncSecond (Trunc(date), round((timezone*60 + risetimeGMT)*60));
    sunset := IncSecond (Trunc(date), round((timezone*60 + settimeGMT)*60));

    if isDaylightSaving (date) then begin
      sunrise := IncMinute(sunrise, 60);
      sunset := IncMinute(sunset, 60);
    end;


  end;  // CalcSunriseSunset




  PROCEDURE CalcSun (shadDateTime : tDateTime;
                     var altitude : double; var azimuth : double);
  VAR
    jd,
    T,
    eqTime,
    solarTimeFix,
    trueSolarTime,
    hourangle,
    haRad,
    csz,
    zenith,
    solarDec,
    azDenom,
    azRad,
    exoatmElevation,
    te, refractionCorrection,
    solarZen   : double;
    y, m, d : word;
  BEGIN
    // adjust for daylight saving if necessary
    if isDaylightSaving (shadDateTime) then begin
      shadDateTime := IncMinute (shadDateTime, -60);
    end;

    jd := DateTimeToJulianDate (shadDateTime);

    T := (jd - TimeZone/24 - 2451545.0)/36525.0;

    eqTime := calcEquationOfTime(T);
    solarDec := calcSunDeclination(T);
    solarTimeFix := eqTime + 4.0 * longitude - 60.0 * TimeZone;
    trueSolarTime := TimeOf(shadDateTime) * 1440 + solarTimeFix;
    while trueSolarTime > 1440 do
      trueSolarTime := TrueSolarTime - 1440;
    hourAngle := trueSolarTime / 4.0 - 180.0;
    if (hourAngle < -180) then
       hourAngle := hourAngle + 360.0;

    haRad := degToRad(hourAngle);

    csz := sin(degToRad(latitude)) * sin(degToRad(solarDec)) +
           cos(degToRad(latitude)) * cos(degToRad(solarDec)) * cos(haRad);
    if (csz > 1.0) then
      csz := 1.0
    else if (csz < -1.0) then
      csz := -1.0;

    zenith := radToDeg(arccos(csz));

    azDenom := cos(degToRad(latitude)) * sin(degToRad(zenith));
    if (abs(azDenom) > 0.001) then
    begin
      azRad := ((sin(degToRad(latitude)) * cos(degToRad(zenith)) )
                  - sin(degToRad(solarDec))) / azDenom;
      if (abs(azRad) > 1.0) then
      begin
        if (azRad < 0) then
          azRad := -1.0
        else
          azRad := 1.0;
      end;

      azimuth := 180.0 - radToDeg(arccos(azRad));

      if (hourAngle > 0.0) then
        azimuth := -azimuth;
    end
    else
    begin
      if (latitude > 0.0) then
        azimuth := 180.0
      else
        azimuth := 0.0;
    end;
    if (azimuth < 0.0) then
      azimuth := azimuth + 360.0;


    exoatmElevation := 90.0 - zenith;
    if (exoatmElevation > 85.0) then
      refractionCorrection := 0.0
    else begin
      te := tan (degToRad(exoatmElevation));
      if (exoatmElevation > 5.0) then
        refractionCorrection := 58.1 / te - 0.07 / intpower(te, 3)
                        + 0.000086 / intpower(te, 5)
      else if (exoatmElevation > -0.575) then
        refractionCorrection := 1735.0 + exoatmElevation *
                (-518.2 + exoatmElevation * (103.4 +
                exoatmElevation * (-12.79 +
                exoatmElevation * 0.711) ) )
      else
        refractionCorrection := -20.774 / te;

      refractionCorrection := refractionCorrection / 3600.0;
    end;

    solarZen := zenith - refractionCorrection;

    altitude := 90 - solarZen;
  END;


end.
