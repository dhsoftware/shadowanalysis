unit ShadowStat;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Geometry, About,
  CommonStuff, System.Generics.Collections, Vcl.ComCtrls, inifiles,  ActiveX, ComObj,
  Vcl.Samples.Spin, DateUtils, Math, Vcl.Menus, System.UITypes, SunCalcs, Process,
  Vcl.Buttons, Vcl.ExtCtrls,ShellApi;

type
  TfmShadowStats = class(TForm)
    memDebug: TMemo;
    btnStart: TButton;
    btnCancel: TButton;
    gbLocation: TGroupBox;
    lblLongLat: TLabel;
    lblTimeZone: TLabel;
    lblDS: TLabel;
    MainMenu1: TMainMenu;
    Options1: TMenuItem;
    miDebug: TMenuItem;
    MinAltitude1: TMenuItem;
    N11: TMenuItem;
    N21: TMenuItem;
    N31: TMenuItem;
    N41: TMenuItem;
    N51: TMenuItem;
    N71: TMenuItem;
    N101: TMenuItem;
    btnStop: TButton;
    gbFile: TGroupBox;
    rbCSV: TRadioButton;
    rbExcel: TRadioButton;
    edFileName: TEdit;
    SpeedButton1: TSpeedButton;
    RadioGroup1: TRadioGroup;
    tbSpeedAccuracy: TTrackBar;
    Label8: TLabel;
    Label9: TLabel;
    moBlankLines: TMenuItem;
    Label10: TLabel;
    Help1: TMenuItem;
    About1: TMenuItem;
    ReferenceManual1: TMenuItem;
    pcDateTime: TPageControl;
    tsDateRange: TTabSheet;
    Label1: TLabel;
    Label5: TLabel;
    dtStartDate: TDateTimePicker;
    cbFrequency: TComboBox;
    tsSolstice: TTabSheet;
    DateTimePicker1: TDateTimePicker;
    Label6: TLabel;
    cbStartTime: TComboBox;
    Label7: TLabel;
    Label3: TLabel;
    SEInterval: TSpinEdit;
    Label4: TLabel;
    cbSolarNoon: TCheckBox;
    Label11: TLabel;
    dtEndDate: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    DateTimePicker3: TDateTimePicker;
    DateTimePicker4: TDateTimePicker;
    DateTimePicker5: TDateTimePicker;
    DateTimePicker6: TDateTimePicker;
    DateTimePicker7: TDateTimePicker;
    DateTimePicker8: TDateTimePicker;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    cbStopTime: TComboBox;
    btn1Year: TButton;
    Panel1: TPanel;
    procedure GetMacroData;
    procedure btnStartClick(Sender: TObject);
    procedure Process;
    procedure FormCreate(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ShowDebugData;
    procedure Options1Click(Sender: TObject);
    procedure MinAltitudeClick(Sender: TObject);
    procedure cbTimeChange(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure ReferenceManual1Click(Sender: TObject);
    procedure btn1YearClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmShadowStats: TfmShadowStats;
  ProcessThread : TProcessThrd;

implementation

{$R *.dfm}

uses FileExistsPrompt;

{$I ../inc/timezone.inc}


Procedure TfmShadowStats.Process;
var
  HistItem : TransformHistTp;
  date, enddate, time, stoptime : TDateTime;
  sunrise, sunset : TDateTime;
  minp, maxp, TempPnt : point;
  RotateDone, flagabove, flagbelow : boolean;
  i, j : integer;
  ProportionToMove, tanang, ang : double;
  miny, maxy : double;
  s, s1, debugname : string;
  fl : TFileStream;
  altitude, azimuth : double;


  procedure DeleteBelow (Group : TList<pntarr>);
  // delete anything in Group that is 'below' surface
  var
    i, j, k, prevj, nextj : integer;
    temparr : pntarr;
    tmppnt : point;
  begin
    i := 0;
    while i < Group.Count do begin
      flagabove := false;
      flagbelow := false;
      for j := 0 to length(Group[i])-1 do begin
        if Group[i][j].z < 0 then
          flagbelow := true;
        if Group[i][j].z >= 0 then
          flagabove := true;
      end;
      if not flagabove then
        Group.delete(i)
      else begin
        if flagbelow then begin
          temparr := Group[i];
          j := 0;
          while j < length(temparr) do begin
            if temparr[j].z <= 0 then begin
              prevj := j-1;
              if prevj < 0 then
                prevj := length(temparr)-1;
              nextj := j + 1;
              if nextj >= length(temparr) then
                nextj := 0;
              if (temparr[prevj].z <= 0) and (temparr[nextj].z <= 0) then begin
                // remove this vertex
                k := j;
                while k < length(temparr)-1 do begin
                  temparr[k] := temparr[k+1];
                  k := k+1;
                end;

                if nextj = 0 then
                  setlength (temparr, length(temparr)-1);
              end
              else if temparr[prevj].z <= 0 then begin
                // move temparr[j] to the intesection of the j-nextj side with z0
                ProportionToMove := -temparr[j].z/(temparr[nextj].z - temparr[j].z);
                temparr[j].x := temparr[j].x + ProportionToMove * (temparr[nextj].x - temparr[j].x);
                temparr[j].y := temparr[j].y + ProportionToMove * (temparr[nextj].y - temparr[j].y);
                temparr[j].z := 0;
                j := j+1;
              end
              else if temparr[nextj].z <= 0 then begin
                // move temparr[j] to the intesection of the j-prevj side with z0
                ProportionToMove := -temparr[j].z/(temparr[prevj].z - temparr[j].z);
                temparr[j].x := temparr[j].x + ProportionToMove * (temparr[prevj].x - temparr[j].x);
                temparr[j].y := temparr[j].y + ProportionToMove * (temparr[prevj].y - temparr[j].y);
                temparr[j].z := 0;
                j := j+1;
              end
              else begin
                // remove j and insert points at the intersection of both adjoining sices with z0
                tmppnt := temparr[j];
                ProportionToMove := -temparr[j].z/(temparr[prevj].z - temparr[j].z);
                temparr[j].x := temparr[j].x + ProportionToMove * (temparr[prevj].x - temparr[j].x);
                temparr[j].y := temparr[j].y + ProportionToMove * (temparr[prevj].y - temparr[j].y);
                temparr[j].z := 0;

                ProportionToMove := -tmppnt.z/(temparr[nextj].z - tmppnt.z);
                tmppnt.x := tmppnt.x + ProportionToMove * (temparr[nextj].x - tmppnt.x);
                tmppnt.y := tmppnt.y + ProportionToMove * (temparr[nextj].y - tmppnt.y);
                tmppnt.z := 0;

                setlength (temparr, length(temparr)+1);
                if length(temparr) > j+1 then
                  for k := length(temparr)-1 downto j+2 do
                    temparr[k] := temparr[k-1];
                temparr[j+1] := tmppnt;
                j := j+1;
              end;
            end
            else
              j := j+1;
          end;

          Group[i] := temparr;
        end;
        i := i+1;
      end;

    end;
  end;


begin
  Screen.Cursor := crHourGlass;
  if not AlreadyTransformed then begin
    AlreadyTransformed := true;
    // rotate everything so that surface is horizontal

    // find the max & min x values of the points in the surface polygon - then use the corresponding points to create
    // a rotation transformation around the y axis
    ply_extent (surface, minp, maxp);
    ReferencePoint := minp;

    // check that minp & maxp x & y values are different.  If not then surface is vertical and parallel
    // to either the y or x axis, so just a single rotation of -90 degrees around the y or x axis is required.
    if RealEqual (minp.x, maxp.x) then begin
      HistItem.transform := rotate;
      HistItem.ang := -halfpi;
      HistItem.axis := y;
      RotatePoints (surface, length(surface), -halfpi, y);
      SurfaceSide := RotatePoint (SurfaceSide, -halfpi, y);
      RotateDone := true;
      TransformHist.Add(HistItem);
      minp := RotatePoint (minp, -halfpi, y);
      maxp := RotatePoint (maxp, -halfpi, y);
    end
    else if RealEqual (minp.y, maxp.y) then begin
      HistItem.transform := rotate;
      HistItem.ang := -halfpi;
      HistItem.axis := x;
      RotatePoints (surface, length(surface), -halfpi, x);
      SurfaceSide := RotatePoint (SurfaceSide, -halfpi, x);
      RotateDone := true;
      TransformHist.Add(HistItem);
      minp := RotatePoint (minp, -halfpi, x);
      maxp := RotatePoint (maxp, -halfpi, x);
    end
    else if RealEqual (minp.z, maxp.z) then begin
      RotateDone := true;
    end
    else begin
      // set minp & Maxp to the actual points with min & max x values.
      minp := surface[0];
      maxp := minp;
      FOR i := 1 to length(surface)-1 DO BEGIN
        if surface[i].x < minp.x then
          minp := surface[i];
        if surface[i].x > maxp.x then
          maxp := surface[i];
      END;

      // rotate about the z axis so that the line between max & min is
      // parallel to the x axis
      RotateDone := false;
      tanang := (maxp.y - minp.y)/(maxp.x - minp.x);
      ang := arctan(tanang);
      HistItem.transform := rotateRel;
      HistItem.ang := -ang;
      HistItem.axis := z;
      HistItem.relpnt := minp;
      TransformHist.Add(HistItem);
      RotatePointsRel(surface, length(surface), minp, -ang, z);
      SurfaceSide := RotatePointRel (SurfaceSide, minp, -ang, z);
      maxp := RotatePointRel (maxp, minp, -ang, z);

      // find min and max y values of modified surface
      miny := surface[0].y;
      maxy := surface[0].y;

      FOR i:=1 to length(surface)-1 DO BEGIN
        miny := min (miny, surface[i].y);
        maxy := max (maxy, surface[i].y);
      END;


      if RealEqual (miny, maxy) then begin
        // it is vertical, and now parallel to the x axis, so just needs
        // to be rotated about the x axis;
        HistItem.transform := rotate;
        HistItem.ang := -halfpi;
        HistItem.axis := x;
        TransformHist.Add(HistItem);

        RotatePoints(surface, length(surface), -halfpi, x);
        SurfaceSide := RotatePoint (SurfaceSide, -halfpi, x);
        RotateDone := true;
      end
      else begin
        // not vertical, calculate rotation about the y axis
        tanang := (maxp.z - minp.z)/(maxp.x - minp.x);
        ang := arctan(tanang);
        HistItem.transform := rotateRel;
        HistItem.ang := ang;
        HistItem.relpnt := minp;
        HistItem.axis := y;
        TransformHist.Add(HistItem);

        RotatePointsRel(surface, length(surface), minp, ang, y);
        SurfaceSide := RotatePointRel (SurfaceSide, minp, ang, y);
      end;
    end;

    // add a rotation around the x axis if required
    // find the max & min y values of the points in the mod_surf polygon - then use the corresponding points
    // to create a rotation transformation around the y axis
    IF not RotateDone THEN BEGIN
      minp := surface[0];
      maxp := minp;
      FOR i := 1 to length(surface)-1 DO BEGIN
        if surface[i].y < minp.y then
          minp := surface[i];
        if surface[i].y > maxp.y then
          maxp := surface[i];
      END;
      tanang := (maxp.z - minp.z	)/(maxp.y - minp.y);
      ang := arctan(tanang);

      HistItem.transform := rotate;
      HistItem.ang := -ang;
      HistItem.axis := x;
      TransformHist.Add(HistItem);

      RotatePoints(surface, length(surface)-1, -ang, x);
      SurfaceSide := RotatePoint (SurfaceSide, -ang, x);
    END;

    // polygon should now be horizontal ... move it to a z-height of zero          HistItem.transform := rotate;
    if abs(surface[0].z) > NearZero then begin
      HistItem.transform := Move;
      HistItem.vect.x := 0;
      HistItem.vect.y := 0;
      HistItem.vect.z := -surface[0].z;
      TransformHist.Add(HistItem);
      SurfaceSide := MovePoint(SurfaceSide, HistItem.vect);
    end;
    for i := 0 to length(surface)-1 do
      surface[i].z := 0.0;

    // polygon is now horizontal.  If SurfaceSide is below it then we need to
    // flip it over.
    if SurfaceSide.z < 0 then begin
      HistItem.transform := rotate;
      HistItem.ang := pi;
      HistItem.axis := x;
      TransformHist.Add(HistItem);
      RotatePoints(surface, length(surface)-1, pi, x);
    end;

    // transformation of surface is now complete, ensure vertices are defined in
    // a clockwise direction.
    FixPolygon(surface, clockwise);

    // apply same transformations to group1 & group2
    for i := 0 to TransformHist.Count-1 do
      case TransformHist[i].transform of
        rotate :    begin
                      for j := 0 to Group1.Count-1 do
                        RotatePoints (Group1[j], length(Group1[j]),
                                      TransformHist[i].ang,
                                      TransformHist[i].axis);
                      for j := 0 to Group2.Count-1 do
                        RotatePoints (Group2[j], length(Group2[j]),
                                      TransformHist[i].ang,
                                      TransformHist[i].axis);
                    end;
        rotateRel : begin
                      for j := 0 to Group1.Count-1 do
                        RotatePointsRel (Group1[j], length(Group1[j]),
                                         TransformHist[i].relpnt,
                                         TransformHist[i].ang,
                                         TransformHist[i].axis);
                      for j := 0 to Group2.Count-1 do
                        RotatePointsRel (Group2[j], length(Group2[j]),
                                         TransformHist[i].relpnt,
                                         TransformHist[i].ang,
                                         TransformHist[i].axis);
                    end;
        move :      begin
                      for j := 0 to Group1.Count-1 do
                        MovePoints (Group1[j], length(Group1[j]),
                                    TransformHist[i].vect.x,
                                    TransformHist[i].vect.y,
                                    TransformHist[i].vect.z);
                      for j := 0 to Group2.Count-1 do
                        MovePoints (Group2[j], length(Group2[j]),
                                    TransformHist[i].vect.x,
                                    TransformHist[i].vect.y,
                                    TransformHist[i].vect.z);
                    end;
      end;

    if ShowDebugInfo then begin
      debugname := ExtractFileDir(ParamStr(0)) + '\\B4DeletedBelowZero.log';
      fl := TFileStream.Create (debugname, fmCreate);
      try
        i := -1;    // not relevant to this debug file
        fl.Write(i, 4);
        i := length(Surface);
        fl.Write(i, 4);
        for i := 0 to length(Surface)-1 do begin
          TempPnt := Surface[i];
          fl.write (TempPnt.v, 24);
        end;
        for i := 0 to Group1.Count-1 do begin
          j := length(Group1[i]);
          fl.Write(j, 4);
          for j := 0 to length(Group1[i])-1 do begin
            TempPnt := Group1[i][j];
            fl.write (TempPnt.v, 24);
          end;
        end;
        for i := 0 to Group2.Count-1 do begin
          j := length(Group2[i]);
          fl.Write(j, 4);
          for j := 0 to length(Group2[i])-1 do begin
            TempPnt := Group2[i][j];
            fl.write (TempPnt.v, 24);
          end;
        end;
      finally
        fl.Destroy;
      end;
    end;


    // get rid of anything in Group1, Group2 that is below the surface
    DeleteBelow (Group1);
    DeleteBelow (Group2);

    if ShowDebugInfo then begin
      debugname := ExtractFileDir(ParamStr(0)) + '\\DeletedBelowZero.log';
      fl := TFileStream.Create (debugname, fmCreate);
      try
        i := -1;    // not relevant to this debug file      x`
        fl.Write(i, 4);
        i := length(Surface);
        fl.Write(i, 4);
        for i := 0 to length(Surface)-1 do begin
          TempPnt := Surface[i];
          fl.write (TempPnt.v, 24);
        end;
        for i := 0 to Group1.Count-1 do begin
          j := length(Group1[i]);
          fl.Write(j, 4);
          for j := 0 to length(Group1[i])-1 do begin
            TempPnt := Group1[i][j];
            fl.write (TempPnt.v, 24);
          end;
        end;
        for i := 0 to Group2.Count-1 do begin
          j := length(Group2[i]);
          fl.Write(j, 4);
          for j := 0 to length(Group2[i])-1 do begin
            TempPnt := Group2[i][j];
            fl.write (TempPnt.v, 24);
          end;
        end;
      finally
        fl.Destroy;
      end;
    end;


    if ShowDebugInfo then begin
      memDebug.Lines.Add('');
      memDebug.Lines.Add('');
      memDebug.Lines.Add('=========== DATA AFTER TRANSFORMATIONS: ============');
      ShowDebugData;
    end;
  end;  // not AlreadyTransformed

  usingExcel := rbExcel.Checked;
  FileName := edFileName.text;
  FileExistsResult := -1;
  SolarAnchor := cbSolarNoon.Checked;

  if FileExists(FileName) then begin
    fmFileExists.cbSheetName.Text := '';
    fmFileExists.Label1.Caption := edFileName.text + ' already exists.  '  +
                                 'Select one of the buttons below to choose how to proceed';
    if usingExcel then begin
      fmFileExists.rbAppend.Caption := 'APPEND add data to the end of an existing sheet, or create a new sheet';
      try
        ExcelApplication := GetActiveOleObject('Excel.Application');
        ExistingExcel := true;
      except
        ExcelApplication := CreateOleObject('Excel.Application');
        ExistingExcel := false;
      end;
      try
        wb := ExcelApplication.Workbooks.Item(FileName);
        ExistingWB := true;
      except
        wb := ExcelApplication.Workbooks.Open(FileName);
        ExistingWB := false;
      end;
      for i := 1 to wb.Sheets.Count do
        fmFileExists.cbSheetName.Items.Add(wb.Sheets[i].Name);
      fmFileExists.cbSheetName.visible := true;
    end
    else begin
      fmFileExists.rbAppend.Caption := 'APPEND data to the end of the existing file';
      fmFileExists.cbSheetName.visible := false;
    end;

    Screen.Cursor := crDefault;

    FileExistsResult := fmFileExists.ShowModal;

    if FileExistsResult = mrCancel then begin
      if usingExcel then begin
        if not ExistingWB then
          wb.close;
        wb := Unassigned;
        ExcelApplication := Unassigned;
      end;
      exit;
    end
    else if (FileExistsResult = mrAppend) and UsingExcel then begin
      SheetName := fmFileExists.cbSheetName.Text;
      NewSheet := fmFileExists.cbSheetName.ItemIndex < 0;
    end
    else if (FileExistsResult = mrOK) and UsingExcel then begin
      wb.close;
      wb := Unassigned;
      ExcelApplication := Unassigned;
    end;
  end;


  if usingExcel then begin
    if FileExistsResult =  mrOK then begin
      if Group2.Count > 0 then
        s :=  ExtractFilePath (ParamStr(0)) +  'DefaultShadStat2.xlsx'
      else
        s :=  ExtractFilePath (ParamStr(0)) +  'DefaultShadStat1.xlsx';
      if not CopyFile (PWideChar (s), PWideChar (FileName), false) then begin
        messagedlg ('Could not replace file (' + s + ')' + sLineBreak +
                    'The file may be in use, or there could be a problem with the default file' + sLineBreak + sLineBreak +
                    'Processing cancelled', mtError, [mbOK], 0);
        exit;
      end;
    end;
  end
  else begin
    try
      AssignFile (csvFile, edFileName.text);
      if FileExistsResult = mrAppend then begin
        append (csvfile);
        writeln (csvfile);
      end
      else begin
        rewrite (csvFile);
        if Group2.Count > 0 then
          writeln (csvfile, 'Date, Time, Group1 %Shaded, Group2 %Shaded, Combined %Shaded, Surface %Increase')
        else
          writeln (csvfile, 'Date, Time, %Shaded');
      end;

    except
      on E: Exception do begin
        MessageDlg('Error opening output file:' + sLineBreak + sLineBreak + E.Message,
                   mtError, [mbOK], 0);
        exit;
      end;
    end;
  end;

  if pcDateTime.ActivePageIndex = 0 then begin
    setlength(Dates, 0);
    date := trunc (dtStartDate.DateTime);
    enddate := trunc (dtEndDate.DateTime);
    if endDate < date then begin
      messagedlg ('Stop Date may not be before Start Date' + sLineBreak + sLineBreak +
              'Processing cancelled', mtError, [mbOK], 0);
      exit;
    end
    else if endDate > IncYear(date, 1) then begin
      messagedlg ('Stop Date may not be more than a year after Start Date' + sLineBreak + sLineBreak +
              'Processing cancelled', mtError, [mbOK], 0);
      exit;
    end;
  end
  else begin
    if DateTimePicker1.Checked then begin
      SetLength (Dates, 1);
      Dates[0] := DateTimePicker1.DateTime;
    end;
    if DateTimePicker2.Checked then begin
      SetLength (Dates, Length(Dates)+1);
      Dates[Length(Dates)-1] := DateTimePicker2.DateTime;
    end;
    if DateTimePicker3.Checked then begin
      SetLength (Dates, Length(Dates)+1);
      Dates[Length(Dates)-1] := DateTimePicker3.DateTime;
    end;
    if DateTimePicker4.Checked then begin
      SetLength (Dates, Length(Dates)+1);
      Dates[Length(Dates)-1] := DateTimePicker4.DateTime;
    end;
    if DateTimePicker5.Checked then begin
      SetLength (Dates, Length(Dates)+1);
      Dates[Length(Dates)-1] := DateTimePicker5.DateTime;
    end;
    if DateTimePicker6.Checked then begin
      SetLength (Dates, Length(Dates)+1);
      Dates[Length(Dates)-1] := DateTimePicker6.DateTime;
    end;
    if DateTimePicker7.Checked then begin
      SetLength (Dates, Length(Dates)+1);
      Dates[Length(Dates)-1] := DateTimePicker7.DateTime;
    end;
    if DateTimePicker8.Checked then begin
      SetLength (Dates, Length(Dates)+1);
      Dates[Length(Dates)-1] := DateTimePicker8.DateTime;
    end;
    if Length(Dates) = 0 then begin
      messagedlg ('No Dates Specified' + sLineBreak + sLineBreak +
              'Processing cancelled', mtError, [mbOK], 0);
      exit;
    end;

  end;


  if ShowDebugInfo then begin
    memDebug.Lines.Add('');
    memDebug.Lines.Add('');
    memDebug.Lines.Add('=========== PROCESSING DATA: ============');
  end;

  if Length(Dates) = 0 then begin
    DateTimeToString(s, 'dd-mmm-yy', date);
    memDebug.Lines.Add ('Start Date: ' + s);
    DateTimeToString(s, 'dd-mmm-yy', enddate);
    memDebug.Lines.Add ('End Date: ' + s);
  end;

  memStatus := memDebug;  // processing thread will write processing date to memStatus even if not debugging
  btnStart.Enabled := false;
  btnStop.Enabled := true;
  btnCancel.Enabled := false;

  Buttons[1] := btnCancel;
  Buttons[2] := btnStop;
  BUttons[3] := btnStart;
  StopRequested := false;
  SpeedSetting := tbSpeedAccuracy.Position;
  BlankLines := moBlankLines.Checked;


  ProcessThread := TProcessThrd.Create (true);
  ProcessThread.setValues(date, endDate, cbStartTime.ItemIndex, cbStopTime.ItemIndex,
                          seInterval.Value, cbFrequency.ItemIndex);
  ProcessThread.FreeOnTerminate := true;
  ProcessThread.Start;
end;

procedure TfmShadowStats.ReferenceManual1Click(Sender: TObject);
var
  helpfilename : string;
begin
  helpfilename := ExtractFileDir(ParamStr(0)) + '\\ShadowAnalysis.pdf';
  if FileExists(helpfilename) then
    ShellExecute(0, 'open', PWideChar(helpfilename),nil,nil, SW_SHOWNORMAL)
  else
    MessageDlg('Manual not found', mtError, [mbOK], 0);
end;


procedure TfmShadowStats.btnStartClick(Sender: TObject);
begin
    Process;
    Screen.Cursor := crDefault;
end;

procedure TfmShadowStats.btnStopClick(Sender: TObject);
begin
  StopRequested := true;
end;

procedure TfmShadowStats.Button1Click(Sender: TObject);
var
  year : integer;
begin
  year := YearOf (Now);
  DateTimePicker1.DateTime := calcKeyDate (AutumnEquinox , year);
  DateTimePicker1.Checked := true;
  DateTimePicker3.DateTime := calcKeyDate (WinterSolstice , year);
  DateTimePicker3.Checked := true;
  DateTimePicker5.DateTime := calcKeyDate (SpringEquinox , year);
  DateTimePicker5.Checked := true;
  DateTimePicker7.DateTime := calcKeyDate (SummerSolstice , year);
  DateTimePicker7.Checked := true;

  DateTimePicker2.DateTime := DateOf (( DateTimePicker1.DateTime + DateTimePicker3.DateTime)/2);
  DateTimePicker2.Checked := false;
  DateTimePicker4.DateTime := DateOf (( DateTimePicker3.DateTime + DateTimePicker5.DateTime)/2);
  DateTimePicker4.Checked := false;
  DateTimePicker6.DateTime := DateOf (( DateTimePicker5.DateTime + DateTimePicker7.DateTime)/2);
  DateTimePicker6.Checked := false;
  DateTimePicker8.DateTime := DateOf (( DateTimePicker7.DateTime + calcKeyDate (AutumnEquinox, year+1))/2);
  DateTimePicker8.Checked := false;

  DateTimePicker1.DateTime := DateOf (DateTimePicker1.DateTime);
  DateTimePicker3.DateTime := DateOf (DateTimePicker3.DateTime);
  DateTimePicker5.DateTime := DateOf (DateTimePicker5.DateTime);
  DateTimePicker7.DateTime := DateOf (DateTimePicker7.DateTime);
end;

procedure TfmShadowStats.Button2Click(Sender: TObject);
begin
  DateTimePicker1.Checked := true;
  DateTimePicker2.Checked := true;
  DateTimePicker3.Checked := true;
  DateTimePicker4.Checked := true;
  DateTimePicker5.Checked := true;
  DateTimePicker6.Checked := true;
  DateTimePicker7.Checked := true;
  DateTimePicker8.Checked := true;
end;

procedure TfmShadowStats.Button3Click(Sender: TObject);
begin
  DateTimePicker1.Checked := false;
  DateTimePicker2.Checked := false;
  DateTimePicker3.Checked := false;
  DateTimePicker4.Checked := false;
  DateTimePicker5.Checked := false;
  DateTimePicker6.Checked := false;
  DateTimePicker7.Checked := false;
  DateTimePicker8.Checked := false;
end;

procedure TfmShadowStats.About1Click(Sender: TObject);
begin
  FmAbout.ShowModal;
end;

procedure TfmShadowStats.btn1YearClick(Sender: TObject);
begin
  dtEndDate.DateTime := IncDay (IncYear (dtStartDate.DateTime), -1);
end;

procedure TfmShadowStats.btnCancelClick(Sender: TObject);
begin
  Application.Terminate;
end;




procedure TfmShadowStats.cbTimeChange(Sender: TObject);
begin
  if (cbStopTime.ItemIndex < 41) and (cbStartTime.ItemIndex > 13) and
     (cbStopTime.ItemIndex <> 0) and
     ((cbStartTime.ItemIndex - 12) > cbStopTime.ItemIndex ) then begin
    MessageDlg('Stop Time cannot be less than Start Time', mtError, [mbOK], 0);
    if (Sender as TComboBox).Tag = 0 then
      cbStartTime.ItemIndex := cbStopTime.ItemIndex + 12
    else
      cbStopTime.ItemIndex := cbStartTime.ItemIndex - 12;
  end;
end;



procedure TerminateApplicationOnError;
begin
  MessageDlg('Invalid data received from Macro' + sLineBreak + '(Application will terminate)',
             mtError, [mbOK], 0);
  Application.Terminate;
end;

procedure TfmShadowStats.GetMacroData;
var
  ini : TIniFile;
  outputdir, outputfile : string;

//  TimeZone : double;
begin
  ini := TIniFile.Create(ParamStr(1) +  'ShStat.ini');
  MacroVersion := ini.ReadString('Macro Information', 'Version', '0');
  RealSize := ini.ReadInteger('Macro Information', 'Real Size', 0);
  Country := ini.ReadString('Location', 'Country', '');
  City := ini.ReadString('Location', 'City', '');
  Longitude := ini.ReadFloat('Location', 'Longitude', 400);
  Latitude := ini.ReadFloat('Location', 'Latitude', 400);
  North := ini.ReadFloat('Orientation', 'North', 0);
  TimeZone := TimezoneFloat (ini.ReadInteger('Time', 'Zone Index', -1));
  if ((RealSize <> 4) and (RealSize <> 8)) or
     ((RealSize=8) and ((Country = '') or (City = ''))) or
     (Longitude > 180) or (Longitude < -180) or
     (Latitude > 90) or (Latitude < -90) or
     (TimeZone = nan)
  then begin
    TerminateApplicationOnError;
  end;

  DS_Start_Ndx := ini.ReadInteger('Time', 'DS Start Ndx', -1);
  DS_End_Ndx := ini.ReadInteger ('Time', 'DS End Ndx', -1);
  useDS := (DS_Start_Ndx >= 0) and (DS_End_Ndx >= 0);
  DS_Year := 0;   // ensure daylight savings dates are calculated in first call to isDaylightSaving function

  OutputDir := ini.ReadString('Output', 'Path', '');
  OutputFile := ini.ReadString('Output', 'File', '');
  edFileName.Text := outputdir + outputfile;
  if outputfile.EndsWith('.xlsx') then
    rbExcel.Checked := true
  else
    rbCSV.Checked := true;
end;

procedure GetSurface;
var
  ndx: integer;
  F: TFileStream;
  buffer4 : array [0..2] of single;
  buffer8 : array [0..2] of double;
begin
  F := TFileStream.Create (ParamStr(1) + 'ShSf.dat', fmOpenRead);
  try
    if F.Size < 12*realsize then begin
      // surface data must contain surfaceside point plus at least 3 points for the surface itself
      TerminateApplicationOnError;
    end;
    if realsize = 4 then begin
      F.Read(buffer4, 12);
      surfaceside.x := buffer4[0];
      surfaceside.y := buffer4[1];
      surfaceside.z := buffer4[2];
    end
    else begin
      F.Read(buffer8, 24);
      surfaceside.x := buffer8[0];
      surfaceside.y := buffer8[1];
      surfaceside.z := buffer8[2];
    end;

    SetLength (surface, (F.Size div (3*realsize) - 1));
    ndx := 0;
    while F.Position < F.Size do begin
      if realsize = 4 then begin
        F.Read(buffer4, 12);
        surface[ndx].x := buffer4[0];
        surface[ndx].y := buffer4[1];
        surface[ndx].z := buffer4[2];
      end
      else begin
        F.Read(buffer8, 24);
        surface[ndx].x := buffer8[0];
        surface[ndx].y := buffer8[1];
        surface[ndx].z := buffer8[2];
      end;
      ndx := ndx+1;
    end;
  finally
    F.Free;
  end;
end;

procedure GetCastingEnts (var PolygonList : TList<pntarr>; FileName : string);
var
  i, ndx: integer;
  F: TFileStream;
  buffer4 : array [0..2] of single;
  buffer8 : array [0..2] of double;
  pntcnt : smallint;
  pgn : pntarr;
begin
  PolygonList := TList<pntarr>.create;

  F := TFileStream.Create (ParamStr(1) + FileName, fmOpenRead);
  try
    while F.Position < F.Size do
    begin
      F.Read(pntcnt, 2);

      setlength (pgn, pntcnt);
      ndx := 0;

      for i := 1 to pntcnt do begin
        if realsize = 4 then begin
          F.Read(buffer4, 12);
          pgn[ndx].x := buffer4[0];
          pgn[ndx].y := buffer4[1];
          pgn[ndx].z := buffer4[2];
        end
        else begin
          F.Read(buffer8, 24);
          pgn[ndx].x := buffer8[0];
          pgn[ndx].y := buffer8[1];
          pgn[ndx].z := buffer8[2];
        end;
        ndx := ndx+1;
      end;
      PolygonList.Add(pgn);
    end;
  finally
    F.Free;
  end;
end;

procedure TfmShadowStats.ShowDebugData;
var
  p : point;
  a : pntarr;
  s : string;
begin
  if not ShowDebugInfo then exit;

  if memDebug.Lines.Count = 0 then
    memDebug.Lines.LoadFromFile(ParamStr(1) +  'ShStat.ini');    // only show this the first time
  memDebug.Lines.Add('');
  memDebug.Lines.Add('SURFACE DATA:');
  memDebug.Lines.Add(' Side: ' + floattostr(surfaceside.x) + ', ' + floattostr(surfaceside.y) + ', ' + floattostr(surfaceside.z));
  memDebug.Lines.Add(' ' + inttostr(length(surface)) + ' Points:');
  for p in surface do begin
    memDebug.Lines.Add('  ' + floattostr(p.x) + ', ' + floattostr(p.y) + ', ' + floattostr(p.z));
  end;
  memDebug.Lines.Add('');
  memDebug.Lines.Add('GROUP 1 DATA (' + inttostr(Group1.Count) + ' polygons):');
  for a in Group1 do begin
    s := '  ';
    for p in a do begin
      s := s + '(' + floattostr(p.x) + ', ' + floattostr(p.y) + ', ' + floattostr(p.z) + '), ';
    end;
    if length(s) > 2 then
      delete (s, length(s)-1, 2);
    memDebug.Lines.Add(s);
  end;
  memDebug.Lines.Add('');
  memDebug.Lines.Add('GROUP 2 DATA (' + inttostr(Group2.Count) + ' polygons):');
  for a in Group2 do begin
    s := '  ';
    for p in a do begin
      s := s + '(' + floattostr(p.x) + ', ' + floattostr(p.y) + ', ' + floattostr(p.z) + '), ';
    end;
    if length(s) > 2 then
      delete (s, length(s)-1, 2);
    memDebug.Lines.Add(s);
  end;
end;

procedure TfmShadowStats.SpeedButton1Click(Sender: TObject);
var
  OpenDialog : TOpenDialog;
begin
  OpenDialog := TOpenDialog.Create(self);
  OpenDialog.InitialDir := ExtractFilePath (edFileName.Text);
  OpenDialog.FileName := ExtractFileName(edFileName.Text);
  OpenDialog.Title := 'Output File';
  OpenDialog.Filter := 'Excel Files (*.xlsx)|*.XLSX|Comma Separated Values (*.csv)|*.csv';
  if rbExcel.Checked then
    OpenDialog.FilterIndex := 1
  else
    OpenDialog.FilterIndex := 2;
  if OpenDialog.Execute then begin
    edFileName.Text := OpenDialog.FileName;
    if (String(OpenDialog.FileName)).EndsWith('.csv') then
      rbCSV.Checked := true
    else if (String(OpenDialog.FileName)).EndsWith('.xlsx') then
      rbExcel.Checked := true;
  end;
    OpenDialog.Free;
end;

procedure TfmShadowStats.Options1Click(Sender: TObject);
begin
  ShowDebugInfo :=(Sender as TMenuItem).Checked;
end;

procedure TfmShadowStats.FormCreate(Sender: TObject);
const
  {$I ../inc/DaylightSaving.inc}

var
  ini : TIniFile;
  s : string;
  i : integer;
  tempdate : TDateTime;

begin
  TransformHist := TList<TransformHistTp>.Create;
  AlreadyTransformed := false;

  // Get any saved settings from ini file
  ini := TIniFIle.Create(ExtractFilePath(ParamStr(0)) + ShadowAnalIni);
  try
    MinAltitude := ini.ReadInteger('Settings', 'MinAltitude', 1);
    case MinAltitude of
      1 : N11.Checked := true;
      2 : N21.Checked := true;
      3 : N31.Checked := true;
      4 : N41.Checked := true;
      5 : N51.Checked := true;
      7 : N71.Checked := true;
      10: N101.Checked := true;
    end;

    if ini.ReadBool('Settings', 'DateRange', true) then
      pcDateTime.ActivePageIndex := 0
    else
      pcDateTime.ActivePageIndex := 1;

    ShowDebugInfo := ini.ReadBool('Settings', 'ShowDebugInfo', false);
    miDebug.Checked := ShowDebugInfo;

    s := ini.ReadString('Settings', 'StartTime', '');
    i := cbStartTime.Items.IndexOf(s);
    i := max (i, 0);
    cbStartTime.ItemIndex := i;
    s := ini.ReadString('Settings', 'StopTime', '');
    i := cbStopTime.Items.IndexOf(s);
    i := max (i, 0);
    cbStopTime.ItemIndex := i;
    seInterval.Value := ini.ReadInteger('Settings', 'Interval', 15);
    s := ini.ReadString('Settings', 'Frequency', 'Every Day');
    i := cbFrequency.Items.IndexOf(s);
    i := max (i, 0);
    cbFrequency.ItemIndex := i;

    tempdate := ini.ReadDate('Settings', 'Date1', 0);
    if tempdate = 0 then begin
      Button1Click (self);
    end
    else begin
      DateTimePicker1.Checked := ini.ReadBool('Settings', 'UseDate1', false);
      DateTimePicker1.date := tempdate;
      DateTimePicker2.Checked := ini.ReadBool('Settings', 'UseDate2', false);
      DateTimePicker2.date := ini.ReadDate('Settings', 'Date2', 0);
      DateTimePicker3.Checked := ini.ReadBool('Settings', 'UseDate3', false);
      DateTimePicker3.date := ini.ReadDate('Settings', 'Date3', 0);
      DateTimePicker4.Checked := ini.ReadBool('Settings', 'UseDate4', false);
      DateTimePicker4.date := ini.ReadDate('Settings', 'Date4', 0);
      DateTimePicker5.Checked := ini.ReadBool('Settings', 'UseDate5', false);
      DateTimePicker5.date := ini.ReadDate('Settings', 'Date5', 0);
      DateTimePicker6.Checked := ini.ReadBool('Settings', 'UseDate6', false);
      DateTimePicker6.date := ini.ReadDate('Settings', 'Date6', 0);
      DateTimePicker7.Checked := ini.ReadBool('Settings', 'UseDate7', false);
      DateTimePicker7.date := ini.ReadDate('Settings', 'Date7', 0);
      DateTimePicker8.Checked := ini.ReadBool('Settings', 'UseDate8', false);
      DateTimePicker8.date := ini.ReadDate('Settings', 'Date8', 0);
    end;
  finally
    ini.Free;
  end;

  // read data passed by macro
  GetMacroData;
  GetSurface;
  GetCastingEnts (Group1, 'ShEx.dat');
  GetCastingEnts (Group2, 'ShNw.dat');

  ShowDebugData;

  gbLocation.Caption := City + ', ' + Country;
  lblLongLat.Caption := FloatToStr(abs(Longitude));
  if Longitude < 0 then
    lblLongLat.Caption := lblLongLat.Caption + '�W, '
  else
    lblLongLat.Caption := lblLongLat.Caption + '�E, ';
  lblLongLat.Caption := lblLongLat.Caption + FloatToStr (abs(Latitude));
  if Latitude < 0 then
    lblLongLat.Caption := lblLongLat.Caption + '�S'
  else
    lblLongLat.Caption := lblLongLat.Caption + '�N';
  lblTimeZone.Caption := formatTimeZone(TimeZone);
  if DS_Start_Ndx >= 0 then begin
    lblDS.Caption := 'Daylight Saving starts ' + DSStartValues[DS_Start_Ndx] +
                     ', ends ' + DSEndValues[DS_End_Ndx];
  end
  else
    lblDS.Caption := '';

  dtStartDate.DateTime := date;

  Caption := 'Shadow Analysis';





  //chbSummerSol.Caption := DateTimeToStr(calcKeyDate ( SummerSolstice, YearOf (Now)));

end;

procedure TfmShadowStats.FormDestroy(Sender: TObject);
var
  ini : TIniFile;
begin
  TransformHist.free;
  ini := TIniFIle.Create(ExtractFilePath(ParamStr(0)) + ShadowAnalIni);
  try
    ini.WriteInteger('Settings', 'MinAltitude', MinAltitude);
    ini.WriteBool('Settings', 'ShowDebugInfo', ShowDebugInfo);
    ini.WriteString('Settings', 'StartTime', cbStartTime.Text);
    ini.WriteString('Settings', 'StopTime', cbStopTime.Text);
    ini.WriteInteger('Settings', 'Interval', seInterval.Value);
    ini.WriteString('Settings', 'Frequency', cbFrequency.Text);
    ini.WriteBool('Settings', 'DateRange', pcDateTime.ActivePageIndex = 0);
    ini.WriteBool('Settings', 'UseDate1', DateTimePicker1.Checked);
    ini.WriteDate('Settings', 'Date1', DateTimePicker1.date);
    ini.WriteBool('Settings', 'UseDate2', DateTimePicker2.Checked);
    ini.WriteDate('Settings', 'Date2', DateTimePicker2.date);
    ini.WriteBool('Settings', 'UseDate3', DateTimePicker3.Checked);
    ini.WriteDate('Settings', 'Date3', DateTimePicker3.date);
    ini.WriteBool('Settings', 'UseDate4', DateTimePicker4.Checked);
    ini.WriteDate('Settings', 'Date4', DateTimePicker4.date);
    ini.WriteBool('Settings', 'UseDate5', DateTimePicker5.Checked);
    ini.WriteDate('Settings', 'Date5', DateTimePicker5.date);
    ini.WriteBool('Settings', 'UseDate6', DateTimePicker6.Checked);
    ini.WriteDate('Settings', 'Date6', DateTimePicker6.date);
    ini.WriteBool('Settings', 'UseDate7', DateTimePicker7.Checked);
    ini.WriteDate('Settings', 'Date7', DateTimePicker7.date);
    ini.WriteBool('Settings', 'UseDate8', DateTimePicker8.Checked);
    ini.WriteDate('Settings', 'Date8', DateTimePicker8.date);
  finally
    ini.Free;
  end;
  Group1.Free;
  Group2.Free;
end;

procedure TfmShadowStats.MinAltitudeClick(Sender: TObject);
begin
  MinAltitude := (Sender as TMenuItem).Tag;
end;

end.
