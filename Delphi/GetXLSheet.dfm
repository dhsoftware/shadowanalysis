object fmSelectXLSheet: TfmSelectXLSheet
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Select Sheet'
  ClientHeight = 108
  ClientWidth = 370
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 337
    Height = 13
    Caption = 
      'Select sheet to append data to (or enter name of new sheet to ce' +
      'ate)'
  end
  object Button1: TButton
    Left = 286
    Top = 75
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object cbSheetName: TComboBox
    Left = 8
    Top = 35
    Width = 353
    Height = 21
    TabOrder = 1
    Text = 'cbSheetName'
  end
end
