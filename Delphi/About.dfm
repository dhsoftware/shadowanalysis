object fmAbout: TfmAbout
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'About Shadow Analysis Macro'
  ClientHeight = 244
  ClientWidth = 324
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblVersion: TLabel
    Left = 24
    Top = 16
    Width = 281
    Height = 13
    AutoSize = False
    Caption = 'lblVersion'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 24
    Top = 35
    Width = 129
    Height = 17
    AutoSize = False
    Caption = #169' David Henderson 2020'
    WordWrap = True
  end
  object Label2: TLabel
    Left = 24
    Top = 58
    Width = 281
    Height = 111
    AutoSize = False
    Caption = 
      'This software is distributed free of charge and without warranty' +
      '. You may distribute it to others provided that you distribute t' +
      'he complete unaltered installation file provided at the dhsoftwa' +
      're.com.au web site, and that you do so free of charge (This incl' +
      'udes not charging for distribution media and not charging for an' +
      'y accompanying software that is on the same media or contained i' +
      'n the same download or distribution file).'
    WordWrap = True
  end
  object lblwww: TLabel
    Left = 183
    Top = 33
    Width = 122
    Height = 13
    Cursor = crHandPoint
    Hint = 'www.dhsoftware.com.au'
    Alignment = taRightJustify
    Caption = 'www.dhsoftware.com.au'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = lblwwwClick
  end
  object lblContribute: TLabel
    Left = 24
    Top = 175
    Width = 263
    Height = 26
    Cursor = crHandPoint
    Hint = 'www.dhsoftware.com.au/contribute.htm'
    Caption = 
      'Contribute towards the development of this and other DataCAD / S' +
      'pirit macros'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsUnderline]
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    WordWrap = True
    OnClick = lblContributeClick
  end
  object Button1: TButton
    Left = 241
    Top = 211
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 0
  end
end
