unit CommonStuff;

interface

uses System.Generics.Collections, Geometry, Vcl.StdCtrls;

CONST
  mrAppend = 10;
  ShadowAnalIni = 'ShadowAnal.ini';
VAR
  ShowDebugInfo : boolean;

  Surface : pntarr;
  SurfaceSide : point;
  Group1 :  TList<pntarr>;
  Group2 :  TList<pntarr>;
  Shad1 : pntarr;
  Shad2 : pntarr;
  ReferencePoint : point;

  LocationIniFile : string;
  ConfigIniFile : string;
  lasttown, lastcountry : string;

  FileName : string;
  FileExistsResult : integer;
  csvFile : TextFile;
  solarAnchor : boolean;
  usingExcel : boolean;
  SheetName : string;
  newSheet : boolean;
  ExistingExcel, ExistingWB : boolean;
  ExcelApplication, xls, wb, ws: variant;


  MacroVersion : string;
  RealSize : integer;
  Country, City : string;
  Longitude, Latitude : double;
  North : double;
  TimeZone : double;
  useDS : boolean;
  DS_Start_Ndx, DS_End_Ndx : integer;
  DS_Start, DS_End : TDateTime;
  DS_Year : word;
  MinAltitude : integer;
  SpeedSetting : integer;
  BlankLines : boolean;

  memStatus : TMemo;
  Buttons : array [1..3] of TButton;
  stopRequested : boolean;
  Dates : array of TDateTime;

type
  TransformTp = (rotate, rotateRel, Move);
  TransformHistTp = record
    transform : TransformTp;
    case TransformTp of
      rotate,
      rotateRel : (ang : double;
                   axis : integer;
                   relpnt : point);
      move      : (vect : point);
    end;

  KeyDateTp = (SummerSolstice, AutumnEquinox, WinterSolstice, SpringEquinox);  // Names based on southern hemisphere seasons

var
  TransformHist : TList<TransformHistTp>;
  AlreadyTransformed : boolean;


implementation

end.
