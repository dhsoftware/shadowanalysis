
; The name of the installer
Name "Shadow Analysis Macro"


; The file to write
OutFile "ShadAnalysisInstall.exe"

RequestExecutionLevel admin ;Require admin rights on NT6+ (When UAC is turned on)

!include LogicLib.nsh


var DCSupDir
var DCInstDir
var SupDir
var Spirit

!define LANG_ENGLISH 3081
!define TEMP1 $R0 ;Temp variable
!define TEMP2 $R1 ;Temp variable
VIProductVersion "1.0.0.0"
VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "Shadow Analysis"
VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductVersion" "1.0.0"
VIAddVersionKey /LANG=${LANG_ENGLISH} "Comments" "For use with DataCAD / Spirit"
VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" "dhSoftware"
;VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalTrademarks" "Test Application is a trademark of Fake company"
VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "David Henderson 2020"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "Installs Shadow Analysis Macro for DataCAD/Spirit"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "1.0.0.0"


; StrContains
; This function does a case sensitive searches for an occurrence of a substring in a string. 
; It returns the substring if it is found. 
; Otherwise it returns null(""). 
; Written by kenglish_hi
; Adapted from StrReplace written by dandaman32
 
 
Var STR_HAYSTACK
Var STR_NEEDLE
Var STR_CONTAINS_VAR_1
Var STR_CONTAINS_VAR_2
Var STR_CONTAINS_VAR_3
Var STR_CONTAINS_VAR_4
Var STR_RETURN_VAR
 
Function StrContains
  Exch $STR_NEEDLE
  Exch 1
  Exch $STR_HAYSTACK
  ; Uncomment to debug
  ;MessageBox MB_OK 'STR_NEEDLE = $STR_NEEDLE STR_HAYSTACK = $STR_HAYSTACK '
    StrCpy $STR_RETURN_VAR ""
    StrCpy $STR_CONTAINS_VAR_1 -1
    StrLen $STR_CONTAINS_VAR_2 $STR_NEEDLE
    StrLen $STR_CONTAINS_VAR_4 $STR_HAYSTACK
    loop:
      IntOp $STR_CONTAINS_VAR_1 $STR_CONTAINS_VAR_1 + 1
      StrCpy $STR_CONTAINS_VAR_3 $STR_HAYSTACK $STR_CONTAINS_VAR_2 $STR_CONTAINS_VAR_1
      StrCmp $STR_CONTAINS_VAR_3 $STR_NEEDLE found
      StrCmp $STR_CONTAINS_VAR_1 $STR_CONTAINS_VAR_4 done
      Goto loop
    found:
      StrCpy $STR_RETURN_VAR $STR_NEEDLE
      Goto done
    done:
   Pop $STR_NEEDLE ;Prevent "invalid opcode" errors and keep the
   Exch $STR_RETURN_VAR  
FunctionEnd
 
!macro _StrContainsConstructor OUT NEEDLE HAYSTACK
  Push `${HAYSTACK}`
  Push `${NEEDLE}`
  Call StrContains
  Pop `${OUT}`
!macroend
 
!define StrContains '!insertmacro "_StrContainsConstructor"'

Function .onInit

ReadRegStr $0 HKCU "Software\Microsoft\Windows\CurrentVersion\App Paths\DCADWIN.EXE" Path
${If} $0 != ""
	StrCpy $1 "DCADWIN.ini"
	${If} ${FileExists} $0$1
		ReadINIStr $2 $0$1 "Paths" "PATH_SUPPORT"
		StrCpy $InstDir $2
		${If} ${FileExists} $InstDir
			${StrContains} $3 ":" $InstDir
			StrCmp "" $3 notfound
				Goto done
			notfound:
				StrCpy $InstDir $0$2
			done:
		${Else}
			StrCpy $InstDir $0$2
		${EndIf}
		StrCpy $DCSupDir $InstDir
		
	
		ReadINIStr $2 $0$1 "Paths" "PATH_MACROS"
		StrCpy $InstDir $2
		${If} ${FileExists} $InstDir
			${StrContains} $3 ":" $InstDir
			StrCmp "" $3 notfound1
				Goto done1
			notfound1:
				StrCpy $InstDir $0$2
			done1:
		${Else}
			StrCpy $InstDir $0$2
		${EndIf}
	${EndIf}
	${If} ${FileExists} $InstDir
	${Else}
		StrCpy $1 "DCX\"
		StrCpy $InstDir $0$1
		StrCpy $1 "Support Files"
		StrCpy $DCSupDir $0$1
		${If} ${FileExists} $InstDir
		${Else} 
			StrCpy $InstDir ""
		${EndIf}
		${If} $InstDir == ""
			StrCpy $1 "\DCX\"
			StrCpy $InstDir $0$1
			StrCpy $1 "\Support Files"
			StrCpy $DCSupDir $0$1
			${If} ${FileExists} $InstDir
			${Else}
				StrCpy $InstDir ""
			${EndIf}
		${EndIf}
		${If} $InstDir == ""
			StrCpy $1 "Macros\"
			StrCpy $InstDir $0$1
			StrCpy $1 "Support Files"
			StrCpy $DCSupDir $0$1
			${If} ${FileExists} $InstDir
			${Else}
				StrCpy $InstDir ""
			${EndIf}
		${EndIf}
		${If} $InstDir == ""
			StrCpy $1 "\Macros\"
			StrCpy $InstDir $0$1
			StrCpy $1 "\Support Files"
			StrCpy $DCSupDir $0$1
			${If} ${FileExists} $InstDir
			${Else}
				StrCpy $InstDir ""
			${EndIf}
		${EndIf}
	${EndIf}
${Else}
	ReadRegStr $0 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\DCADWIN.exe" Path
	${If} $0 != ""
		StrCpy $1 "DCADWIN.ini"
		${If} ${FileExists} $0$1
			ReadINIStr $2 $0$1 "Paths" "PATH_SUPPORT"
			StrCpy $InstDir $2
			${If} ${FileExists} $InstDir
			${Else}
				StrCpy $InstDir $0$2
			${EndIf}
			StrCpy $DCSupDir $InstDir
			
			ReadINIStr $2 $0$1 "Paths" "PATH_MACROS"
			StrCpy $InstDir $2
			${If} ${FileExists} $InstDir
			${Else}
				StrCpy $InstDir $0$2
			${EndIf}
		${EndIf}
		${If} ${FileExists} $InstDir
		${Else}
			StrCpy $1 "DCX\"
			StrCpy $InstDir $0$1
			StrCpy $1 "Support Files"
			StrCpy $DCSupDir $0$1
			${If} ${FileExists} $InstDir
			${Else} 
				StrCpy $InstDir ""
			${EndIf}
			${If} $InstDir == ""
				StrCpy $1 "\DCX\"
				StrCpy $InstDir $0$1
				StrCpy $1 "\Support Files"
				StrCpy $DCSupDir $0$1
				${If} ${FileExists} $InstDir
				${Else}
					StrCpy $InstDir ""
				${EndIf}
			${EndIf}
			${If} $InstDir == ""
				StrCpy $1 "Macros\"
				StrCpy $InstDir $0$1
				StrCpy $1 "Support Files"
				StrCpy $DCSupDir $0$1
				${If} ${FileExists} $InstDir
				${Else}
					StrCpy $InstDir ""
				${EndIf}
			${EndIf}
			${If} $InstDir == ""
				StrCpy $1 "\Macros\"
				StrCpy $InstDir $0$1
				StrCpy $1 "\Support Files"
				StrCpy $DCSupDir $0$1
				${If} ${FileExists} $InstDir
				${Else}
					StrCpy $InstDir ""
				${EndIf}
			${EndIf}
		${EndIf}
	${ElseIf} ${FileExists} "C:\DataCAD 24\macros"
		StrCpy $InstDir "C:\DataCAD 24\macros\"
		StrCpy $DCSupDir "C:\DataCAD 24\Support Files"
	${ElseIf} ${FileExists} "C:\DataCAD 23\macros"
		StrCpy $InstDir "C:\DataCAD 23\macros\"
		StrCpy $DCSupDir "C:\DataCAD 23\Support Files"
	${ElseIf} ${FileExists} "C:\DataCAD 22\macros"
		StrCpy $InstDir "C:\DataCAD 22\macros\"
		StrCpy $DCSupDir "C:\DataCAD 22\Support Files"
	${ElseIf} ${FileExists} "C:\DataCAD 21\macros"
		StrCpy $InstDir "C:\DataCAD 21\macros\"
		StrCpy $DCSupDir "C:\DataCAD 21\Support Files"
	${ElseIf} ${FileExists} "C:\DataCAD 20\macros"
		StrCpy $InstDir "C:\DataCAD 20\macros\"
		StrCpy $DCSupDir "C:\DataCAD 20\Support Files"
	${ElseIf} ${FileExists} "C:\DataCAD 19\macros"
		StrCpy $InstDir "C:\DataCAD 19\macros\"
		StrCpy $DCSupDir "C:\DataCAD 19\Support Files"
	${ElseIf} ${FileExists} "C:\DataCAD 18\macros"
		StrCpy $InstDir "C:\DataCAD 18\macros\"
		StrCpy $DCSupDir "C:\DataCAD 18\Support Files"
	${ElseIf} ${FileExists} "C:\DataCAD 17\macros"
		StrCpy $InstDir "C:\DataCAD 17\macros\"
		StrCpy $DCSupDir "C:\DataCAD 17\Support Files"
	${ElseIf} ${FileExists} "C:\DataCAD 16\macros"
		StrCpy $InstDir "C:\DataCAD 16\macros\"
		StrCpy $DCSupDir "C:\DataCAD 16\Support Files"
	${ElseIf} ${FileExists} "C:\DataCAD 15\macros"
		StrCpy $InstDir "C:\DataCAD 15\macros\"
		StrCpy $DCSupDir "C:\DataCAD 15\Support Files"
	${ElseIf} ${FileExists} "C:\DataCAD 14\macros"
		StrCpy $InstDir "C:\DataCAD 14\macros\"
		StrCpy $DCSupDir "C:\DataCAD 14\Support Files"
	${ElseIf} ${FileExists} "C:\DataCAD 13\macros"
		StrCpy $InstDir "C:\DataCAD 13\macros\"
		StrCpy $DCSupDir "C:\DataCAD 13\Support Files"
	${ElseIf} ${FileExists} "C:\DataCAD 12\macros"
		StrCpy $InstDir "C:\DataCAD 12\macros\"
		StrCpy $DCSupDir "C:\DataCAD 12\Support Files"
	${Else}
		StrCpy $InstDir "C:\DataCAD\Macros\"
		StrCpy $DCSupDir "C:\DataCAD\Support Files"
	${EndIf}
${EndIf}

	StrCpy $0 $DCSupDir "" -1
	StrCmp $0 "\" 0 +2
	StrCpy $DCSupDir $DCSupDir -1
	StrCpy $0 $InstDir "" -1
	StrCmp $0 "\" 0 +2
	StrCpy $InstDir $InstDir -1
	
	StrCpy $DCInstDir $InstDir

  InitPluginsDir
  File /oname=$PLUGINSDIR\cad.ini "cad.ini"
  File /oname=$PLUGINSDIR\Directories.ini "Directories.ini"
  WriteIniStr $PLUGINSDIR\Directories.ini "Field 4" "State" "$DCSupDir"
  WriteIniStr $PLUGINSDIR\Directories.ini "Field 2" "State" "$InstDir"
FunctionEnd


Function SetCustom

  ;Display the InstallOptions dialog

  Push ${TEMP1}

    InstallOptions::dialog "$PLUGINSDIR\cad.ini"
    Pop ${TEMP1}

  Pop ${TEMP1}
  
  ReadINIStr $Spirit "$PLUGINSDIR\cad.ini" "Field 2" "State"
  ${If} $Spirit == 1
	${If} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2028\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2028\130_Macros"
	${ElseIf} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2027\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2027\130_Macros"
	${ElseIf} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2026\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2026\130_Macros"
	${ElseIf} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2025\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2025\130_Macros"
	${ElseIf} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2024\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2024\130_Macros"
	${ElseIf} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2023\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2023\130_Macros"
	${ElseIf} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2022\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2022\130_Macros"
	${ElseIf} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2021\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2021\130_Macros"
	${ElseIf} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2020\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2020\130_Macros"
	${ElseIf} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2019\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2019\130_Macros"
	${ElseIf} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2018\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2018\130_Macros"
	${ElseIf} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2017\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2017\130_Macros"
	${ElseIf} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2016\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2016\130_Macros"
	${ElseIf} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2015\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2015\130_Macros"
	${ElseIf} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2014\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2014\130_Macros"
	${ElseIf} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2013\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2013\130_Macros"
	${ElseIf} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2012\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2012\130_Macros"
	${ElseIf} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2011\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2011\130_Macros"
	${ElseIf} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2010\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2010\130_Macros"
	${ElseIf} ${FileExists} "C:\Program Files (x86)\STI\SPIRIT2009\100_Systemfiles\lan\EN-US\160_Language Files"
		StrCpy $InstDir "C:\ProgramData\STI\SPIRIT2009\130_Macros"
	${Else}
		StrCpy $InstDir "C:\ProgramData\STI\SPIRITxxxx\130_Macros"
	${EndIf}
	StrCpy $SupDir $InstDir
    WriteIniStr $PLUGINSDIR\Directories.ini "Field 4" "State" "(this field not used for Spirit)";$SupDir ;"C:\Program Files (x86)\STI\SPIRIT2017\100_Systemfiles\lan\EN-US\160_Language Files"
	WriteIniStr $PLUGINSDIR\Directories.ini "Field 4" "Left"  "500"
	WriteIniStr $PLUGINSDIR\Directories.ini "Field 4" "Right"  "500"
 	WriteIniStr $PLUGINSDIR\Directories.ini "Field 2" "Left"  "70"
    WriteIniStr $PLUGINSDIR\Directories.ini "Field 2" "State" $InstDir
	WriteIniStr $PLUGINSDIR\Directories.ini "Field 1" "Text" "";"Language Files Directory:"
	WriteIniStr $PLUGINSDIR\Directories.ini "Field 5" "Text" "The macro file (ShadAnal.dcx) will be installed in the Macro Directory.\r\nA dhsoftware folder will also be created in this directory for other required files.\r\n\r\nPlease check the location below  and correct it if necessary\r\nbefore proceeding:"
  ${Else}
	WriteIniStr $PLUGINSDIR\Directories.ini "Field 4" "Left"  "85"
	WriteIniStr $PLUGINSDIR\Directories.ini "Field 4" "Right"  "255"
    WriteIniStr $PLUGINSDIR\Directories.ini "Field 4" "State" "$DCSupDir"
 	WriteIniStr $PLUGINSDIR\Directories.ini "Field 2" "Left"  "85"
    WriteIniStr $PLUGINSDIR\Directories.ini "Field 2" "State" "$DCInstDir"
	WriteIniStr $PLUGINSDIR\Directories.ini "Field 1" "Text" "Support Files Directory:"
	WriteIniStr $PLUGINSDIR\Directories.ini "Field 5" "Text" "Macro files (ShadAnalysis.dmx and ShadAnal.dcx) will be installed in the\r\nMacro Directory.\r\nOther required files will be installed in a dhSoftware folder created within\r\nthe Support Files directory.\r\n\r\nCheck the locations shown below and correct them if necessary before\r\nproceeding:"
  ${EndIf}
  
  
  Push ${TEMP2}

    InstallOptions::dialog "$PLUGINSDIR\Directories.ini"
    Pop ${TEMP2}
  	;WriteINIStr ${DCMainDIR} "$PLUGINSDIR\test.ini" "Field 2" "State"

  Pop ${TEMP2}

FunctionEnd




; Request application privileges for Windows Vista
RequestExecutionLevel user

DirText "You should install to your existing DataCAD macro folder.  Ensure that the Destination Folder below is correct before proceeding."  "Enter existing DataCAD Macros Folder (DCX folder in early DataCAD versions)" "" "Browse for Existing DataCAD Macros or DCX Folder:"

;--------------------------------

; Pages

Page license
Page custom SetCustom ValidateCustom ": Install Directories" ;Custom page. InstallOptions gets called in SetCustom.
;Page directory
Page instfiles

; PageEx license
;   LicenseText "ReadMe"
;   LicenseData "ReadMe.txt"
;	 LicenseForceSelection off

; PageExEnd



;--------------------------------
BrandingText  /TRIMCENTER "dhSoftware"
Caption "Shadow Analysis v1.0"
LicenseData "License.txt"
LicenseForceSelection checkbox "I Accept"
;LicenseForceSelection radiobuttons "I Accept" "I Decline"
; The stuff to install
Section "" ;No components page, name is not important


	${StrContains} $3 "Program Files" $InstDir$SupDir
	StrCmp "Program Files" $3 found
	Goto done
	found:
	  UserInfo::GetAccountType
		pop $0
		${If} $0 != "admin" ;not running as Admin. There will probably be errors trying to install to Program Files if dhsoftware folder does not already exist.
			MessageBox mb_iconinformation "You may need administrator access for this install.$\nIf the installation fails then Abort/Cancel and then run the install program as Administrator (right click and select 'Run as administrator')."
		${EndIf}
	done:

  ${If} $Spirit == 1
	StrCpy $SupDir $InstDir
  ${EndIf}


  

  ; Set output path to the installation directory.
  SetOutPath $InstDir
  
  ; Put file there
	File "C:\DCAL\Projects\shadowstatistics\Classic\ShadAnal.dcx"
	${If} $Spirit == 0
		File "C:\DCAL\Projects\shadowstatistics\D4D\ShadAnalysis.dmx"
	${EndIf}
	StrCpy $0 $SupDir
	StrCpy $1 "\dhsoftware"
	SetOutPath $0$1
	

	${If} $Spirit == 1
		File /oname=ShadAnalysis1.exe "C:\DataCAD 21.1\Support Files\dhsoftware\ShadAnalysis.exe"
		File /oname=ShadAnalysis.exe "C:\DCAL\Projects\shadowstatistics\Classic\Spirit\Win32\Release\ShAnalInvoker.exe"
	${Else}
		File "C:\DataCAD 21.1\Support Files\dhsoftware\ShadAnalysis.exe"
	${EndIf}
	File "C:\DCAL\Projects\shadowstatistics\Classic\Help\Win32\Release\ShadAnalHelp.exe"
	File "C:\DCAL\Projects\shadowstatistics\docs\ShadowAnalysis.pdf"
	File "C:\DataCAD 21.1\Support Files\dhsoftware\DefaultShadStat1.xltx"
	File "C:\DataCAD 21.1\Support Files\dhsoftware\DefaultShadStat1.xlsx"
	File "C:\DataCAD 21.1\Support Files\dhsoftware\DefaultShadStat2.xltx"
	File "C:\DataCAD 21.1\Support Files\dhsoftware\DefaultShadStat2.xlsx"
  
  
SectionEnd ; end the section

Function ValidateCustom

  ReadINIStr $SupDir "$PLUGINSDIR\Directories.ini" "Field 4" "State"
  ReadINIStr $InstDir "$PLUGINSDIR\Directories.ini" "Field 2" "State"
  ReadINIStr $Spirit "$PLUGINSDIR\cad.ini" "Field 2" "State"
  
FunctionEnd

