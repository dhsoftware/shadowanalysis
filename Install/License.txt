This software is distributed free of charge and WITHOUT WARRANTY. 
You are not required to pay for it, but if you find it useful then a financial contribution towards the cost of its development and distribution is expected and would be appreciated. Contributions can be made using the link on the 'About' information displayed by the macro, or using the Contribute link at the dhsoftware.com.au website.

You may distribute this software to others provided that you distribute the complete unaltered installation file provided by me at the dhsoftware.com.au web site, and that you do so free of charge (This includes not  charging for distribution media and not charging for any accompanying software that is on the same media or contained in the same download or distribution file). If you wish to make any charge at all you need to obtain specific permission from me.

Whilst it is free (or because of this) I would like and expect that if you can think of any improvements or spot any bugs (or even spelling or formatting errors in the documentation) that you would let me know.  Your feedback will help with future development of the macro.


