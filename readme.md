This source is made publicly available on the understanding that it is copyright David Henderson 2020 and is not 'open source'. 
You are invited to look at it for your own inspiration, but you  must not make changes and then make a competing product 
available to others. You can make changes for your own (or your company's) use, however in general I would prefer that you 
let me know of your requirements so that I can consider including them in a future release of my software. 
If in doubt please contact me.                                           