unit Hilite;

interface

  uses UInterfaces, URecords, UConstants;

  /// <summary> Hilites (or unhilites) and entity
  /// </summary>
  /// <param name="ent">The entity to hilite (or unhilite)
  /// </param>
  /// <param name="clr">The colour to hilite the entity with. Zero indicates to UNhilite.
  /// </param>
  /// <remarks> Colour of zero causes the entity to be UNhilited
  /// </remarks>
  procedure HiliteEnt (var ent : entity; clr : integer);

  /// <summary> Hilites (or unhilites) all entities in a selection set
  /// </summary>
  /// <param name="SetNum">The number of the selection set to hilite (or unhilite)
  /// </param>
  /// <param name="clr">The colour to hilite with. Zero indicates to UNhilite.
  /// </param>
  /// <remarks> Clr of zero causes the entity to be UNhilited
  /// </remarks>
  procedure HiliteSelSet (SetNum : integer; clr : integer);


  /// <summary> Hilites (or unhilites) entities that either have the specified attribute
  ///           themselves, or are on a layer that has the attribute
  /// </summary>
  /// <param name="AtribName">The attribute to look for
  /// </param>
  /// <param name="Colour">The colour to hilite the entities with. Zero indicates to unhilite.
  /// </param>
  /// <remarks> Colour of zero causes the entities to be UNhilited
  /// </remarks>
  procedure HiliteByAtr (AtribName : atrname; Colour : integer);

implementation

  procedure HiliteEnt (var ent : entity; clr : integer);
  var
    b : boolean;
    svlintp : integer;
    svspacing : double;
  begin
    if clr = 0 then begin
      svlintp := ent.ltype;
      svspacing := ent.spacing;
    end
    else begin
      ent_draw (ent, drmode_black);
      ent.color := clr;
    end;
    ent.ltype := ltype_dashed;
    ent.spacing := pixsize * 25.0;
    ent.Width := ent.Width+1;
    b := pgSaveVar^.showwgt;
    pgSaveVar^.showwgt := true;
    if clr = 0 then begin
      ent_draw_dl(ent, drmode_black, true);
      ent.ltype := svlintp;
      ent.spacing := svspacing;
      ent.Width := ent.Width-1;
      ent_draw_dl (ent, drmode_white, true);
    end
    else
      ent_draw_dl (ent, drmode_white, true);
    pgSaveVar^.showwgt := b;
  end;

  procedure HiliteSelSet (SetNum : integer; clr : integer);
  var
    mode : mode_type;
    addr : entaddr;
    ent  : entity;
  begin
    mode_init (mode);
    mode_lyr (mode, lyr_all);
    mode_ss (mode, SetNum-1);
    addr := ent_first (mode);
    while ent_get (ent, addr) do begin
      HiliteEnt(ent, clr);
      addr := ent_next (ent, mode);
    end;
  end;

  procedure HiliteByAtr (AtribName : atrname; colour : integer);
  /// colour of zero will UNhilite
  var
    filler : byte;
    mode : mode_type;
    atr : attrib;
    ent : entity;
    addr : entaddr;
    lyr : lyraddr;
    rlyr : Rlayer;
    i   : integer;
  begin
    lyr := lyr_first;
    while lyr_get (lyr, rlyr) do begin
      if atr_lyrfind (lyr, AtribName, atr) then begin
        mode_init (mode);
        mode_1lyr (mode, lyr);
        addr := ent_first (mode);
        while ent_get (ent, addr) do begin
          addr := ent_next (ent, mode);
          HiliteEnt (ent, colour);
        end;
      end;
      lyr := rlyr.nxt;
    end;

    mode_init (mode);
    mode_lyr (mode, lyr_all);
    mode_atr (mode, AtribName);
    addr := ent_first (mode);
    while ent_get (ent, addr) do begin
      addr := ent_next (ent, mode);
      HiliteEnt (ent, colour);
    end;

    if atr_sysfind (AtribName, atr) and (atr.atrtype = atr_str) then
    for i := 1 to 8 do begin
      if (length (atr.str) >= i) and (atr.str[i] = ansichar(ord('0') + i)) then begin
        mode_init (mode);
        mode_ss (mode, i-1);
        addr := ent_first (mode);
        while ent_get (ent, addr) do begin
          addr := ent_next (ent, mode);
          HiliteEnt (ent, colour);
          if colour = 0 then
            ent_draw_dl (ent, drmode_white, true);
        end;
      end;
    end;

  end;



end.
