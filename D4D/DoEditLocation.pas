unit DoEditLocation;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Menus,
  Vcl.ComCtrls, Vcl.ExtCtrls, CommonStuff, System.UITypes;

type
  TEditLocation = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    O9h: TLabel;
    Label6: TLabel;
    Label12: TLabel;
    cbTimeZone: TComboBox;
    btnOK: TButton;
    btnCancel: TButton;
    MainMenu1: TMainMenu;
    LongitudeLatitudeFormat1: TMenuItem;
    DegreesMinutesSeconds1: TMenuItem;
    DecimalDegrees1: TMenuItem;
    PageControl1: TPageControl;
    tsDMS: TTabSheet;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    Label7: TLabel;
    Label10: TLabel;
    edLoD: TEdit;
    edLaM: TEdit;
    edLoS: TEdit;
    edLaD: TEdit;
    edLoM: TEdit;
    edLaS: TEdit;
    cbEastWest: TComboBox;
    cbNorthSouth: TComboBox;
    tsDecimal: TTabSheet;
    Label9: TLabel;
    Label11: TLabel;
    edLongitude: TEdit;
    edLatitude: TEdit;
    edCity: TEdit;
    cbCountry: TComboBox;
    chbDS: TCheckBox;
    pnlDS: TPanel;
    cbDSStarts: TComboBox;
    lblDSStarts: TLabel;
    cbDSEnds: TComboBox;
    lblDSEnds: TLabel;
    Label15: TLabel;
    procedure edLongitudeKeyPress(Sender: TObject; var Key: Char);
    procedure edLongitudeChange(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure DegreesMinutesSeconds1Click(Sender: TObject);
    procedure DecimalDegrees1Click(Sender: TObject);
    procedure edLoDKeyPress(Sender: TObject; var Key: Char);
    procedure edLoDChange(Sender: TObject);
    procedure chbDSClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    laststring : string;
  public
    { Public declarations }
  end;

var
  EditLocation: TEditLocation;

implementation

{$R *.dfm}

procedure TEditLocation.btnOKClick(Sender: TObject);
var
  d : double;
begin
  if (length (trim(edCity.Text)) = 0) or (length (trim(cbCountry.Text)) = 0) then
    messagedlg ('Country and City must both be entered', mtError, [mbOK], 0)
  else if not TryStrToFloat(edLongitude.Text, d) then
    messagedlg ('Longitude is invalid', mtError, [mbOK], 0)
  else if (d < -180) or (d > 180) then
    messagedlg ('Longitude must be between 180�W and 180�E', mtError, [mbOK], 0)
  else if not TryStrToFloat(edLatitude.Text, d) then
    messagedlg ('Latitude is invalid', mtError, [mbOK], 0)
  else if (d < -89) or (d > 89) then
    messagedlg ('Latitude must be between 89�S and 89�N', mtError, [mbOK], 0)
  else if cbTimeZone.ItemIndex < 0 then
    messagedlg ('TimeZone is not valid', mtError, [mbOK], 0)
  else if chbDS.Checked and ((cbDSStarts.ItemIndex < 0) or (cbDSEnds.ItemIndex < 0)) then
    messagedlg ('If Daylight Saving is selected then both start and end dates must be specified', mtError, [mbOK], 0)
  else
    ModalResult := mrOK;
end;

procedure TEditLocation.chbDSClick(Sender: TObject);
begin
  pnlDS.Visible := chbDS.Checked;
  pnlDS.Enabled := chbDS.Checked;
  cbDSStarts.Enabled := chbDS.Checked;
  cbDSEnds.Enabled := chbDS.Checked;
end;

procedure TEditLocation.DecimalDegrees1Click(Sender: TObject);
begin
  PageControl1.ActivePage := tsDecimal;
end;

procedure TEditLocation.DegreesMinutesSeconds1Click(Sender: TObject);
begin
  PageControl1.ActivePage := tsDMS;
end;

procedure TEditLocation.edLoDChange(Sender: TObject);
var
  i : integer;
  lod, lom, los, lad, lam, las : integer;
  d : double;
begin
  if PageControl1.ActivePage = tsDMS then begin
    if (Sender is TEdit) then begin
      if not (((Sender as TEdit).Text = '') or TryStrToInt((Sender as TEdit).Text, i)) then
        (Sender as TEdit).Text := lastString;
      if not TryStrToInt((Sender as TEdit).Text, i) then begin
        (Sender as TEdit).Text := '';
      end;
    end;

    if not TryStrToInt(edLoD.Text, lod) then lod := 0;
    if not TryStrToInt(edLoM.Text, lom) then lom := 0;
    if not TryStrToInt(edLoS.Text, los) then los := 0;
    d := round((lod + lom/60 + los/3600)*100000000)/100000000;
    if cbEastWest.ItemIndex = 1 then d := -d;
    edLongitude.Text := FloatToStr(d);

    if not TryStrToInt(edLaD.Text, lad) then lad := 0;
    if not TryStrToInt(edLaM.Text, lam) then lam := 0;
    if not TryStrToInt(edLaS.Text, las) then las := 0;
    d := round((lad + lam/60 + las/3600)*100000000)/100000000;
    if cbNorthSouth.ItemIndex = 1 then d := -d;
    edLatitude.Text := FloatToStr(d);

  end;
end;

procedure TEditLocation.edLoDKeyPress(Sender: TObject; var Key: Char);
begin
  laststring := (Sender as TEdit).Text;
  if CharInSet (key, [' '..'/', ':'..'~']) then
    key := #0;
end;

procedure TEditLocation.edLongitudeChange(Sender: TObject);
var
  d,m,s : string;
  i : integer;
  v : double;
begin
  if PageControl1.ActivePage = tsDecimal then begin
    s := (TEdit(Sender)).Text;
    i := 1;
    while i < length(s) do begin
      if not CharInSet (s[i], ['0'..'9', '.', '+', '-']) then
        delete (s, i, 1)
      else begin
        if CharInSet(s[i], ['+', '-']) and (i <> 1) then
          delete (s, i, 1)
        else
          i := i + 1;
      end;
    end;

    if TryStrToFloat(s, v) then begin
      (TEdit(Sender)).Text := s;
      if v < 0 then begin
        i := 1; //item index for north/south or east/west combobox
        v := -v;
      end
      else
        i := 0;
      d := inttostr (trunc(v));
      v := (v-trunc(v))*60;
      m := inttostr (trunc (v));
      v := (v-trunc(v))*60;
      s := inttostr (round (v));

      if (TEdit (sender)).Tag = 0 then begin  // longitude
        cbEastWest.ItemIndex := i;
        edLoD.Text := d;
        edLoM.Text := m;
        edLoS.Text := s;
      end
      else begin  // latitude
        cbNorthSouth.ItemIndex := i;
        edLaD.Text := d;
        edLaM.Text := m;
        edLaS.Text := s;
      end;

    end
    else if (TEdit(Sender)).Text <> '-' then  // allow them to enter a negative sign before entereing any numbers
      (TEdit(Sender)).Text := laststring;
  end;
end;

procedure TEditLocation.edLongitudeKeyPress(Sender: TObject; var Key: Char);
begin
  if CharInSet (key, [' '..'*', ',','/',':'..'~']) then
    key := #0
  else
    laststring := (sender as TEdit).Text;
end;

procedure TEditLocation.FormCreate(Sender: TObject);
var
  i : integer;
begin
  cbDSStarts.Items.Clear;
  for i := 0 to length (DSStartValues)-1 do
    cbDSStarts.Items.Add(DSStartValues[i]);
  cbDSEnds.Items.Clear;
  for i := 0 to length (DSEndValues)-1 do
    cbDSEnds.Items.Add(DSEndValues[i]);
end;

end.
