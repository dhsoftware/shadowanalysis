unit CommonStuff;

interface

  uses URecords, UInterfacesRecords, SysUtils;

  type
    ShadStatL = record
      state   : integer;
      surface,
      leader,
      head    : entity;
      horizontal,
      otherface : boolean;
      north   : double;
      arrowsz : double;
      GroupSelected : array [1..2] of boolean;
      SelTp   : array[3..4] of integer;
      Append  : boolean;
      pt1     : point;
      c1Set   : integer;
      c2Set   : integer;
      c1Lyr   : lyrAddr;
      c2Lyr   : lyrAddr;
      HiClrs  : array [0..3] of integer;
      SelSets : array [1..2, 0..7] of boolean;
      lastkey : integer;
      LocationIniFile : string;
      Country,
      City    : string;
      Longitude,
      Latitude: double;
      timezone : integer;
      daylightsaving : boolean;
      DS_start,
      DS_end : integer;
      case byte of
         0: (getp: getpointarg);
         1: (gete: getEscArg);
         2: (getl: TFLyrArg);
         3: (gesc: globescArg);
         4: (getc: getcharArg);
    end;
    pShadStatL = ^ShadStatL;

  var
    l : pShadStatL;

  const
    ShadStatSurf = 'ShadStatSurf';
    ShadStatGrp1 = 'ShadStatGrp1';
    ShadStatGrp2 = 'ShadStatGrp2';
    ShadStatFace = 'ShadStatFace';

    ShadStatSel1 = 'ShadStatSel1';
    ShadStatSel2 = 'ShadStatSel2';

    ShadStatHiSf = 'ShadStatHiSf';
    ShadStatHiG1 = 'ShadStatHiG1';
    ShadStatHiG2 = 'ShadStatHiG2';
    ShadStatHiAr = 'ShadStatHiAr';

    GeoDataFile = 'GeoDataFile';

    ShadowNorth = 'ShadowNorth';


{$I ../inc/DaylightSaving.inc}

  function BlankString (s: string) : boolean;


implementation

  function BlankString (s: string) : boolean;
  begin
    result := (length(trim(s)) = 0);
  end;


end.
