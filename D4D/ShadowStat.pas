unit ShadowStat;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Geometry,
  UVariables, System.Generics.Collections, Vcl.ComCtrls, inifiles,
  Vcl.Samples.Spin, DateUtils, Math, Vcl.Menus, DoEditLocation;

type
  TForm3 = class(TForm)
    Memo1: TMemo;
    dtStartDate: TDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    cbPeriod: TComboBox;
    Label3: TLabel;
    SEInterval: TSpinEdit;
    Label4: TLabel;
    sePeriod: TSpinEdit;
    Label5: TLabel;
    cbFrequency: TComboBox;
    Button1: TButton;
    MainMenu1: TMainMenu;
    SetLocationFile1: TMenuItem;
    Label6: TLabel;
    cbCountry: TComboBox;
    cbCity: TComboBox;
    lblCity: TLabel;
    lblLongitude: TLabel;
    Label7: TLabel;
    lblLatitude: TLabel;
    Label9: TLabel;
    lblTimezone: TLabel;
    Label11: TLabel;
    ChangelocationFile1: TMenuItem;
    EditCurrentLocation1: TMenuItem;
    AddNewLocation1: TMenuItem;
    Button2: TButton;
    FileOpenDialog1: TFileOpenDialog;
    procedure cbPeriodChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Process;
    procedure FormCreate(Sender: TObject);
    procedure cbCountryChange(Sender: TObject);
    procedure cbCityChange(Sender: TObject);
    procedure EditCurrentLocation1Click(Sender: TObject);
    procedure AddNewLocation1Click(Sender: TObject);
    procedure ChangelocationFile1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
    procedure SetUpComboBoxes;
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

{$I ../inc/timezone.inc}



Procedure TForm3.Process;

type
  TransformTp = (rotate, rotateRel, Move);
  TransformHistTp = record
    transform : TransformTp;
    case TransformTp of
      rotate,
      rotateRel : (ang : double;
                   axis : integer;
                   relpnt : point);
      move      : (vect : point);
    end;


var
  HistItem : TransformHistTp;
  TransformHist : TList<TransformHistTp>;
  date, enddate, time : TDateTime;
  minp, maxp : point;
  RotateDone : boolean;
  i, j : integer;
  tanang, ang : double;
  miny, maxy : double;

begin
  if (lblLongitude.Caption = '') or (lblLatitude.Caption = '') or (lblTimezone.Caption = '') then begin
    messagedlg ('You must select a valid Country/City before processing', mtError, [mbOK], 0);
    exit;
  end;
  // rotate everything so that surface is horizontal

  // find the max & min x values of the points in the surface polygon - then use the corresponding points to create
  // a rotation transformation around the y axis
  ply_extent (surface, minp, maxp);

  TransformHist := TList<TransformHistTp>.Create;
  try
    // check that minp & maxp x & y values are different.  If not then surface is vertical and parallel
    // to either the y or x axis, so just a single rotation of -90 degrees around the y or x axis is required.
    if RealEqual (minp.x, maxp.x) then begin
      HistItem.transform := rotate;
      HistItem.ang := -halfpi;
      HistItem.axis := y;
      RotatePoints (surface, length(surface), -halfpi, y);
      SurfaceSide := RotatePoint (SurfaceSide, -halfpi, y);
      RotateDone := true;
      TransformHist.Add(HistItem);
      minp := RotatePoint (minp, -halfpi, y);
      maxp := RotatePoint (maxp, -halfpi, y);
    end
    else if RealEqual (minp.y, maxp.y) then begin
      HistItem.transform := rotate;
      HistItem.ang := -halfpi;
      HistItem.axis := x;
      RotatePoints (surface, length(surface), -halfpi, x);
      SurfaceSide := RotatePoint (SurfaceSide, -halfpi, x);
      RotateDone := true;
      TransformHist.Add(HistItem);
      minp := RotatePoint (minp, -halfpi, x);
      maxp := RotatePoint (maxp, -halfpi, x);
    end
    else if RealEqual (minp.z, maxp.z) then begin
      RotateDone := true;
    end
    else begin
      // set minp & Maxp to the actual points with min & max x values.
      minp := surface[0];
      maxp := minp;
      FOR i := 1 to length(surface)-1 DO BEGIN
        if surface[i].x < minp.x then
          minp := surface[i];
        if surface[i].x > maxp.x then
          maxp := surface[i];
      END;

      // rotate about the z axis so that the line between max & min is
      // parallel to the x axis
      RotateDone := false;
      tanang := (maxp.y - minp.y)/(maxp.x - minp.x);
      ang := arctan(tanang);
      HistItem.transform := rotateRel;
      HistItem.ang := -ang;
      HistItem.axis := z;
      HistItem.relpnt := minp;
      TransformHist.Add(HistItem);
      RotatePointsRel(surface, length(surface), minp, -ang, z);
      SurfaceSide := RotatePointRel (SurfaceSide, minp, -ang, z);
      maxp := RotatePointRel (maxp, minp, -ang, z);

      // find min and max y values of modified surface
      miny := surface[0].y;
      maxy := surface[0].y;

      FOR i:=1 to length(surface)-1 DO BEGIN
        miny := min (miny, surface[i].y);
        maxy := max (maxy, surface[i].y);
      END;


      if RealEqual (miny, maxy) then begin
        // it is vertical, and now parallel to the x axis, so just needs
        // to be rotated about the x axis;
        HistItem.transform := rotate;
        HistItem.ang := -halfpi;
        HistItem.axis := x;
        TransformHist.Add(HistItem);

        RotatePoints(surface, length(surface), -halfpi, x);
        SurfaceSide := RotatePoint (SurfaceSide, -halfpi, x);
        RotateDone := true;
      end
      else begin
        // not vertical, calculate rotation about the y axis
        tanang := (maxp.z - minp.z)/(maxp.x - minp.x);
        ang := arctan(tanang);
        HistItem.transform := rotateRel;
        HistItem.ang := ang;
        HistItem.relpnt := minp;
        HistItem.axis := y;
        TransformHist.Add(HistItem);

        RotatePointsRel(surface, length(surface), minp, ang, y);
        SurfaceSide := RotatePointRel (SurfaceSide, minp, ang, y);
      end;
    end;

    // add a rotation around the x axis if required
    // find the max & min y values of the points in the mod_surf polygon - then use the corresponding points
    // to create a rotation transformation around the y axis
    IF not RotateDone THEN BEGIN
      minp := surface[0];
      maxp := minp;
      FOR i := 1 to length(surface)-1 DO BEGIN
        if surface[i].y < minp.y then
          minp := surface[i];
        if surface[i].y > maxp.y then
          maxp := surface[i];
      END;
      tanang := (maxp.z - minp.z	)/(maxp.y - minp.y);
      ang := arctan(tanang);

      HistItem.transform := rotate;
      HistItem.ang := -ang;
      HistItem.axis := x;
      TransformHist.Add(HistItem);

      RotatePoints(surface, length(surface)-1, -ang, x);
      SurfaceSide := RotatePoint (SurfaceSide, -ang, x);
    END;

    // polygon should now be horizontal ... move it to a z-height of zero          HistItem.transform := rotate;
    if abs(surface[0].z) > NearZero then begin
      HistItem.transform := Move;
      HistItem.vect.x := 0;
      HistItem.vect.y := 0;
      HistItem.vect.z := -surface[0].z;
      TransformHist.Add(HistItem);
      SurfaceSide := MovePoint(SurfaceSide, HistItem.vect);
    end;
    for i := 0 to length(surface)-1 do
      surface[i].z := 0.0;

    // polygon is now horizontal.  If SurfaceSide is below it then we need to
    // flip it over.
    if SurfaceSide.z < 0 then begin
      HistItem.transform := rotate;
      HistItem.ang := pi;
      HistItem.axis := x;
      TransformHist.Add(HistItem);
      RotatePoints(surface, length(surface)-1, pi, x);
    end;

    // apply same transformations to group1 & group2
    for i := 0 to TransformHist.Count-1 do
      case TransformHist[i].transform of
        rotate :    begin
                      for j := 0 to Group1.Count-1 do
                        RotatePoints (Group1[j], length(Group1[j])-1,
                                      TransformHist[i].ang,
                                      TransformHist[i].axis);
                      for j := 0 to Group2.Count-1 do
                        RotatePoints (Group2[j], length(Group2[j])-1,
                                      TransformHist[i].ang,
                                      TransformHist[i].axis);
                    end;
        rotateRel : begin
                      for j := 0 to Group1.Count-1 do
                        RotatePointsRel (Group1[j], length(Group1[j])-1,
                                         TransformHist[i].relpnt,
                                         TransformHist[i].ang,
                                         TransformHist[i].axis);
                      for j := 0 to Group2.Count-1 do
                        RotatePointsRel (Group2[j], length(Group2[j])-1,
                                         TransformHist[i].relpnt,
                                         TransformHist[i].ang,
                                         TransformHist[i].axis);
                    end;
        move :      begin
                      for j := 0 to Group1.Count-1 do
                        MovePoints (Group1[j], length(Group1[j])-1,
                                    TransformHist[i].vect.x,
                                    TransformHist[i].vect.y,
                                    TransformHist[i].vect.z);
                      for j := 0 to Group2.Count-1 do
                        MovePoints (Group2[j], length(Group2[j])-1,
                                    TransformHist[i].vect.x,
                                    TransformHist[i].vect.y,
                                    TransformHist[i].vect.z);
                    end;
      end;



    date := dtStartDate.DateTime;
    case cbPeriod.ItemIndex of
      0 : enddate := IncDay (date, SEInterval.Value);
      1 : enddate := IncWeek (date, SEInterval.Value);
      2 : enddate := IncMonth (date, SEInterval.Value);
      3 : enddate := incYear (date, SEInterval.Value)
      else exit;
    end;

    while date < enddate do begin
      time := date;
      while time < incDay (date, 1) do begin

      end;



      case cbFrequency.ItemIndex of
        0 : date := IncDay(date, 1);
        1 : date := IncDay(date, 2);
        2 : date := IncWeek(date, 1);
        3 : date := IncMonth(date, 1);
      end;
    end;
  finally
    TransformHist.free;
  end;
end;   //Process


procedure TForm3.AddNewLocation1Click(Sender: TObject);
var
  ini : TIniFile;
begin
  EditLocation.cbCountry.Enabled := true;
  EditLocation.cbCountry.Items := cbCountry.Items;
  EditLocation.cbCountry.ItemIndex := cbCountry.ItemIndex;
  EditLocation.edCity.Enabled := true;
  EditLocation.edCity.Text := '';
  EditLocation.cbTimeZone.ItemIndex := -1;
  EditLocation.Caption := 'Add Location';
  EditLocation.ShowModal;
  if EditLocation.ModalResult = mrOK then begin
    ini := TiniFile.Create (LocationIniFile);
    ini.WriteString(EditLocation.edCity.Text, 'country', EditLocation.cbCountry.Text);
    ini.WriteString(EditLocation.edCity.Text, 'longitude', EditLocation.edLongitude.Text);
    ini.WriteString(EditLocation.edCity.Text, 'latitude', EditLocation.edLatitude.Text);
    ini.WriteString (EditLocation.edCity.Text, 'timezone', TimeZoneString(EditLocation.cbTimeZone.ItemIndex));
    if cbCountry.Items.IndexOf(EditLocation.cbCountry.Text) < 0 then
      cbCountry.Items.Add(EditLocation.cbCountry.Text);
    cbCountry.ItemIndex := cbCountry.Items.IndexOf(EditLocation.cbCountry.Text);
    cbCountryChange (Sender);
    cbCity.ItemIndex := cbCity.Items.IndexOf(EditLocation.edCity.Text);
    cbCityChange(Sender);
  end;
end;

procedure TForm3.Button1Click(Sender: TObject);
var
  i, j, plycnt, ndx: integer;
  F: TFileStream;
  buffer4 : array [0..2] of single;
  buffer8 : array [0..2] of double;
  s : string;
  realsize, pntcnt : smallint;
  pgn : pntarr;
  ini : TIniFile;

begin
{  a1 := CrossProductPt (Vector3d(0,0,0), Vector3d(0,2,0), Vector3d(1,1,1));
  a2 := CrossProductPt (Vector3d(1,1,1), Vector3d(0,2,0), Vector3d(0,0,0));
  s := floattostr (a1.z) + '  !  ' + floattostr (a2.z);
  messagedlg (s, mtInformation, [mbOK], 0);
}
  if (Trim(cbCity.Text) = '') or (Trim(cbCountry.Text) = '') then begin
    messagedlg ('Country and City need to be selected before proceeding', mtError, [mbOK], 0);
    exit;
  end;

  ini := TIniFile.Create (ConfigIniFile);
  ini.WriteString('ShadowStats', 'GeoDataFile', LocationIniFile);
  ini.WriteString('ShadowStats', 'Town', cbCity.text);
  ini.WriteString('ShadowStats', 'Country', cbCountry.text);

  for i := 0 to ParamCount do
    Memo1.Lines.Add(ParamStr(i));

  F := TFileStream.Create (ParamStr(1) + 'ShSf.dat', fmOpenRead);
  Memo1.Lines.Add('Surface:');

  Group1 := TList<pntarr>.create;
  Group2 := TList<pntarr>.create;
  try
    try
      if F.Size > 2 then begin
        F.Read(realsize, 2);

        if F.Size >= 2+4*realsize then begin
          s := '';
          if realsize = 4 then begin
            F.Read(buffer4, 4);
            north := buffer4[0];
            F.Read(buffer4, 12);
            surfaceside.x := buffer4[0];
            surfaceside.y := buffer4[1];
            surfaceside.z := buffer4[2];
            for i := 0 to 2 do begin
              s := s + buffer4[i].ToString + ', ';
            end;
          end
          else begin
            F.Read(buffer8, 8);
            north := buffer8[0];
            F.Read(buffer8, 24);
            surfaceside.x := buffer8[0];
            surfaceside.y := buffer8[1];
            surfaceside.z := buffer8[2];
            for i := 0 to 2 do begin
              s := s + buffer8[i].ToString + ', ';
            end;
          end;
          s := s + '(face indication)';
          Memo1.Lines.Add(s);

        end;

        SetLength (surface, (F.Size-2-3*realsize) div (3*realsize));
        ndx := 0;

        while F.Position < F.Size do
        begin
          s := '';
          if realsize = 4 then begin
            F.Read(buffer4, 12);
            surface[ndx].x := buffer4[0];
            surface[ndx].y := buffer4[1];
            surface[ndx].z := buffer4[2];

            for i := 0 to 2 do begin
              s := s + buffer4[i].ToString + ', ';
            end;
          end
          else begin
            F.Read(buffer8, 24);
            surface[ndx].x := buffer8[0];
            surface[ndx].y := buffer8[1];
            surface[ndx].z := buffer8[2];

            for i := 0 to 2 do begin
              s := s + buffer8[i].ToString + ', ';
            end;
          end;
          ndx := ndx+1;
          Memo1.Lines.Add(s);
        end;
      end;

      F.Free;


      F := TFileStream.Create (ParamStr(1) + 'ShEx.dat', fmOpenRead);
      if F.Size > 2 then begin
        F.Read(realsize, 2);
        plycnt := 1;
        while F.Position < F.Size do
        begin
          Memo1.Lines.Add('Existing ' + plycnt.ToString + ':');
          plycnt := plycnt+1;
          F.Read(pntcnt, 2);

          setlength (pgn, pntcnt);
          ndx := 0;

          for j := 1 to pntcnt do begin
            s := '';
            if realsize = 4 then begin
              F.Read(buffer4, 12);
              pgn[ndx].x := buffer4[0];
              pgn[ndx].y := buffer4[1];
              pgn[ndx].z := buffer4[2];
              for i := 0 to 2 do begin
                s := s + buffer4[i].ToString + ', ';
              end;
            end
            else begin
              F.Read(buffer8, 24);
              pgn[ndx].x := buffer8[0];
              pgn[ndx].y := buffer8[1];
              pgn[ndx].z := buffer8[2];
              for i := 0 to 2 do begin
                s := s + buffer8[i].ToString + ', ';
              end;
            end;
            ndx := ndx+1;
            Memo1.Lines.Add(s);
          end;
          Group1.Add(pgn);
        end;
      end;

      F.Free;


      F := TFileStream.Create (ParamStr(1) + 'ShNw.dat', fmOpenRead);
      if F.Size > 2 then begin
        F.Read(realsize, 2);

        plycnt := 1;
        while F.Position < F.Size do
        begin
          Memo1.Lines.Add('New ' + plycnt.ToString + ':');
          plycnt := plycnt+1;
          F.Read(pntcnt, 2);

          setlength (pgn, pntcnt);
          ndx := 0;

          for j := 1 to pntcnt do begin
            s := '';
            if realsize = 4 then begin
              F.Read(buffer4, 12);
              pgn[ndx].x := buffer4[0];
              pgn[ndx].y := buffer4[1];
              pgn[ndx].z := buffer4[2];
              for i := 0 to 2 do begin
                s := s + buffer4[i].ToString + ', ';
              end;
            end
            else begin
              F.Read(buffer8, 24);
              pgn[ndx].x := buffer8[0];
              pgn[ndx].y := buffer8[1];
              pgn[ndx].z := buffer8[2];
              for i := 0 to 2 do begin
                s := s + buffer8[i].ToString + ', ';
              end;
            end;
            Memo1.Lines.Add(s);
          end;
          Group2.Add(pgn);
        end;
      end;

      F.Free;

      Process;
    except
      messagedlg ('An error occurred importing DataCAD data', mtError, [mbOK], 0);
    end;
  finally
    Group1.Free;
    Group2.Free;
    try
      F.Free;
    except
    end;
  end;

end;

procedure TForm3.Button2Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TForm3.cbCityChange(Sender: TObject);
var
  ini : Tinifile;
  longitude, latitude, timezone : double;

  function formatAngle (ang : double) : string;
  var
    d,m,s : integer;
    r : double;
  begin
    d := Trunc(ang);
    r := (ang - d)*60;
    m := trunc(r);
    r := (r-m)*60;
    s := round (r);
    result := IntToStr(d) + '� ' + IntToStr(m) + ''' ' + IntToStr(s) + '" ';
  end;

  function formatLongitude : string;
  begin
    result := formatAngle (abs(Longitude));
    if longitude < 0 then result := result + 'W'
    else if longitude > 0 then result := result + 'E';
  end;

  function formatLatitude : string;
  begin
    result := formatAngle (abs(Latitude));
    if longitude < 0 then result := result + 'S'
    else if longitude > 0 then result := result + 'N';
  end;



begin   // cbCityChange
  ini := Tinifile.Create(LocationIniFile);
  try
    longitude := ini.ReadFloat(cbCity.Text, 'longitude', 200);
    latitude := ini.ReadFloat(cbCity.Text, 'latitude', 100);
    timezone := ini.ReadFloat(cbCity.Text, 'timezone', 20);
  finally
    ini.Free;
  end;
  if (latitude > 90) or (longitude > 180) or (timezone > 18) then begin

    lblLongitude.Caption := '';
    lblLatitude.Caption := '';
    lblTimezone.Caption := '';
  end
  else begin
    lblLongitude.Caption := formatLongitude;
    lblLatitude.Caption := formatLatitude;
    lblTimezone.Caption := formatTimeZone(timezone);
  end;
end;

procedure TForm3.cbCountryChange(Sender: TObject);
var
  ini : TMeminifile;
  iniSections : TStringList;
  Country, Town : string;
begin
  ini := TMeminifile.Create(LocationIniFile);
  iniSections := TStringList.Create;
  try
    ini.ReadSections(iniSections);
    cbCity.Items.Clear;
    for town in iniSections do begin
      Country := ini.ReadString(town, 'country', '');
      if Country = cbCountry.Text then
        cbCity.Items.Add(town);
    end;
  finally
    ini.Free;
    iniSections.Free;
  end;
  cbCity.ItemIndex := 0;
  cbCityChange (Sender);
end;

procedure TForm3.cbPeriodChange(Sender: TObject);
begin
  case (tComboBox(Sender)).ItemIndex of
    0: SEPeriod.MaxValue := 366;
    1: SEPeriod.MaxValue := 53;
    2: SEPeriod.MaxValue := 12;
    3: SEPeriod.MaxValue := 2;
  end;
  if SEPeriod.Value > SEPeriod.MaxValue then
    SEPeriod.Value := SEPeriod.MaxValue
end;

procedure TForm3.ChangelocationFile1Click(Sender: TObject);
var
  ini : TMemIniFile;
  fl : TextFile;
begin
  FileOpenDialog1.FileName := ExtractFileName (LocationIniFile);
  FileOpenDialog1.DefaultFolder := ExtractFilePath(LocationIniFile);
  FileOpenDialog1.OkButtonLabel := 'Select File';
  FileOpenDialog1.DefaultExtension := 'ini';
  if FileOpenDialog1.Execute then begin
    LocationIniFile := FileOpenDialog1.FileName;
    ini := TMemIniFile.Create(ConfigIniFile);
    try
      ini.WriteString('ShadowStats', 'GeoDataFile', LocationIniFile);
      lasttown := cbCity.text;
      lastcountry := cbCountry.text;
    finally
      ini.Free;
    end;
    ini := TMemIniFile.Create(LocationIniFile);
    try
      lastcountry := ini.ReadString(lasttown, 'country', '');
      if lastcountry = '' then begin
        lasttown := '';
      end;
    finally
      ini.Free;
    end;

    SetupComboBoxes;
  end;
end;

procedure TForm3.EditCurrentLocation1Click(Sender: TObject);
var
  ini : TIniFile;
  s : string;
  d : double;
begin
  EditLocation.edCity.Text := cbCity.Text;
  EditLocation.edCity.Enabled := false;
  EditLocation.cbCountry.Text := cbCountry.Text;
  EditLocation.cbCountry.Enabled := false;
  EditLocation.Caption := 'Edit Location';
  ini := TIniFile.Create(LocationIniFile);
  try
    s := ini.ReadString(cbCity.Text, 'longitude', '');
    EditLocation.edLongitude.Text := s;
    s := ini.ReadString(cbCity.Text, 'latitude', '');
    EditLocation.edLatitude.Text := s;
    d := ini.ReadFloat(cbCity.Text, 'timezone', 0);
    EditLocation.cbTimeZone.ItemIndex :=
                    EditLocation.cbTimeZone.items.IndexOf (formatTimeZone (d));
    EditLocation.PageControl1.ActivePage := EditLocation.tsDecimal;
    EditLocation.edLongitudeChange(EditLocation.edLongitude);   // force deg/min/sec fields to update
    EditLocation.edLongitudeChange(EditLocation.edLatitude);    // force deg/min/sec fields to update

    EditLocation.ShowModal;
    if EditLocation.modalResult = mrOK then begin
      ini.WriteString(cbCity.Text, 'longitude', EditLocation.edLongitude.Text);
      lblLongitude.Caption :=  EditLocation.edLoD.Text + '� ' +
                               EditLocation.edLoM.Text + ''' ' +
                               EditLocation.edLoS.Text + '" ' +
                               EditLocation.cbEastWest.Text[1];
      ini.WriteString(cbCity.Text, 'latitude', EditLocation.edLatitude.Text);
      lblLatitude.Caption :=  EditLocation.edLaD.Text + '� ' +
                              EditLocation.edLaM.Text + ''' ' +
                              EditLocation.edLaS.Text + '" ' +
                              EditLocation.cbNorthSouth.Text[1];
      ini.WriteString (cbCity.Text, 'timezone', TimeZoneString(EditLocation.cbTimeZone.ItemIndex));
      lblTimezone.Caption := EditLocation.cbTimeZone.Text;
    end;
  finally
    ini.Free;
  end;
end;

procedure TForm3.SetUpComboBoxes;
var
  ini : TMemInifile;
  iniSections : TStringList;
  town, country: string;
begin
  cbCountry.Items.Clear;
  cbCity.Items.Clear;
  ini := TMeminifile.Create(LocationIniFile);
  iniSections := TStringList.Create;
  try
    ini.ReadSections(iniSections);
    for town in iniSections do begin
      Country := ini.ReadString(town, 'country', '');
      if cbCountry.Items.IndexOf(Country) < 0 then
        cbCountry.Items.Add(Country);
      if Country = LastCountry then
        cbCity.Items.Add(town)
    end;
  finally
    ini.Free;
    iniSections.Free;
  end;
  cbCountry.ItemIndex := cbCountry.Items.IndexOf(LastCountry);
  cbCity.ItemIndex := cbCity.Items.IndexOf(LastTown);
  if cbCountry.ItemIndex > 0 then
    cbCountry.Text := '';
  if cbCity.ItemIndex > 0 then
    cbCity.Text := '';
end;

procedure TForm3.FormCreate(Sender: TObject);
var
  fl : TextFile;
  ini : TMeminifile;
  path : string;
  msg : string;
begin
    ConfigIniFile := IncludeTrailingBackslash (ExtractFilePath (ParamStr(0))) + 'dhsoftware.ini';
    ini := TMemInifile.create (ConfigIniFile);
    try
      LocationIniFile := ini.readstring ('ShadowStats', 'GeoDataFile', '');
      lasttown := ini.readstring ('ShadowStats', 'Town', ')');
      lastcountry := ini.readstring ('ShadowStats', 'Country', '');

      if (Trim(LocationIniFile)='') or (not FileExists(LocationIniFile)) then begin

        msg := LocationIniFile;

        AssignFile (fl, ParamStr(1) + 'Sh.info');
        try
          Reset (fl);
          readln (fl, path);   // contains version number of dcx or dmx macro file

          readln (fl, path);
        finally
          closefile (fl);
        end;

        LocationIniFile := IncludeTrailingBackslash(path) + 'NX\Support Files\geogdata.ini';

        if (Trim(msg) <> Trim(LocationIniFile)) and (Trim(msg) <> '') then
          messagedlg ('Resetting Location File' + sLineBreak + sLineBreak +
                      'Existing file (' + msg +') no longer exists' + sLineBreak +
                      'Reverting to default file (' + LocationIniFile + ')', mtWarning, [mbOK], 0);

        ini.writestring ('ShadowStats', 'GeoDataFile', LocationIniFile);
      end;


    finally
      ini.free;
    end;

  SetupComboBoxes;

  cbCityChange (cbCity);

end;

end.
