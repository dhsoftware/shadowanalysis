unit views;

interface

  uses UInterfaces;

  procedure view_checkmode (projtype : integer);


implementation

  procedure view_checkmode (projtype : integer);
  begin
    if projtype <> view_currmode then
      view_setmode (projtype, true);
  end;

end.
