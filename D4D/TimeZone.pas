unit TimeZone;

interface

  uses System.SysUtils, Math, Geometry;

  function formatTimeZone (timezone : double) : string;
  function TimeZoneString (ndx : integer) : string;
  function TimezoneFloat (ndx : integer) : double;
  function TimzoneNdx (zone : double) : integer;

implementation

{$I ../inc/timezone.inc}

end.
