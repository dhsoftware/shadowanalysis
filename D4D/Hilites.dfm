object HiliteSettings: THiliteSettings
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = 'Hilite Settings'
  ClientHeight = 227
  ClientWidth = 331
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object btnSurface: TButton
    Left = 13
    Top = 19
    Width = 303
    Height = 25
    Caption = 'Hilite Colour for Shadow Surface:'
    TabOrder = 1
    OnClick = pnlSurfaceClick
  end
  object pnlSurface: TPanel
    Left = 15
    Top = 21
    Width = 299
    Height = 21
    Caption = 'Hilite Colour for Shadow Surface:'
    ParentBackground = False
    TabOrder = 5
    OnClick = pnlSurfaceClick
  end
  object btnGroup1: TButton
    Left = 13
    Top = 104
    Width = 303
    Height = 25
    Caption = 'Hilite Colour for Shadow Surface:'
    TabOrder = 3
    OnClick = pnlGroup1Click
  end
  object pnlGroup1: TPanel
    Left = 15
    Top = 106
    Width = 299
    Height = 21
    Caption = 'Hilite Colour for Shadow Casting Group 1:'
    ParentBackground = False
    TabOrder = 6
    OnClick = pnlGroup1Click
  end
  object btnGroup2: TButton
    Left = 13
    Top = 143
    Width = 303
    Height = 25
    Caption = 'Hilite Colour for Shadow Surface:'
    TabOrder = 4
    OnClick = pnlGroup2Click
  end
  object pnlGroup2: TPanel
    Left = 15
    Top = 145
    Width = 299
    Height = 21
    Caption = 'Hilite Colour for Shadow Casting Group 2:'
    ParentBackground = False
    TabOrder = 7
    OnClick = pnlGroup2Click
  end
  object btnOK: TButton
    Left = 241
    Top = 189
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 0
    OnClick = btnOKClick
  end
  object btnArrow: TButton
    Left = 13
    Top = 56
    Width = 303
    Height = 25
    Caption = 'Hilite Colour for Shadow Surface:'
    TabOrder = 2
    OnClick = pnlArrowClick
  end
  object pnlArrow: TPanel
    Left = 15
    Top = 58
    Width = 299
    Height = 21
    Caption = 'Arrow Colour for Shadow  Face:'
    ParentBackground = False
    TabOrder = 8
    OnClick = pnlArrowClick
  end
end
