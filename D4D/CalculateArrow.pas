unit CalculateArrow;

interface
  uses System.Generics.Collections, Math, URecords, UInterfaces, UConstants,
  CommonStuff, Geometry;

  procedure ShowArrow (surface : entity; const otherface : boolean;
                       var leader, head : entity;
                       arrowSz : double;
                       clr : integer);

implementation

  type
    TransformTp = (rotate, rotateRel, Move);
    TransformHistTp = record
      transform : TransformTp;
      case TransformTp of
        rotate,
        rotateRel : (ang : double;
                     axis : integer;
                     relpnt : point);
        move      : (vect : point);
      end;

  var
    HistItem : TransformHistTp;
    TransformHist : TList<TransformHistTp>;


  procedure ShowArrow (surface : entity; const otherface : boolean;
                       var leader, head : entity;
                       arrowSz : double;
                       clr : integer);
  // This procedure will calculate a transformation that can be applied to move the surface to be in the horizontal plane
  // at a z height of zero.  This simplifies subsequent calculations.

  VAR
    rotatedone : boolean;
    i			: integer;
    minp, maxp	: point;
    headz, headr : double;
    tanang, ang	: double;
    miny, maxy	: double;

  BEGIN
    // undraw any existing arrow
    if (leader.enttype = entln3) and (head.enttype = entply) then begin
      ent_draw (leader, drmode_black);
      ent_draw (head, drmode_black);
    end;

    // no further processing if surface is not a valid polygon entity
    if isnil (surface.addr) or (surface.enttype <> entply) then
      exit;

    rotatedone := false;
    // find the max & min x values of the points in the surface polygon - then use the corresponding points to create
    // a rotation transformation around the y axis
    ent_extent (surface, minp, maxp);

    TransformHist := TList<TransformHistTp>.Create;
    try
      // check that minp & maxp x & y values are different.  If not then surface is vertical and parallel
      // to either the y or x axis, so just a single rotation of -90 degrees around the y or x axis is required.
      if RealEqual (minp.x, maxp.x, NearZero) then begin
        HistItem.transform := rotate;
        HistItem.ang := -halfpi;
        HistItem.axis := y;
        RotatePoints (surface.plypnt, surface.plynpnt, -halfpi, y);
        RotateDone := true;
        TransformHist.Add(HistItem);
        minp := RotatePoint (minp, -halfpi, y);
        maxp := RotatePoint (maxp, -halfpi, y);
      end
      else if RealEqual (minp.y, maxp.y, NearZero) then begin
        HistItem.transform := rotate;
        HistItem.ang := -halfpi;
        HistItem.axis := x;
        RotatePoints (surface.plypnt, surface.plynpnt, -halfpi, x);
        RotateDone := true;
        TransformHist.Add(HistItem);
        minp := RotatePoint (minp, -halfpi, x);
        maxp := RotatePoint (maxp, -halfpi, x);
      end
      else if RealEqual (minp.z, maxp.z, NearZero) then begin
        RotateDone := true;
      end
      else begin
        // set minp & Maxp to the actual points with min & max x values.
        minp := surface.plyPnt[1];
        maxp := minp;
        FOR i := 2 to surface.plyNpnt DO BEGIN
          if surface.plyPnt[i].x < minp.x then
            minp := surface.plyPnt[i];
          if surface.plyPnt[i].x > maxp.x then
            maxp := surface.plyPnt[i];
        END;

        // rotate about the z axis so that the line between max & min is
        // parallel to the x axis
        tanang := (maxp.y - minp.y)/(maxp.x - minp.x);
        ang := arctan(tanang);
        HistItem.transform := rotateRel;
        HistItem.ang := -ang;
        HistItem.axis := z;
        HistItem.relpnt := minp;
        TransformHist.Add(HistItem);
        RotatePointsRel(surface.plypnt, surface.plynpnt, minp, -ang, z);
        maxp := RotatePointRel (maxp, minp, -ang, z);

        // find min and max y values of modified surface
        miny := surface.plypnt[1].y;
        maxy := surface.plypnt[1].y;

        FOR i:=2 to surface.plyNpnt DO BEGIN
          miny := min (miny, surface.plyPnt[i].y);
          maxy := max (maxy, surface.plyPnt[i].y);
        END;


        if RealEqual (miny, maxy, NearZero) then begin
          // it is vertical, and now parallel to the x axis, so just needs
          // to be rotated about the x axis;
          HistItem.transform := rotate;
          HistItem.ang := -halfpi;
          HistItem.axis := x;
          TransformHist.Add(HistItem);

          RotatePoints(surface.plypnt, surface.plynpnt, -halfpi, x);
          RotateDone := true;
        end
        else begin
          // not vertical, calculate rotation about the y axis
          tanang := (maxp.z - minp.z)/(maxp.x - minp.x);
          ang := arctan(tanang);
          HistItem.transform := rotateRel;
          HistItem.ang := ang;
          HistItem.relpnt := minp;
          HistItem.axis := y;
          TransformHist.Add(HistItem);

          RotatePointsRel(surface.plypnt, surface.plynpnt, minp, ang, y);
          RotatePointRel (maxp, minp, ang, y);
        end;
      end;

      // add a rotation around the x axis if required
      // find the max & min y values of the points in the mod_surf polygon - then use the corresponding points
      // to create a rotation transformation around the y axis
      IF not RotateDone THEN BEGIN
        minp := surface.plyPnt[1];
        maxp := minp;
        FOR i := 2 to surface.plyNpnt DO BEGIN
          if surface.plyPnt[i].y < minp.y then
            minp := surface.plyPnt[i];
          if surface.plyPnt[i].y > maxp.y then
            maxp := surface.plyPnt[i];
        END;
        tanang := (maxp.z - minp.z	)/(maxp.y - minp.y);
        ang := arctan(tanang);

        HistItem.transform := rotate;
        HistItem.ang := -ang;
        HistItem.axis := x;
        TransformHist.Add(HistItem);

        RotatePoints(surface.plypnt, surface.plynpnt, -ang, x);
      END;

      // polygon should now be horizontal ... move it to a z-height of zero          HistItem.transform := rotate;
      if abs(surface.plypnt[1].z) > NearZero then begin
        HistItem.transform := Move;
        HistItem.vect.x := 0;
        HistItem.vect.y := 0;
        HistItem.vect.z := -surface.plypnt[1].z;
        TransformHist.Add(HistItem);
      end;
      for i := 1 to surface.plynpnt do
        surface.plypnt[i].z := 0.0;


      // polygon is now horizontal at zero height ... create arrow
      ent_extent (surface, minp, maxp);
      ent_init (leader, entln3);
      meanpnt (minp, maxp, leader.ln3pt1);
      leader.ln3pt2 := leader.ln3pt1;
      if otherface then
        leader.ln3pt2.z := leader.ln3pt2.z + leader.ln3pt2.z - pixsize * 100.0*arrowSz
      else
        leader.ln3pt2.z := leader.ln3pt2.z + leader.ln3pt2.z + pixsize * 100.0*arrowSz;

      ent_init (head, entply);
      headz := 45*pixsize*arrowsz;
      if otherface then
        headz := - headz;
      headr := pixsize * 15.0*arrowSz;
      head.plypnt[1] := leader.ln3pt1;
      head.plypnt[2] := MovePoint (leader.ln3pt1, headr, 0, headz);
      head.plypnt[3] := MovePoint (leader.ln3pt1, 0, headr, headz);
      head.plypnt[4] := leader.ln3pt1;
      head.plypnt[5] := head.plypnt[3];
      head.plypnt[6] := MovePoint (leader.ln3pt1, -headr, 0, headz);
      head.plypnt[7] := leader.ln3pt1;
      head.plypnt[8] := head.plypnt[6];
      head.plypnt[9] := MovePoint (leader.ln3pt1, 0, -headr, headz);
      head.plypnt[10] := leader.ln3pt1;
      head.plypnt[11] := head.plypnt[9];
      head.plypnt[12] := head.plypnt[2];
      head.plynpnt := 12;

      for i := TransformHist.Count-1 downto 0 do
        case TransformHist[i].transform of
          rotate :    begin
                       leader.ln3pt1 := RotatePoint (leader.ln3pt1,
                                                     -TransformHist[i].ang,
                                                     TransformHist[i].axis);
                       leader.ln3pt2 := RotatePoint (leader.ln3pt2,
                                                     -TransformHist[i].ang,
                                                     TransformHist[i].axis);
                       RotatePoints (head.plypnt, head.plynpnt,
                                     -TransformHist[i].ang,
                                     TransformHist[i].axis);
                      end;
          rotateRel : begin
                       leader.ln3pt1 := RotatePointRel (leader.ln3pt1,
                                                        TransformHist[i].relpnt,
                                                        -TransformHist[i].ang,
                                                        TransformHist[i].axis);
                       leader.ln3pt2 := RotatePointRel (leader.ln3pt2,
                                                        TransformHist[i].relpnt,
                                                        -TransformHist[i].ang,
                                                        TransformHist[i].axis);
                       RotatePointsRel (head.plypnt, head.plynpnt,
                                        TransformHist[i].relpnt,
                                        -TransformHist[i].ang,
                                        TransformHist[i].axis);
                      end;
          move :      begin
                        leader.ln3pt1 := MovePoint(leader.ln3pt1,
                                                   -TransformHist[i].vect.x,
                                                   -TransformHist[i].vect.y,
                                                   -TransformHist[i].vect.z);
                        leader.ln3pt2 := MovePoint(leader.ln3pt2,
                                                   -TransformHist[i].vect.x,
                                                   -TransformHist[i].vect.y,
                                                   -TransformHist[i].vect.z);
                        MovePoints (head.plypnt, head.plynpnt,
                                    -TransformHist[i].vect.x,
                                    -TransformHist[i].vect.y,
                                    -TransformHist[i].vect.z);
                      end;
        end;

      leader.color := clr;
      ent_draw (leader, drmode_white);

      head.color := clr;
      ent_draw (head, drmode_white);


    finally
      TransformHist.free;
    end;
  END; // ShowArrow;

end.
