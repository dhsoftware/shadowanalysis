unit GetLocation;

{$WARN SYMBOL_PLATFORM Off}

interface

uses
  System.SysUtils, System.Variants, System.Classes, UInterfaces, UConstants,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, URecords,
  Vcl.ComCtrls, inifiles, Settings, Vcl.Menus, DoEditLocation, CommonStuff,
  Math, Geometry, TimeZone, System.UITypes;

type
  TLocationForm = class(TForm)
    Label6: TLabel;
    cbCountry: TComboBox;
    cbCity: TComboBox;
    lblCity: TLabel;
    lblLongitude: TLabel;
    Label7: TLabel;
    lblLatitude: TLabel;
    Label9: TLabel;
    lblTimezone: TLabel;
    Label11: TLabel;
    FileOpenDialog1: TFileOpenDialog;
    MainMenu1: TMainMenu;
    LocationOptions1: TMenuItem;
    AddLocation1: TMenuItem;
    EditCurrentLocation1: TMenuItem;
    ChangeLocationFIle1: TMenuItem;
    Button1: TButton;
    Button2: TButton;
    lblSavings: TLabel;
    lblDSStarts: TLabel;
    lblDSEnds: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure cbCountryChange(Sender: TObject);
    procedure cbCityChange(Sender: TObject);
    procedure EditCurrentLocation1Click(Sender: TObject);
    procedure AddNewLocation1Click(Sender: TObject);
    procedure ChangelocationFile1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
//    daylightSaving : boolean;
    { Private declarations }
//    Country, Town : string;
    procedure SetUpComboBoxes;
  public
    { Public declarations }
    longitude,
    latitude : double;
    timezone : integer;
    dsStartNdx, dsStopNdx : integer;
    daylightSaving : boolean;
  end;


  function formatLongitudeDec (lo : double) : string;
  function formatLatitudeDec (la : double) : string;

  var
    LocationForm: TLocationForm;

implementation

{$R *.dfm}

{ $I ../inc/timezone.inc}

  function formatLongitudeDec (lo : double) : string;
  begin
    result := FloatToStr(abs(lo)) +  '�';
    if lo < 0 then result := result + 'W'
    else result := result + 'E';
  end;

  function formatLatitudeDec (la : double) : string;
  begin
    result := FloatToStr(abs(la)) +  '�';
    if la < 0 then result := result + 'S'
    else result := result + 'N';
  end;


procedure TLocationForm.AddNewLocation1Click(Sender: TObject);
var
  ini : TIniFile;
begin
  EditLocation := TEditLocation.Create(Application);
  try
    EditLocation.cbCountry.Enabled := true;
    EditLocation.cbCountry.Items := cbCountry.Items;
    EditLocation.cbCountry.ItemIndex := cbCountry.ItemIndex;
    EditLocation.edCity.Enabled := true;
    EditLocation.edCity.Text := '';
    EditLocation.cbTimeZone.ItemIndex := -1;
    EditLocation.Caption := 'Add Location';
    EditLocation.ShowModal;
    if EditLocation.ModalResult = mrOK then begin
      ini := TiniFile.Create (l^.LocationIniFile);
      ini.WriteString(EditLocation.edCity.Text, 'country', EditLocation.cbCountry.Text);
      ini.WriteString(EditLocation.edCity.Text, 'longitude', EditLocation.edLongitude.Text);
      ini.WriteString(EditLocation.edCity.Text, 'latitude', EditLocation.edLatitude.Text);
      ini.WriteString (EditLocation.edCity.Text, 'timezone', TimeZoneString(EditLocation.cbTimeZone.ItemIndex));
      ini.WriteBool(EditLocation.edCity.Text, 'DaylightSaving', EditLocation.chbDS.checked);
      ini.WriteInteger(EditLocation.edCity.Text, 'SavingStartNdx', EditLocation.cbDSStarts.ItemIndex);
      ini.WriteInteger(EditLocation.edCity.Text, 'SavingStopNdx', EditLocation.cbDSEnds.ItemIndex);
      if cbCountry.Items.IndexOf(EditLocation.cbCountry.Text) < 0 then
        cbCountry.Items.Add(EditLocation.cbCountry.Text);
      cbCountry.ItemIndex := cbCountry.Items.IndexOf(EditLocation.cbCountry.Text);
      cbCountryChange (Sender);
      cbCity.ItemIndex := cbCity.Items.IndexOf(EditLocation.edCity.Text);
      cbCityChange(Sender);
    end;
  finally
    EditLocation.Free;
  end;
end;

procedure TLocationForm.EditCurrentLocation1Click(Sender: TObject);
var
  ini : TIniFile;
  s : string;
  d : double;
begin
  EditLocation := TEditLocation.Create(nil);
  try
    SetFormPos(TForm(EditLocation));
    EditLocation.edCity.Text := cbCity.Text;
    EditLocation.edCity.Enabled := false;
    EditLocation.cbCountry.Text := cbCountry.Text;
    EditLocation.cbCountry.Enabled := false;
    EditLocation.Caption := 'Edit Location';
    ini := TIniFile.Create(l^.LocationIniFIle);
    try
      s := ini.ReadString(cbCity.Text, 'longitude', '');
      EditLocation.edLongitude.Text := s;
      s := ini.ReadString(cbCity.Text, 'latitude', '');
      EditLocation.edLatitude.Text := s;
      d := ini.ReadFloat(cbCity.Text, 'timezone', 0);
      EditLocation.cbTimeZone.ItemIndex :=
                      EditLocation.cbTimeZone.items.IndexOf (formatTimeZone (d));
      EditLocation.PageControl1.ActivePage := EditLocation.tsDecimal;
      EditLocation.edLongitudeChange(EditLocation.edLongitude);   // force deg/min/sec fields to update
      EditLocation.edLongitudeChange(EditLocation.edLatitude);    // force deg/min/sec fields to update
      if l^.daylightsaving then begin
        EditLocation.chbDS.Checked := true;
        EditLocation.cbDSStarts.ItemIndex := l^.DS_start;
        EditLocation.cbDSEnds.ItemIndex := l^.DS_end;
      end;


      EditLocation.ShowModal;
      SaveFormPos(TForm(EditLocation));
      if EditLocation.modalResult = mrOK then begin
        TryStrToFloat(EditLocation.edLongitude.Text, longitude);
        TryStrToFloat(EditLocation.edLatitude.Text, latitude);
        ini.WriteString(cbCity.Text, 'longitude', EditLocation.edLongitude.Text);
        lblLongitude.Caption :=  EditLocation.edLoD.Text + '� ' +
                                 EditLocation.edLoM.Text + ''' ' +
                                 EditLocation.edLoS.Text + '" ' +
                                 EditLocation.cbEastWest.Text[1];
        ini.WriteString(cbCity.Text, 'latitude', EditLocation.edLatitude.Text);
        lblLatitude.Caption :=  EditLocation.edLaD.Text + '� ' +
                                EditLocation.edLaM.Text + ''' ' +
                                EditLocation.edLaS.Text + '" ' +
                                EditLocation.cbNorthSouth.Text[1];
        ini.WriteString (cbCity.Text, 'timezone', TimeZoneString(EditLocation.cbTimeZone.ItemIndex));
        ini.WriteBool(cbCity.Text, 'DaylightSaving', EditLocation.chbDS.checked);
        ini.WriteInteger(cbCity.Text, 'SavingStartNdx', EditLocation.cbDSStarts.ItemIndex);
        ini.WriteInteger(cbCity.Text, 'SavingStopNdx', EditLocation.cbDSEnds.ItemIndex);
        lblTimezone.Caption := EditLocation.cbTimeZone.Text;
        if EditLocation.chbDS.checked then begin
          daylightSaving := true;
          dsStartNdx := EditLocation.cbDSStarts.ItemIndex;
          dsStopNdx := EditLocation.cbDSEnds.ItemIndex;
        end
        else begin
          daylightSaving := false;
          dsStartNdx := -1;
          dsStopNdx := -1;
        end;
        l^.daylightsaving := daylightSaving;
        l^.DS_start := dsStartNdx;
        l^.DS_end := dsStopNdx;
        SaveInt ('ShadStatDS', ord (daylightsaving), true);
        SaveInt ('ShadStatDSSt', dsStartNdx, true);
        SaveInt ('ShadStatDSEn', dsStopNdx, true);
      end;
    finally
      ini.Free;
    end;
  finally
    EditLocation.Free;
  end;

  cbCityChange (cbCity);
end;



procedure TLocationForm.Button2Click(Sender: TObject);
begin
  if BlankString (cbCity.Text) or BlankString (cbCountry.Text) then
    messagedlg ('Country and City must be entered', mtError, [mbOK], 0)
  else
    l^.daylightsaving := daylightsaving;
    ModalResult := mrOK;
end;

procedure TLocationForm.cbCityChange(Sender: TObject);
var
  ini : Tinifile;

  function formatAngle (ang : double) : string;
  var
    d,m,s : integer;
    r : double;
  begin
    d := Trunc(ang);
    r := (ang - d)*60;
    m := trunc(r);
    r := (r-m)*60;
    s := round (r);
    result := IntToStr(d) + '� ' + IntToStr(m) + ''' ' + IntToStr(s) + '" ';
  end;

  function formatLongitude : string;
  begin
    result := formatAngle (abs(Longitude));
    if longitude < 0 then result := result + 'W'
    else if longitude > 0 then result := result + 'E';
  end;

  function formatLatitude : string;
  begin
    result := formatAngle (abs(Latitude));
    if latitude < 0 then result := result + 'S'
    else if latitude > 0 then result := result + 'N';
  end;




begin   // cbCityChange
  ini := Tinifile.Create(l^.LocationIniFIle);
  try
    longitude := ini.ReadFloat(cbCity.Text, 'longitude', 200);
    latitude := ini.ReadFloat(cbCity.Text, 'latitude', 100);
    timezone := TimzoneNdx(ini.ReadFloat(cbCity.Text, 'timezone', 100));
    daylightSaving := ini.ReadBool(cbCity.Text, 'DaylightSaving', false);
    dsStartNdx := ini.ReadInteger(cbCity.Text, 'SavingStartNdx', -1);
    dsStopNdx := ini.ReadInteger(cbCity.Text, 'SavingStopNdx', -1);
  finally
    ini.Free;
  end;
  if (latitude > 90) or (longitude > 180) or (timezone > 99) then begin
     lblLongitude.Caption := '';
    lblLatitude.Caption := '';
    lblTimezone.Caption := '';
    lblSavings.Caption := '';
    lblDSStarts.Caption := '';
    lblDSEnds.Caption := '';
  end
  else begin
    lblLongitude.Caption := formatLongitude;
    lblLatitude.Caption := formatLatitude;
    lblTimezone.Caption :=  formatTimeZone(TimeZoneFloat(timezone)); //  formatTimeZone(timezone);
    if daylightSaving and (dsStartNdx >= 0) and (dsStopNdx >= 0) and
       (dsStartNdx < Length(DSStartValues)) and
       (dsStopNdx < Length(DSEndValues))
    then begin
      lblSavings.Caption := 'Daylight Saving:';
      lblDSStarts.Caption := 'Starts ' + DSStartValues[dsStartNdx];
      lblDSEnds.Caption := 'Ends ' + DSEndValues[dsStopNdx];
    end
    else begin
      lblSavings.Caption := '';
      lblDSStarts.Caption := '';
      lblDSEnds.Caption := '';
    end;
  end;
end;

procedure TLocationForm.cbCountryChange(Sender: TObject);
var
  ini : TMeminifile;
  iniSections : TStringList;
  Country, Town : string;
begin
  ini := TMeminifile.Create(l^.LocationIniFIle);
  iniSections := TStringList.Create;
  try
    ini.ReadSections(iniSections);
    cbCity.Items.Clear;
    for town in iniSections do begin
      Country := ini.ReadString(town, 'country', '');
      if Country = cbCountry.Text then
        cbCity.Items.Add(town);
    end;
  finally
    ini.Free;
    iniSections.Free;
  end;
  cbCity.ItemIndex := 0;
  cbCityChange (Sender);
end;


procedure TLocationForm.ChangelocationFile1Click(Sender: TObject);
begin
  FileOpenDialog1.FileName := ExtractFileName (l^.LocationIniFIle);
  FileOpenDialog1.DefaultFolder := ExtractFilePath(l^.LocationIniFIle);
  if FileOpenDialog1.Execute then begin
    l^.LocationIniFIle := FileOpenDialog1.FileName;
    SaveStr(GeoDataFile, l^.LocationIniFile);

    SetupComboBoxes;
  end;
end;


procedure TLocationForm.SetUpComboBoxes;
var
  ini : TMemInifile;
  iniSections : TStringList;
  ThisTown, ThisCountry: string;
begin
  cbCountry.Items.Clear;
  cbCity.Items.Clear;
  ini := TMeminifile.Create(l^.LocationIniFIle);
  iniSections := TStringList.Create;
  try
    ini.ReadSections(iniSections);
    for ThisTown in iniSections do begin
      ThisCountry := ini.ReadString(ThisTown, 'country', '');
      if cbCountry.Items.IndexOf(ThisCountry) < 0 then
        cbCountry.Items.Add(ThisCountry);
      if ThisCountry = l^.Country then
        cbCity.Items.Add(ThisTown);
    end;
  finally
    ini.Free;
    iniSections.Free;
  end;
  cbCountry.ItemIndex := cbCountry.Items.IndexOf(l^.Country);
  cbCity.ItemIndex := cbCity.Items.IndexOf(l^.City);
  if cbCountry.ItemIndex < 0 then
    cbCountry.Text := '';
  if cbCity.ItemIndex < 0 then
    cbCity.Text := '';
end;

procedure TLocationForm.FormCreate(Sender: TObject);
begin

  SetupComboBoxes;

  if cbCountry.Items.Count = 0 then begin
    AddNewLocation1Click (self);
    if cbCountry.Items.Count > 0 then begin
      cbCountry.ItemIndex := 0;
      l^.Country := cbCountry.Text;
      if cbCity.Items.Count > 0 then begin
        cbCity.ItemIndex := 0;
        l^.City :=  cbCity.Text;
      end;

    end;
  end;

  cbCityChange (cbCity);

end;

end.
