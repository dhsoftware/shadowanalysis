unit AtrUtils;

interface

  uses UInterfaces, UConstants, URecords;

  /// <summary> Adds (or updates) and atr_addr attribute to the specified entity with lgladdr set to the entity's own address
  /// </summary>
  /// <param name="ent">The entity the attribute is added to (or updated)
  /// </param>
  /// <param name="AtribName">the name of the attribute to add (or update)
  /// </param>
  /// <remarks> Note that this procedure does NOT remove the same attribute name from any other
  ///  entities that already have it ... use Ent_RemoveAtr to do this if necessary.
  /// </remarks>
  procedure Ent_AddAddrAtr (var ent : entity; AtribName : atrname);


  /// <summary> Deletes the specified attribute from all matching entities
  /// </summary>
  /// <param name="AtribName">The attribute name to delete
  /// </param>
  /// <param name="entTypes">Array of integer: Specify the type(s) of entities to delete the attribute from (an empty array will delete from all entity types)
  /// </param>
  /// <param name="lyrSearch">Should be one of lyr_curr, lyr_on or lyr_all. The attribute will be deleted from entities on the specified layer(s) only.
  /// </param>
  procedure Ent_RemoveAtr (AtribName : atrname; entTypes : array of integer;
                           lyrSearch : integer);


  /// <summary> Finds the first entity with an atr_addr attribute of the specified name
  /// </summary>
  /// <param name="ent">The entity that is returned (will have a nil address if none found)
  /// </param>
  /// <param name="entTypes">Array of integer: Specify the type(s) of entities to search for (an empty array will return all entity types)
  /// </param>
  /// <param name="AtribName">The attribute name to search for
  /// </param>
  /// <param name="lyrSearch">Should be one of lyr_curr, lyr_on or lyr_all
  /// </param>
  /// <returns><c>True</c> if a suitable entity is found, <c>False</c> if none found.
  /// </returns>
  function Ent_GetByAddrAtr (var ent : entity; entTypes : array of integer;
                             AtribName : atrname; lyrSearch : integer) : boolean;

  /// <summary> Removes the specified attribute from all layers in the drawing
  /// </summary>
  /// <param name="AtribName">The name of the attribute to delete
  /// </param>
  procedure Lyr_RemoveAtr (AtribName : atrname);


  /// <summary> Sets the flag in the names system attribute for the specified selection set
  /// </summary>
  /// <param name="SetNum">The selection set number (1-8)
  /// </param>
  /// <param name="SetOn">indicates whether the selection set is selected (if true) or not.
  /// </param>
  /// <param name="AtribName">The name of the system attribute
  /// </param>
  /// <remarks> Flags for all 8 selection sets are stored in a single string attibute.
  /// The appropriate character in the string (e.g. first character for selset 1) is set
  /// to a dash (-) if the selection set is not selected, or to the selection set number
  /// if the set is selected (e.g. '-2--5---' would indicate that sets 2 & 5 are selected).
  /// </remarks>
  procedure SelSet_SetAtr (SetNum : integer; SetOn : boolean; AtribName : atrname);



  /// <summary> Sets the flag in the names system attribute to false for all selection sets
  /// </summary>
  /// <param name="AtribName">The name of the system attribute
  /// </param>
  procedure SelSets_RemoveAtr (AtribName : atrname);



implementation
  procedure Ent_AddAddrAtr (var ent : entity; AtribName : atrname);
  var
    atr : attrib;
  begin
    if atr_entfind (ent, AtribName, atr) then begin
      atr.atrtype := atr_addr;
      atr.lgladdr := ent.addr;
      atr_update (atr);
    end
    else begin
      atr_init (atr, atr_addr);
      atr.lgladdr := ent.addr;
      atr.name := AtribName;
      atr_add2ent (ent, atr);
    end;
  end;


  function Ent_GetByAddrAtr (var ent : entity; entTypes : array of integer;
                             AtribName : atrname; lyrSearch : integer) : boolean;
  var
    atr  : attrib;
    mode : mode_type;
    i    : integer;
    addr : entaddr;
  begin
    result := false;
    mode_init (mode);
    mode_lyr (mode, lyrSearch);
    if length (entTypes) > 0 then for i := 0 to length(entTypes)-1 do
      mode_enttype (mode, entTypes[i]);
    mode_atr (mode, AtribName);
    addr := ent_first (mode);
    while ent_get (ent, addr) and not result do begin
      if atr_entfind (ent, AtribName, atr) and
         (atr.atrtype = atr_addr) then
        result := true
      else
        addr := ent_next (ent, mode);
    end;
    if not result then
      setnil (ent.addr);
  end;

  {$Hints off}  // Supress hint about filler not being used.
  procedure Ent_RemoveAtr (AtribName : atrname; entTypes : array of integer;
                           lyrSearch : integer);
  var
    filler : byte;  // Without this filler the AtribName parameter gets corrupted by mode_init call.
                    // I expect that it has something to do with mode_type having an odd number of bytes and
                    // mode_init operating on a word-aligned area of memory, but Mark/Dave have been unable
                    // to reproduce the problem even though similar symptoms have happened for me predictably
                    // in multiple projects
    mode : mode_type;
    addr : entaddr;
    ent  : entity;
    atr  : attrib;
    i    : integer;
  begin
    mode_init (mode);
    mode_lyr (mode, lyrSearch);
    if length (entTypes) > 0 then for i := 0 to length(entTypes)-1 do
      mode_enttype (mode, entTypes[i]);
    mode_atr (mode, AtribName);
    addr := ent_first (mode);
    while ent_get (ent, addr)  do begin
      if atr_entfind (ent, AtribName, atr) then
        atr_delent (ent, atr);
      addr := ent_next (ent, mode);
    end;
  end;
  {$Hints On}

  procedure Lyr_RemoveAtr (AtribName : atrname);
  var
    lyr : lyraddr;
    atr : attrib;
  begin
    lyr := lyr_first;
    while not lyr_nil (lyr) do begin
      if atr_lyrfind (lyr, AtribName, atr) then
        atr_dellyr (lyr, atr);
      lyr := lyr_next (lyr);
    end;
  end;

  procedure SelSet_SetAtr (SetNum : integer; SetOn : boolean; AtribName : atrname);
  var
    atr : attrib;
  begin
    if atr_sysfind (AtribName, atr) then begin
      if atr.atrtype <> atr_str then begin
        atr.atrtype := atr_str;
        atr.Str := '--------';
      end;
      if SetOn then
        atr.str[SetNum] := ansichar (ord('0') + SetNum)
      else
        atr.str[SetNum] := '-';
      atr_update (atr);
    end
    else begin
      atr_init (atr, atr_str);
      atr.name := AtribName;
      atr.Str := '--------';
      if SetOn then
        atr.str[SetNum] := ansichar (ord('0') + SetNum);
      atr_add2sys (atr);
    end;
  end;

  procedure SelSets_RemoveAtr (AtribName : atrname);
  var
    atr : attrib;
  begin
    if atr_sysfind (AtribName, atr) then begin
      atr.atrtype := atr_str;
      atr.Str := '--------';
      atr_update (atr);
    end;
  end;

end.
