unit Hilites;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, UInterfaces, URecords, UConstants,
  CommonStuff, Settings;

type
  THiliteSettings = class(TForm)
    btnSurface: TButton;
    pnlSurface: TPanel;
    btnGroup1: TButton;
    pnlGroup1: TPanel;
    btnGroup2: TButton;
    pnlGroup2: TPanel;
    btnOK: TButton;
    btnArrow: TButton;
    pnlArrow: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure pnlSurfaceClick(Sender: TObject);
    procedure pnlGroup1Click(Sender: TObject);
    procedure pnlGroup2Click(Sender: TObject);
    procedure pnlArrowClick(Sender: TObject);
  private
    { Private declarations }
    procedure setColours;
  public
    { Public declarations }
  end;

  /// <summary> Hilites (or unhilites) and entity
  /// </summary>
  /// <param name="ent">The entity to hilite (or unhilite)
  /// </param>
  /// <param name="clr">The colour to hilite the entity with. Zero indicates to UNhilite.
  /// </param>
  /// <remarks> Colour of zero causes the entity to be UNhilited
  /// </remarks>
  procedure HiliteEnt (var ent : entity; clr : integer);

  /// <summary> Hilites (or unhilites) all entities in a selection set
  /// </summary>
  /// <param name="SetNum">The number of the selection set to hilite (or unhilite)
  /// </param>
  /// <param name="clr">The colour to hilite with. Zero indicates to UNhilite.
  /// </param>
  /// <returns> True if any entities were hilited, otherwise (if set is empty) returns false </returns>
  /// <remarks> Clr of zero causes the entity to be UNhilited
  /// </remarks>
  function HiliteSelSet (SetNum : integer; clr : integer) : boolean;


  /// <summary> Hilites (or unhilites) entities that have the specified attribute,
  ///           are on a layer that has the attribute, or are in a selection set
  ///           flagged by a system attribute with the specified name
  /// </summary>
  /// <param name="AtribName">The attribute to look for
  /// </param>
  /// <param name="Colour">The colour to hilite the entities with. Zero indicates to unhilite.
  /// </param>
  /// <returns> Returns true if any entities were hilited, otherwise false </returns>
  /// <remarks> Colour of zero causes the entities to be UNhilited
  /// </remarks>
  function HiliteByAtr (AtribName : atrname; Colour : integer) : boolean;


var
  HiliteSettings: THiliteSettings;

implementation

{$R *.dfm}

  procedure HiliteEnt (var ent : entity; clr : integer);
  var
    b : boolean;
    svlintp : integer;
    svspacing : double;
  begin
    svlintp := ent.ltype;
    svspacing := ent.spacing;
    if clr <> 0 then begin
      ent_draw (ent, drmode_black);
      ent.color := clr;
    end;
    ent.ltype := ltype_dashed;
    ent.spacing := pixsize * 25.0;
    ent.Width := ent.Width+1;
    b := pgSaveVar^.showwgt;
    pgSaveVar^.showwgt := true;
    if clr = 0 then begin
      ent_draw_dl(ent, drmode_black, true);
      ent.ltype := svlintp;
      ent.spacing := svspacing;
      ent.Width := ent.Width-1;
      ent_draw_dl (ent, drmode_white, true);
    end
    else
      ent_draw_dl (ent, drmode_white, true);
    pgSaveVar^.showwgt := b;
  end;

  function HiliteSelSet (SetNum : integer; clr : integer) : boolean;
  var
    mode : mode_type;
    addr : entaddr;
    ent  : entity;
  begin
    result := false;
    mode_init (mode);
    mode_lyr (mode, lyr_all);
    mode_ss (mode, SetNum-1);
    addr := ent_first (mode);
    while ent_get (ent, addr) do begin
      HiliteEnt(ent, clr);
      result := true;
      addr := ent_next (ent, mode);
    end;
  end;

  {$Hints off}     // Supress hint about filler not being used.
  function HiliteByAtr (AtribName : atrname; colour : integer) : boolean;
  /// colour of zero will UNhilite, result inicates if any entities were hilited.
  var
    filler : byte;  // Without this filler the AtribName parameter gets corrupted by mode_init call.
                    // I expect that it has something to do with mode_type having an odd number of bytes and
                    // mode_init operating on a word-aligned area of memory, but Mark/Dave have been unable
                    // to reproduce the problem even though similar symptoms have happened for me predictably
                    // in multiple projects

    mode : mode_type;
    atr : attrib;
    ent : entity;
    addr : entaddr;
    lyr : lyraddr;
    rlyr : Rlayer;
    i   : integer;
  begin
    result := false;
    lyr := lyr_first;
    while lyr_get (lyr, rlyr) do begin
      if atr_lyrfind (lyr, AtribName, atr) then begin
        mode_init (mode);
        mode_1lyr (mode, lyr);
        addr := ent_first (mode);
        while ent_get (ent, addr) do begin
          addr := ent_next (ent, mode);
          HiliteEnt (ent, colour);
          result := true;
        end;
      end;
      lyr := rlyr.nxt;
    end;

    mode_init (mode);
    mode_lyr (mode, lyr_all);
    mode_atr (mode, AtribName);
    addr := ent_first (mode);
    while ent_get (ent, addr) do begin
      addr := ent_next (ent, mode);
      HiliteEnt (ent, colour);
      result := true;
    end;

    if atr_sysfind (AtribName, atr) and (atr.atrtype = atr_str) then
    for i := 1 to 8 do begin
      if (length (atr.str) >= i) and (atr.str[i] = ansichar(ord('0') + i)) then begin
        mode_init (mode);
        mode_ss (mode, i-1);
        addr := ent_first (mode);
        while ent_get (ent, addr) do begin
          addr := ent_next (ent, mode);
          HiliteEnt (ent, colour);
          result := true;
          if colour = 0 then
            ent_draw_dl (ent, drmode_white, true);
        end;
      end;
    end;

  end;
  {$Hints On}



  function PnlFontClr (bgClr : TColor) : TColor;
  // returns white for dark backgroundes, otherwise retunns black
  var
    brightness : double;
  begin
    brightness := sqrt(0.241*sqr(GetRValue(bgClr)) +
                       0.691*sqr(GetGValue(bgClr)) +
                       0.068*sqr(GetBValue(bgClr)));
    if brightness > 128 then
      result := clBlack
    else
      result := clWhite;
  end;

PROCEDURE ClrPnlCaption (var pnl : TPanel; clr : longint);
VAR
  s : shortstring;
  caption : string;
  colonpos : integer;
BEGIN
  clrGetName (clr, s);
  pnl.Color := DCADRGBtoRGB (clr);
  caption := pnl.caption;
  colonpos := pos (':', caption);
  caption := copy (caption, 1, colonpos) + ' ' + string(s);
  pnl.Caption := caption;
  pnl.Font.Color := PnlFontClr (pnl.Color);
END;

procedure THiliteSettings.setColours;
begin
  ClrPnlCaption (pnlSurface, l^.HiClrs[0]);
  ClrPnlCaption (pnlArrow, l^.HiClrs[3]);
  ClrPnlCaption (pnlGroup1, l^.HiClrs[1]);
  ClrPnlCaption (pnlGroup2, l^.HiClrs[2]);
end;

procedure THiliteSettings.btnOKClick(Sender: TObject);
begin
  SaveFormPos (TForm(HiliteSettings));

  SaveInt ('ShadStatHiSf', l^.HiClrs[0]);
  SaveInt ('ShadStatHiAr', l^.HiClrs[3]);
  SaveInt ('ShadStatHiG1', l^.HiClrs[1]);
  SaveInt ('ShadStatHiG2', l^.HiClrs[2]);

  HiliteEnt (l^.surface, l^.HiClrs[0]);
  l^.GroupSelected[1] := HiliteByAtr ('ShadStatGrp1', l^.HiClrs[1]);
  l^.GroupSelected[2] := HiliteByAtr ('ShadStatGrp2', l^.HiClrs[2]);

  ModalResult := mrOK;
end;

procedure THiliteSettings.FormCreate(Sender: TObject);
begin
//  SetFormPos (TForm(Sender));
  setColours;
end;
procedure THiliteSettings.pnlArrowClick(Sender: TObject);
begin
  GetColorIndex (l^.HiClrs[3]);
  setColours;
end;

procedure THiliteSettings.pnlGroup1Click(Sender: TObject);
begin
  GetColorIndex (l^.HiClrs[1]);
  setColours;
end;

procedure THiliteSettings.pnlGroup2Click(Sender: TObject);
begin
  GetColorIndex (l^.HiClrs[2]);
  setColours;
end;

procedure THiliteSettings.pnlSurfaceClick(Sender: TObject);
begin
  GetColorIndex (l^.HiClrs[0]);
  setColours;
end;


end.
