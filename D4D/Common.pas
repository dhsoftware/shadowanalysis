unit Common;

interface

  uses URecords, UInterfacesRecords;

  type
    ShadStatL = record
      state   : integer;
      surface : entity;
      c1Set   : integer;
      c2Set   : integer;
      c1Lyr   : lyrAddr;
      c2Lyr   : lyrAddr;
      case byte of
         0: (getp: getpointarg);
         1: (gete: getEscArg);
         2: (getl: TFLyrArg);
    end;
    pShadStatL = ^ShadStatL;

 const
   MacroVersion = '0.0.0.1';

  var
    l       : pShadStatL;


implementation

end.
