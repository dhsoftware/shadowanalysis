
function formatTimeZone (timezone : double) : string;
var
  h,m : integer;
begin
  h := trunc (abs(Timezone));
  m := round ((abs(Timezone)-h)*60);
  result := 'UTC';
  if timezone < 0 then
    result := result + '-'
  else
    result := result + '+';
  result := result + inttostr(h);
  if m > 0 then begin
    result := result  + ':';
    if m<10 then
      result := result + '0';
    result := result + inttostr(m);
  end;
end;


function TimeZoneString (ndx : integer) : string;
begin
  case ndx of
    0:  result :=  '-12';
    1:  result :=  '-11';
    2:  result :=  '-10';
    3:  result :=  '-9.5';
    4:  result :=  '-9';
    5:  result :=  '-8';
    6:  result :=  '-7';
    7:  result :=  '-6';
    8:  result :=  '-5';
    9:  result :=  '-4.5';
    10: result :=  '-4';
    11: result :=  '-3.5';
    12: result :=  '-3';
    13: result :=  '-2';
    14: result :=  '-1';
    15: result :=  '0';
    16: result :=  '1';
    17: result :=  '2';
    18: result :=  '3';
    19: result :=  '3.5';
    20: result :=  '4';
    21: result :=  '4.5';
    22: result :=  '5';
    23: result :=  '5.5';
    24: result :=  '5.75';
    25: result :=  '6';
    26: result :=  '6.5';
    27: result :=  '7';
    28: result :=  '8';
    29: result :=  '8.75';
    30: result :=  '9';
    31: result :=  '9.5';
    32: result :=  '10';
    33: result :=  '10.5';
    34: result :=  '11';
    35: result :=  '11.5';
    36: result :=  '12';
    37: result :=  '12.75';
    38: result :=  '13';
    39: result :=  '14';
    else result :=  '';
  end;
end;

function TimezoneFloat (ndx : integer) : double;
begin
  case ndx of
    0:  result :=  -12;
    1:  result :=  -11;
    2:  result :=  -10;
    3:  result :=  -9.5;
    4:  result :=  -9;
    5:  result :=  -8;
    6:  result :=  -7;
    7:  result :=  -6;
    8:  result :=  -5;
    9:  result :=  -4.5;
    10: result :=  -4;
    11: result :=  -3.5;
    12: result :=  -3;
    13: result :=  -2;
    14: result :=  -1;
    15: result :=  0;
    16: result :=  1;
    17: result :=  2;
    18: result :=  3;
    19: result :=  3.5;
    20: result :=  4;
    21: result :=  4.5;
    22: result :=  5;
    23: result :=  5.5;
    24: result :=  5.75;
    25: result :=  6;
    26: result :=  6.5;
    27: result :=  7;
    28: result :=  8;
    29: result :=  8.75;
    30: result :=  9;
    31: result :=  9.5;
    32: result :=  10;
    33: result :=  10.5;
    34: result :=  11;
    35: result :=  11.5;
    36: result :=  12;
    37: result :=  12.75;
    38: result :=  13;
    39: result :=  14;
    else result := NAN;
  end;
end;

function TimzoneNdx (zone : double) : integer;
var
  flresult : double;
  minNdx, maxNdx : integer;
begin
  minNdx := 0;
  maxNdx := 39;
  repeat
    result := (minNdx + maxNdx) div 2;
    flresult := TimezoneFloat (result);
    if realequal (flresult, zone, 0.001) then
      exit
    else if flresult < zone then
      minNdx := result+1
    else
      maxNdx := result-1;
  until (minNdx > maxNdx);
  result := -1;
end;