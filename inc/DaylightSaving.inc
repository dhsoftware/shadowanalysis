    DSStartValues : array[0..8] of string =  ('Second Sunday in March',
                                              'Friday before last Sunday in March',
                                              'Last Friday in March',
                                              'Last Sunday in March',
                                              'First Sunday in April',
                                              'First Sunday in September',
                                              'Last Sunday in September',
                                              'First Sunday in October',
                                              'First Sunday in November');

    DSEndValues : array[0..5] of string =  ('Third Sunday in January',
                                            'Fourth Sunday in March',
                                            'First Sunday in April',
                                            'Last Friday  in October',
                                            'Last Sunday in October',
                                            'First Sunday in November');
