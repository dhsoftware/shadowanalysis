  Function RotatePoint (const Pnt : point;
                        const Ang : double;
                        const Axis: integer) : point;

  Function RotatePointRel (const Pnt : point;
                           const RelPnt : point;
                           const Ang : double;
                           const Axis: integer) : point;

  Function MovePoint (const Pnt : point;
                      const dx, dy, dz : double) : point;  overload;

  Function MovePoint (const Pnt : point;
                      const Vct : point) : point;  overload;

  Function InvertVct (const Vct : point) : point;


  function fixZero (num : double) : double;

  procedure RotatePoints (var points : array of point;
                          const npnt : integer;
                          const Ang : double;
                          const Axis: integer);

  procedure RotatePointsRel (var points : array of point;
                             const npnt : integer;
                             const RelPnt : point;
                             const Ang : double;
                             const Axis: integer);

  Function MovePoints (var points : array of point;
                       const npnt : integer;
                       const Vct : point) : point;  overload;

  Function MovePoints (var points : array of point;
                       const npnt : integer;
                       const dx, dy, dz : double) : point;  overload;

  Function RealEqual (d1, d2 : double) : boolean;  overload;
  Function RealEqual (d1, d2, delta : double) : boolean;   overload;
  Function PointsEqual (p1, p2 : point) : boolean;   overload;
  Function PointsEqual (p1, p2 : point; delta : double) : boolean;  overload;


implementation

  Function RealEqual (d1, d2 : double) : boolean;
  begin
    result := (abs(d1-d2) <= NearZero);
  end;

  Function RealEqual (d1, d2, delta : double) : boolean;
  begin
    result := (abs(d1-d2) <= abs(delta));
  end;

  Function PointsEqual (p1, p2 : point) : boolean;
  begin
    result := RealEqual (p1.x, p2.x) and RealEqual (p1.y, p2.y);
  end;

  Function PointsEqual (p1, p2 : point; delta : double) : boolean;
  begin
    result := RealEqual (p1.x, p2.x, delta) and RealEqual (p1.y, p2.y, delta);
  end;




  Function RotatePoint (const Pnt : point;
                        const Ang : double;
                        const Axis: integer) : point;
  var
    a, r : double;
  begin
    case axis of
      z : begin
            if RealEqual(pnt.y, 0.0) and RealEqual(pnt.x, 0.0) then begin
              result := Pnt;
              exit;
            end;
            a := ArcTan2 (Pnt.y, Pnt.x) + Ang;
            if a > pi then
              a := a - TwoPi
            else if a < -pi then
              a := a + TwoPi;
            r:= sqrt (sqr(Pnt.x) + sqr(Pnt.y));
            result.x := fixZero(r*Cos(a));
            result.y := fixZero(r*Sin(a));
            result.z := Pnt.z;
          end;
      y : begin
            if RealEqual(pnt.z, 0.0) and RealEqual(pnt.x, 0.0) then begin
              result := Pnt;
              exit;
            end;
            a := ArcTan2 (Pnt.x, Pnt.z) + Ang;    // subract Ang since dcad angles are anticlockwise
            if a > pi then
              a := a - TwoPi
            else if z < -pi then
              a := a + TwoPi;
            r:= sqrt (sqr(Pnt.x) + sqr(Pnt.z));
            result.z := fixZero(r*Cos(a));
            result.x := fixZero(r*Sin(a));
            result.y := Pnt.y;
          end;
      x : begin
            if RealEqual(pnt.y, 0.0) and RealEqual(pnt.z, 0.0) then begin
              result := Pnt;
              exit;
            end;
            a := ArcTan2 (Pnt.z, Pnt.y) + Ang;    // subract Ang since dcad angles are anticlockwise
            if a > pi then
              a := a - TwoPi
            else if z < -pi then
              a := a + TwoPi;
            r:= sqrt (sqr(Pnt.y) + sqr(Pnt.z));
            result.y := fixZero(r*Cos(a));
            result.z := fixZero(r*Sin(a));
            result.x := Pnt.x;
          end;
    end;
  end;  // RotatePoint


  Function RotatePointRel (const Pnt : point;
                           const RelPnt : point;
                           const Ang : double;
                           const Axis: integer) : point;
  var
    a, r : double;
  begin
    case axis of
      z : begin
            if RealEqual(pnt.y, RelPnt.y) and RealEqual(pnt.x, RelPnt.x) then begin
              result := Pnt;
              exit;
            end;
            a := ArcTan2 (Pnt.y-RelPnt.y, Pnt.x-RelPnt.x) + Ang;
            if a > pi then
              a := a - TwoPi
            else if a < -pi then
              a := a + TwoPi;
            r:= sqrt (sqr(Pnt.x-RelPnt.x) + sqr(Pnt.y-RelPnt.y));
            result.x := fixZero(r*Cos(a)) + RelPnt.x;
            result.y := fixZero(r*Sin(a)) + RelPnt.y;
            result.z := Pnt.z;
          end;
      y : begin
            if RealEqual(pnt.z, RelPnt.z) and RealEqual(pnt.x, RelPnt.x) then begin
              result := Pnt;
              exit;
            end;
            a := ArcTan2 (Pnt.x-RelPnt.x, Pnt.z-RelPnt.z) + Ang;    // subract Ang since dcad angles are anticlockwise
            if a > pi then
              a := a - TwoPi
            else if z < -pi then
              a := a + TwoPi;
            r:= sqrt (sqr(Pnt.x-RelPnt.x) + sqr(Pnt.z-RelPnt.z));
            result.z := fixZero(r*Cos(a)) + RelPnt.z;
            result.x := fixZero(r*Sin(a)) + RelPnt.x;
            result.y := Pnt.y;
          end;
      x : begin
            if RealEqual(pnt.y, RelPnt.y) and RealEqual(pnt.z, RelPnt.z) then begin
              result := Pnt;
              exit;
            end;
            a := ArcTan2 (Pnt.z-RelPnt.z, Pnt.y-RelPnt.y) + Ang;    // subract Ang since dcad angles are anticlockwise
            if a > pi then
              a := a - TwoPi
            else if z < -pi then
              a := a + TwoPi;
            r:= sqrt (sqr(Pnt.y-RelPnt.y) + sqr(Pnt.z-RelPnt.z));
            result.y := fixZero(r*Cos(a)) + RelPnt.y;
            result.z := fixZero(r*Sin(a)) + RelPnt.z;
            result.x := Pnt.x;
          end;
    end;
  end;  // RotatePointRel

  Function MovePoint (const Pnt : point;
                      const dx, dy, dz : double) : point;
  begin
    result.x := Pnt.x + dx;
    result.y := Pnt.y + dy;
    result.z := Pnt.z + dz;
  end;


  Function MovePoint (const Pnt : point;
                      const Vct : point) : point;
  begin
    result.x := pnt.x + Vct.x;
    result.y := pnt.y + Vct.y;
    result.z := pnt.z + Vct.z;
  end; // MovePoint

  Function InvertVct (const Vct : point) : point;
  begin
    result.x := -Vct.x;
    result.y := -Vct.y;
    result.z := -Vct.z;
  end; // InvertVct


  procedure RotatePoints (var points : array of point;
                          const npnt : integer;
                          const Ang : double;
                          const Axis: integer);
  var
    i : integer;
  begin
    for i := low(points) to low(points)+npnt-1 do
      points[i] := RotatePoint (points[i], Ang, Axis);
  end;  // RotatePoints


  procedure RotatePointsRel (var points : array of point;
                             const npnt : integer;
                             const RelPnt : point;
                             const Ang : double;
                             const Axis: integer);
  var
    i : integer;
  begin
    for i := low(points) to low(points)+npnt-1 do
      points[i] := RotatePointRel(points[i], RelPnt, Ang, Axis);
  end;


  Function MovePoints (var points : array of point;
                       const npnt : integer;
                       const Vct : point) : point;
  var
    i : integer;
  begin
    for i := low(points) to low(points)+npnt-1 do begin
      points[i].x := points[i].x + Vct.x;
      points[i].y := points[i].y + Vct.y;
      points[i].z := points[i].z + Vct.z;
    end;
  end;

  Function MovePoints (var points : array of point;
                       const npnt : integer;
                       const dx, dy, dz : double) : point;
  var
    i : integer;
  begin
    for i := low(points) to low(points)+npnt-1 do begin
      points[i].x := points[i].x + dx;
      points[i].y := points[i].y + dy;
      points[i].z := points[i].z + dz;
    end;
  end;



  function fixZero (num : double) : double;
  begin
    if abs(num) <= NearZero then
      result := 0.0
    else
      result := num;
  end;