unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, ShellApi;

type
  TSpacePlanner = class(TForm)
    Shape1: TShape;
    Panel1: TPanel;
    lblwww: TLabel;
    lblCopyright: TLabel;
    lblSpacePlanner: TLabel;
    lblVersion: TLabel;
    lblContribute: TLabel;
    btnInstructions: TButton;
    mCopyright: TMemo;
    btnDismiss: TButton;
    lblErr: TLabel;
    procedure btnDismissClick(Sender: TObject);
    procedure btnInstructionsClick(Sender: TObject);
    procedure lblContributeClick(Sender: TObject);
    procedure lblwwwClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure mCopyrightEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SpacePlanner: TSpacePlanner;

implementation

{$R *.dfm}

procedure TSpacePlanner.btnDismissClick(Sender: TObject);
begin
  SpacePlanner.Close;
end;

procedure TSpacePlanner.btnInstructionsClick(Sender: TObject);
var
  helpfilename : PWideChar;
  tempstr : string;
begin
  try
    tempstr := ExtractFilePath(ParamStr(0));
    helpfilename := PWideChar(tempstr + 'ShadowAnalysis.pdf');
    ShellExecute(0, 'open', helpfilename,nil,nil, SW_SHOWNORMAL) ;
    SpacePlanner.Close;
   except
    on E: Exception do
      lblErr.Caption := E.ClassName + ': ' + E.Message;
  end;
end;

procedure TSpacePlanner.FormCreate(Sender: TObject);
begin
  if ParamCount > 0 then
    lblVersion.Caption := 'Version ' + paramstr(1);
end;

procedure TSpacePlanner.lblContributeClick(Sender: TObject);
begin
  ShellExecute(self.WindowHandle,'open','www.dhsoftware.com.au/contribute.htm',nil,nil, SW_SHOWNORMAL);
end;

procedure TSpacePlanner.lblwwwClick(Sender: TObject);
begin
ShellExecute(self.WindowHandle,'open','www.dhsoftware.com.au',nil,nil, SW_SHOWNORMAL);
end;

procedure TSpacePlanner.mCopyrightEnter(Sender: TObject);
begin
  btnDismiss.SetFocus;
end;

end.
