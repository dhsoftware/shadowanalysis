program ShAnalInvoker;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,  Windows, ShellApi;

begin
  try
    { TODO -oUser -cConsole Main : Insert code here }
    ShellExecute (0, 'Open', pchar (ExtractFilePath (ParamStr(0)) + 'ShadAnalysis1.exe'), pchar(paramstr(1)), '', SW_SHOW);
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
